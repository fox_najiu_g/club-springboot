package com.fox0g.club.club_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.service.ClubInfoService;
import com.fox0g.club.model.dto.AllClubVo;
import com.fox0g.club.model.dto.ClubDto;
import com.fox0g.club.model.dto.ClubsDto;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.ShowClub;
import com.fox0g.club.model.vo.ClubInfoVo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.model.vo.UpdateClubVo;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-02-25 21:53
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/club")
public class ClubInfoController {

    @Autowired
    private ClubInfoService clubInfoService;


    @PostMapping("/applyClub/{userId}")
    public Result applyClub(@Validated @RequestBody ClubInfoVo clubVo, @PathVariable Long userId){
        //由校验 合法
        //TODO 验证 该用户是否存在
        //校验clubId 是否合法
        if(Long.toString(userId).length()!=10){
            return Result.fail("用户不合法，请检查后重新申请");
        }
        //
        boolean isAdd = clubInfoService.addClubInfo(userId,clubVo);

        return isAdd ? Result.success("申请成功，审核中...") : Result.fail("申请失败，请稍后重试");

    }

    /*
     * @Description: 根据用户id获取该用户所有club
     * @Param: userId
     * @return: Result
     * @Date: 2023/2/26
    */
    @GetMapping("/getAllClubByUserId/{userId}")
    public Result getAllClubByUserId(@PathVariable Long userId){
        //校验clubId 是否合法
        if(Long.toString(userId).length()!=10){
            return Result.fail("用户不合法，请检查后重新申请");
        }

        //查 该用户所有 club
        List<AllClubVo> clubList = clubInfoService.getByUserId(userId);
        return Result.success(clubList);
    }

    /*
     * @Description: 根据clubId 获取club信息
     * @Param: clubId
     * @return: Result
     * @Date: 2023/2/26
    */
    @GetMapping("/getClubInfoByClubId/{clubId}")
    public Result getClubInfoByClubId(@PathVariable Long clubId){
        //校验clubId 是否合法
        if(Long.toString(clubId).length()!=10){
            return Result.fail("错误！，请检查后重新申请");
        }

        //查询该 club 的所有信息
        ClubInfo clubInfo = clubInfoService.getClubInfo(clubId);
        if(ObjectUtils.isEmpty(clubInfo)){
            return Result.fail("错误，不存在该Club");
        }
        ClubDto clubDto = new ClubDto();
        BeanUtils.copyProperties(clubInfo,clubDto);
        return Result.success(clubDto);
    }


    @PostMapping("/updateClubInfo")
    public Result updateClubInfo(@Validated @RequestBody UpdateClubVo clubVo){
        //由校验 合法

        //根据clubId 去修改updateInfo
        //先去查询该club 是否存在
        ClubInfo clubInfo = clubInfoService.getClubInfo(clubVo.getClubId());
        if(ObjectUtils.isEmpty(clubInfo)){
            return Result.fail("保存失败,请稍后重试");
        }
        BeanUtils.copyProperties(clubVo,clubInfo);
        boolean isUpdate = clubInfoService.updateClubInfo(clubInfo);
        return isUpdate ? Result.success("保存成功"):Result.fail("保存失败,请稍后重试");
    }

    @GetMapping("/getClubInfo/{clubId}")
    public ClubInfo getClubInfo(@PathVariable Long clubId){
        //校验clubId 是否合法
        if(Long.toString(clubId).length()!=10){
            return null;
        }

        //查询该 club 的所有信息
        ClubInfo clubInfo = clubInfoService.getClubInfo(clubId);
        if(ObjectUtils.isEmpty(clubInfo)){
            return null;
        }
        return clubInfo;
    }

    @GetMapping("/getClubList")
    public List<ShowClub> getClubList(){

        //查询该 club 的所有信息
        List<ShowClub> list = clubInfoService.getClubList();

        return list;
    }

    @GetMapping("/getPartClubInfo/{clubId}")
    public Result getPartClubInfo(@PathVariable Long clubId){
        ShowClub clubPartInfo = clubInfoService.getPartClubInfoByClubId(clubId);

        return Result.success(clubPartInfo);
    }

    @GetMapping("/searchClubListByParam/{searchParam}")
    public Result searchClubListByParam(@PathVariable String searchParam, PageParamVo pageParam){

        PageInfo<ClubsDto> list = clubInfoService.getClubListBySearch(searchParam,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }
}
