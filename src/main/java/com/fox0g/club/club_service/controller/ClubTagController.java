package com.fox0g.club.club_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.service.ClubTagService;
import com.fox0g.club.model.dto.ClubTagDto;
import com.fox0g.club.model.dto.ClubsDto;
import com.fox0g.club.model.po.ClubTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-02 14:45
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/club")
public class ClubTagController {

    @Autowired
    private ClubTagService clubTagService;

    /*
     * @Description: 根据cludId查询所有 标签类型的tag
     * @Param: tagType
 * @param clubId
     * @return: Result
     * @Date: 2023/3/2
    */
    @GetMapping("/getClubTagBy/{tagType}/{clubId}")
    public Result getClubTagByTagTypeClubId(@PathVariable("tagType")Integer tagType,@PathVariable("clubId")Long clubId){

        List<ClubTagDto> tagList = clubTagService.getClubTagByTagTypeClubId(tagType, clubId);
        return Result.success("成功",tagList);
    }

    /*
     * @Description: 根据标签id和clubid确定并删除
     * @Param: id
 * @param clubId
     * @return: Result
     * @Date: 2023/3/2
    */
    @DeleteMapping("/deleteClubTagBy/{id}/{clubId}")
    public Result deleteClubTagByIdClubId(@PathVariable("id")Integer id,@PathVariable("clubId")Long clubId){
        boolean isDelete = clubTagService.deleteClubTagByIdClubId(id, clubId);
        return isDelete ? Result.success("成功"):Result.fail("失败");
    }

    @PutMapping("/deleteClubTagBy/{tagType}/{clubId}")
    public Result joinMultipleActiveApplyById(@PathVariable("tagType")Integer tagType,@PathVariable("clubId")Long clubId,@RequestBody List<String> clubTagList){


        //查找当前共有几个
        int count = clubTagService.getClubTagCountByTagType(tagType,clubId);

        //判断类型 是否过量
        if(tagType==331 && (count+clubTagList.size())>3){
            //主标签
            //过量
            return Result.fail("认证标签已过量！");
        }
        if(tagType==332 && (count+clubTagList.size())>5){
            //副标签
            //过量
            return Result.fail("认证标签已过量！");
        }
        //未过量
        clubTagList.stream().forEach(clubTag -> {
            clubTagService.save(new ClubTag(tagType, clubTag, clubId));
        });
        return Result.success("成功");

    }

    @GetMapping("/getClubListByClubTag/{tagType}/{tagValue}")
    public Result getClubListByClubTag(@PathVariable("tagType")Integer tagType,@PathVariable("tagValue")String tagValue){

        List<ClubsDto> list = clubTagService.getClubByClubTag(tagType,tagValue);
        return Result.success(list);
    }

    //TODO 完成club页面 标签的显示
}
