package com.fox0g.club.club_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.service.ClubConstituteService;
import com.fox0g.club.model.dto.ClubConstituteDto;
import com.fox0g.club.model.po.ClubConstitute;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.vo.ClubConstituteVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-01 14:00
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/club")
public class ClubConstituteController {

    @Autowired
    private ClubConstituteService clubConstituteService;

    /*
     * @Description: 获取父节点的信息
     * @Param: clubId
     * @return: Result
     * @Date: 2023/3/1
    */
    @GetMapping("/getClubParentByClubId/{clubId}")
    public Result getClubConstituteParentByClubId(@PathVariable Long clubId){

        List<ClubConstituteDto> list = clubConstituteService.getClubParentByParentId(0L,clubId);
        return Result.success("成功",list);
    }

    /*
     * @Description: 根据父节点id获取所有子节点
     * @Param: parentId
     * @return: Result
     * @Date: 2023/3/1
    */
    @GetMapping("/getClubChildByParentId/{parentId}")
    public Result getClubConstituteChildByParentId(@PathVariable Long parentId){

        List<ClubConstituteDto> list = clubConstituteService.getClubChildByParentId(parentId);
        return Result.success("成功",list);
    }

    /*
     * @Description: 新增club成员/部门
     * @Param: clubConstituteVo
     * @return: Result
     * @Date: 2023/3/1
    */
    @PutMapping("/addClubConstituteChild")
    public Result addClubConstituteChild(@RequestBody ClubConstituteVo clubConstituteVo){
        ClubConstitute clubConstitute = new ClubConstitute();
        BeanUtils.copyProperties(clubConstituteVo,clubConstitute);
        boolean isInsert = clubConstituteService.insertClubConstitute(clubConstitute);
        return isInsert ? Result.success("操作成功"):Result.fail("操作失败");
    }

    /*
     * @Description: 根据value删除关联下的所有联系
     * @Param: value
     * @return: Result
     * @Date: 2023/3/1
    */
    @DeleteMapping("/deleteClubConstitute/{value}")
    public Result deleteClubConstitute(@PathVariable Long value){

        boolean isDelete = clubConstituteService.deleteClubConstitute(value);
        return isDelete ? Result.success("成功"):Result.fail("失败");
    }

    @Transactional
    @PostMapping("/upIndex/{parentId}/{id}")
    public Result upIndexByParentId(@PathVariable("parentId")Long parentId,@PathVariable("id")Integer id){

        List<ClubConstituteDto> sameList = clubConstituteService.getClubChildByParentId(parentId);
        //获取
        ClubConstitute item = clubConstituteService.getById(id);
        //获取下标
        int index=0;
        for(int i=0;i<sameList.size();i++){
            if(sameList.get(i).getId()==id){
                index=i;
                break;
            }
        }
        if(index<=0){
            //是第一个 无法再上移
            return Result.success();
        }
        //获取上一个
        ClubConstituteDto upItem = sameList.get(index - 1);
        //上一个的ID
        Integer upId = upItem.getId();
        //将上一个id置0
        upItem.setId(0);
        clubConstituteService.updateById(upItem);
        //更换当前ID
        item.setId(upId);
        clubConstituteService.updateById(item);
        //更换上一个ID
        upItem.setId(id);
        clubConstituteService.updateById(upItem);
        return Result.success();

    }

    @Transactional
    @PostMapping("/downIndex/{parentId}/{id}")
    public Result downIndexByParentId(@PathVariable("parentId")Long parentId,@PathVariable("id")Integer id){

        List<ClubConstituteDto> sameList = clubConstituteService.getClubChildByParentId(parentId);
        /*if(id==sameList.get(0).getId()){

        }*/
        //获取
        ClubConstitute item = clubConstituteService.getById(id);
        //获取下标
        int index=0;
        for(int i=0;i<sameList.size();i++){
            if(sameList.get(i).getId()==id){
                index=i;
                break;
            }
        }
        if(index>=sameList.size()-1){
            //是第一个 无法再上移
            return Result.success();
        }
        //获取下一个
        ClubConstituteDto downItem = sameList.get(index + 1);
        //下一个的ID
        Integer downId = downItem.getId();
        //将上一个id置0
        downItem.setId(0);
        clubConstituteService.updateById(downItem);
        //更换当前ID
        item.setId(downId);
        clubConstituteService.updateById(item);
        //更换上一个ID
        downItem.setId(id);
        clubConstituteService.updateById(downItem);
        return Result.success();

    }

    @GetMapping("/getUserClubList/{userId}")
    public Result getUserClubList(@PathVariable Long userId){
        //TODO userId校验
        List<ClubInfo> list = clubConstituteService.getUserClubListByUserId(userId);
        return Result.success(list);
    }

}
