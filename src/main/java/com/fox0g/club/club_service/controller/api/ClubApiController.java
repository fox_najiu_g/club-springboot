package com.fox0g.club.club_service.controller.api;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.service.ClubAccountService;
import com.fox0g.club.club_service.service.ClubApplyService;
import com.fox0g.club.club_service.service.ClubConstituteService;
import com.fox0g.club.club_service.service.ClubInfoService;
import com.fox0g.club.model.dto.ApplyClubDto;
import com.fox0g.club.model.dto.ClubApplyUserDto;
import com.fox0g.club.model.dto.ClubInfoDto;
import com.fox0g.club.model.po.ClubAccount;
import com.fox0g.club.model.po.ClubConstitute;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-05 11:13
 * @author: Fox0g
 * @description: TODO
 */
@Slf4j
@RestController
@RequestMapping("/club/manage")
public class ClubApiController {

    @Autowired
    private ClubInfoService clubInfoService;
    @Autowired
    private ClubApplyService clubApplyService;
    @Autowired
    private ClubAccountService clubAccountService;
    @Autowired
    private ClubConstituteService clubConstituteService;

    @GetMapping("/getAllClub")
    public Result getAllClub(PageParamVo pageParamVo){

        PageInfo<ClubInfoDto> clubDtoPageInfo = clubInfoService.selectAllClubList(pageParamVo);
        return Result.success(clubDtoPageInfo);
    }

    @DeleteMapping("/deleteClub/{clubId}")
    public Result deleteClub(@PathVariable Long clubId){
        boolean isDelete = clubInfoService.deleteClubByClubId(clubId);
        return isDelete ? Result.success("删除成功"):Result.fail("删除失败，请稍后重试");
    }

    @GetMapping("/getAllApplyClub")
    public Result getAllApplyClub(PageParamVo pageParamVo){

        PageInfo<ApplyClubDto> pageInfo = clubInfoService.selectAllApplyClubList(pageParamVo);
        return Result.success(pageInfo);
    }
    @GetMapping("/getAllApplyingClub")
    public Result getAllApplyingClub(PageParamVo pageParamVo){

        PageInfo<ApplyClubDto> pageInfo = clubInfoService.selectAllApplyingClubList(pageParamVo);
        return Result.success(pageInfo);
    }

    @PostMapping("/updateApplyingClubStatus/{id}/{applyStatus}")
    public Result updateApplyingClubStatus(@PathVariable("id")Integer id,@PathVariable("applyStatus")Integer applyStatus){
        ClubInfo clubInfo = clubInfoService.getById(id);
        clubInfo.setApplyStatus(applyStatus);
        clubInfo.setClubRegistration(LocalDate.now());
        //更新状态（通过申请）
        boolean isUpdate = clubInfoService.updateClubInfo(clubInfo);
        //申请通过
        if(isUpdate){
            //创建组织账户
            ClubAccount clubAccount = new ClubAccount();
            clubAccount.setClubId(clubInfo.getClubId());
            clubAccount.setClubIsJoin(1);
            clubAccount.setUserId(clubInfo.getUserId());
            clubAccountService.save(clubAccount);
            //创建组织组成
            ClubConstitute clubConstitute = new ClubConstitute();
            clubConstitute.setParentId(0L);
            clubConstitute.setLabel(clubInfo.getClubName());
            clubConstitute.setValue(clubInfo.getClubId());
            clubConstitute.setIsLeaf(0);
            clubConstituteService.save(clubConstitute);

            return Result.success("操作成功");
        }

        return Result.fail("操作失败");
    }

    @PostMapping("/multipleUpdateApplyingClubStatus/{applyStatus}")
    public Result joinMultipleActiveApplyById(@PathVariable("applyStatus")Integer applyStatus,@RequestBody List<Integer> ids){

        clubInfoService.multipleUpdateApplyingClubStatus(ids,applyStatus);
        return Result.success("操作成功");

    }

    @GetMapping("/getAllClubAllApplyUser")
    public Result getAllClubAllApplyUser(PageParamVo pageParamVo){
        PageInfo<ClubApplyUserDto> pageInfo = clubApplyService.selectAllClubAllApplyUser(pageParamVo);
        return Result.success(pageInfo);
    }
}
