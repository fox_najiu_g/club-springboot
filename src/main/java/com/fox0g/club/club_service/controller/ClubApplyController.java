package com.fox0g.club.club_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.service.ClubApplyService;
import com.fox0g.club.club_service.service.ClubConstituteService;
import com.fox0g.club.model.dto.ClubApplyDto;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.ClubApplyVo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.user_service.controller.UserInfoController;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-01 21:09
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/club")
public class ClubApplyController {

    @Autowired
    private ClubApplyService clubApplyService;
    @Autowired
    private UserInfoController userInfoController;
    @Autowired
    private ClubConstituteService clubConstituteService;


    /*
     * @Description: 分页：获取申请信息
     * @Param: clubId
 * @param pageParam
     * @return: Result
     * @Date: 2023/3/1
    */
    @GetMapping("/getAllApplyByClubId/{clubId}")
    public Result getAllApplyClubByClubId(@PathVariable Long clubId, PageParamVo pageParam){

        //clubId 是否合法
        if(Long.toString(clubId).length()!=10){
            return Result.fail("失败");
        }
        PageInfo<ClubApplyDto> clubApplyList = clubApplyService.getAllApplyByClubId(clubId,pageParam);
        return Result.success("获取成功",clubApplyList);
    }

    /*
     * @Description: 修改申请状态
     * @Param: id
 * @param applyStatus
     * @return: Result
     * @Date: 2023/3/1
    */
    @PostMapping("/updateStatusById/{id}/{applyStatus}")
    public Result updateStatus(@PathVariable("id")Integer id,@PathVariable("applyStatus")Integer applyStatus){

        boolean isUpdate = clubApplyService.updateStatusById(id,applyStatus);
        return isUpdate ? Result.success("成功"):Result.fail("失败");
    }

    @PutMapping("/addUserApply")
    public Result addUserApply(@RequestBody ClubApplyVo clubApplyVo){
        //TODO vo校验
        UserInfo userInfo = userInfoController.getUserInfoByUserId(clubApplyVo.getUserId());
        if(ObjectUtils.isEmpty(userInfo)){
            //用户非法
            return Result.fail("非法用户");
        }
        //获取该用户加入的所有组织
        List<Long> clubList = clubConstituteService.getParentValueByChildValue(userInfo.getUserId());
        if(ObjectUtils.isNotEmpty(clubList)){
            for (Long clubId : clubList) {
                if(clubId.equals(clubApplyVo.getClubId())){
                    //该用户已加入该组织
                    return Result.fail("已加入该组织");
                }
            }
        }

        //未加入该组织 可加入

        boolean isAdd = clubApplyService.insertClubApply(clubApplyVo);
        return isAdd ? Result.success("审核中..."):Result.fail("请勿重复提交");
    }
}
