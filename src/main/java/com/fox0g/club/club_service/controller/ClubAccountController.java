package com.fox0g.club.club_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.service.ClubAccountService;
import com.fox0g.club.club_service.service.ClubInfoService;
import com.fox0g.club.model.po.ClubAccount;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.vo.ClubAccountIsJoinVo;
import com.fox0g.club.model.vo.ClubAccountVo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: club-parent
 * @Date: 2023-02-26 22:24
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/club")
public class ClubAccountController {

    @Autowired
    private ClubAccountService clubAccountService;
    @Autowired
    private ClubInfoService clubInfoService;

    /*
     * @Description: 根据clubId获取club 账号信息
     * @Param: clubId
     * @return: Result
     * @Date: 2023/2/27
    */
    @GetMapping("/getClubAccountByClubId/{clubId}")
    public Result getClubAccountByClubId(@PathVariable Long clubId){
        //校验clubId 是否合法
        if(Long.toString(clubId).length()!=10){
            return Result.fail("非法，请检查后重新申请");
        }
        //根据clubId 查询所有信息
        ClubAccount clubAccount = clubAccountService.getByClubId(clubId);
        if(ObjectUtils.isEmpty(clubAccount)){
            return Result.fail();
        }
        return Result.success("获取成功",clubAccount);
    }

    @PostMapping("/updateClubAccount")
    public Result updateClubAccount(@RequestBody ClubAccountVo clubAccountVo){
        //简单校验 数据
        if(Long.toString(clubAccountVo.getClubId()).length()!=10 ||ObjectUtils.isEmpty(clubAccountVo)){
            return Result.fail();
        }
        //如果userId存在 校验其合法
        if(clubAccountVo.getUserId()!=null&&Long.toString(clubAccountVo.getUserId()).length()!=10){
            return Result.fail();
        }
        //
        ClubAccount clubAccount = new ClubAccount();
        BeanUtils.copyProperties(clubAccountVo,clubAccount);
        clubAccount.setClubId(clubAccountVo.getClubId());
        boolean isUpdate = clubAccountService.updateClubAccount(clubAccount);


        if(!isUpdate){
            return Result.fail("操作失败");
        }
        ClubInfo clubInfo = clubInfoService.getClubInfo(clubAccountVo.getClubId());
        clubInfo.setUserId(clubAccountVo.getUserId());
        clubInfoService.updateById(clubInfo);
        return Result.success("操作成功");

    }

    /*
     * @Description: 根据clubId获取clubAccount 是否可以加入
     * @Param: clubId
     * @return: Result
     * @Date: 2023/2/27
    */
    @GetMapping("/getClubAccountIsJoin/{clubId}")
    public Result getClubAccountIsJoin(@PathVariable Long clubId){
        //校验clubId 是否合法
        if(Long.toString(clubId).length()!=10){
            return Result.fail("非法，请检查后重新申请");
        }
        //合法，查询该 club 是否可以join
        Integer isJoin = clubAccountService.getClubAccountIsJoin(clubId);
        return isJoin!=null ? Result.success("成功",isJoin):Result.fail("失败",isJoin);
    }

    /*
     * @Description: 审批
     * @Param: isJoinVo
     * @return: Result
     * @Date: 2023/3/1
    */
    @PostMapping("/updateIsJoin")
    public Result updateIsJoin(@RequestBody ClubAccountIsJoinVo isJoinVo){
        //校验clubId 是否合法
        if(Long.toString(isJoinVo.getClubId()).length()!=10){
            return Result.fail("非法，请检查后重新申请");
        }
        //根据clubId修改 clubIsJoin
        Boolean isUpdate = clubAccountService.updateClubAccountIsJoin(isJoinVo.getClubId(),isJoinVo.getClubIsJoin());

        return isUpdate==true ? Result.success():Result.fail();
    }
}
