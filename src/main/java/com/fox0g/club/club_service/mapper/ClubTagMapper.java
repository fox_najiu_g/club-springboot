package com.fox0g.club.club_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.dto.ClubTagDto;
import com.fox0g.club.model.po.ClubTag;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【club_tag】的数据库操作Mapper
* @createDate 2023-03-02 14:36:58
* @Entity com.fox0g.model.po.ClubTag
*/
@Mapper
public interface ClubTagMapper extends BaseMapper<ClubTag> {


    @Select("select * from club_tag where tag_type=#{tagType}  and club_id=#{clubId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "tag_type",property = "tagType"),
            @Result(column = "tag_value",property = "tagValue"),
            @Result(column = "club_id",property = "clubId"),
    })
    List<ClubTagDto> getClubTagByTagTypeClubId(@Param("tagType") Integer tagType, @Param("clubId")Long clubId);

    @Delete("delete from club_tag where id=#{id} and club_id=#{clubId}")
    boolean deleteClubTagByIdAndClubId(@Param("id")Integer id,@Param("clubId")Long clubId);


    @Select("select club_id from club_tag where tag_type=#{tagType} and tag_value=#{tagValue}")
    List<Long> getClubIdByClubTag(@Param("tagType")Integer tagType,@Param("tagValue")String tagValue);

    @Select("select tag_value from club_tag where club_id=#{clubId} and tag_type=#{tagType}")
    List<String> getTagValueByClubIdTagValue(@Param("clubId")Long clubId,@Param("tagType")Integer tagType);


}




