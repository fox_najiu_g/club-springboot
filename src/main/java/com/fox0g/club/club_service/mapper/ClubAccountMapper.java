package com.fox0g.club.club_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.ClubAccount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
* @author 86187
* @description 针对表【club_account】的数据库操作Mapper
* @createDate 2023-02-26 22:23:00
* @Entity com.fox0g.model.po.ClubAccount
*/
@Mapper
public interface ClubAccountMapper extends BaseMapper<ClubAccount> {

    @Select("select club_is_join from club_account where club_id = #{clubId}")
    Integer getClubAccountIsJoin(@Param("clubId") Long clubId);

    @Update("update club_account set club_is_join = #{clubIsJoin} where club_id = #{clubId}")
    Boolean updateClubIsJoin(@Param("clubIsJoin") int clubIsJoin,
                             @Param("clubId") Long clubId);

}




