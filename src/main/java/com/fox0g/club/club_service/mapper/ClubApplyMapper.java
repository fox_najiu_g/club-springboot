package com.fox0g.club.club_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.dto.ClubApplyDto;
import com.fox0g.club.model.dto.ClubApplyUserDto;
import com.fox0g.club.model.po.ClubApply;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【club_apply】的数据库操作Mapper
* @createDate 2023-03-01 21:08:25
* @Entity com.fox0g.model.po.ClubApply
*/
@Mapper
public interface ClubApplyMapper extends BaseMapper<ClubApply> {


    @Select("select * from club_apply where club_id = #{clubId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "user_name",property = "userName"),
            @Result(column = "apply_remark",property = "applyRemark"),
            @Result(column = "apply_status",property = "applyStatus"),
    })
    List<ClubApplyDto> selectApplyByClubId(@Param("clubId")Long clubId);

    @Update("update club_apply set apply_status = #{applyStatus} where id = #{id}")
    boolean updateStatusById(@Param("id")Integer id,@Param("applyStatus")Integer applyStatus);

    @Select("select club_apply.id,club_apply.club_id,club_info.club_name,club_apply.apply_date,club_apply.user_id,club_apply.user_name,club_apply.apply_remark,club_apply.apply_status from club_apply,club_info where club_info.club_id=club_apply.club_id")
    List<ClubApplyUserDto> getAllClubAllApplyUser();


}




