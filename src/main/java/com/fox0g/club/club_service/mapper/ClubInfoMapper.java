package com.fox0g.club.club_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.dto.AllClubVo;
import com.fox0g.club.model.dto.ApplyClubDto;
import com.fox0g.club.model.dto.ClubInfoDto;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.ShowClub;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【club_info】的数据库操作Mapper
* @createDate 2023-02-25 21:58:48
* @Entity com.fox0g.model.po.ClubInfo
*/
@Mapper
public interface ClubInfoMapper extends BaseMapper<ClubInfo> {

    @Select("select club_id,club_name from club_info where user_id = #{userId} and apply_status=1")
    @Results(value ={
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "club_name",property = "clubName")
    })
    List<AllClubVo> getAllClubIdName(@Param("userId") Long userId);

    @Select("select club_id,user_id,club_name,club_registration from club_info where apply_status=1")
    @Results(value ={
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "club_name",property = "clubName"),
            @Result(column = "club_registration",property = "clubRegistration"),
    })
    List<ClubInfoDto> getAllClubList();

    @Select("select id,apply_date,club_id,club_name,user_id,club_reason,apply_status from club_info")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "club_name",property = "clubName"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "club_reason",property = "clubReason"),
            @Result(column = "apply_status",property = "applyStatus"),
    })
    List<ApplyClubDto> getApplyClubList();

    @Select("select id,apply_date,club_id,club_name,user_id,club_reason,apply_status from club_info where apply_status=3")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "club_name",property = "clubName"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "club_reason",property = "clubReason"),
            @Result(column = "apply_status",property = "applyStatus"),
    })
    List<ApplyClubDto> getApplyingClubList();


    @Select("select club_id,club_name,club_image from club_info where apply_status=1")
    @Results(value ={
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "club_name",property = "clubName"),
            @Result(column = "club_image",property = "clubImage"),
    })
    List<ShowClub> getShowClubList();

    @Select("select id,club_id,club_name,club_image from club_info where club_id=#{clubId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "club_id",property = "clubId"),
            @Result(column = "club_name",property = "clubName"),
            @Result(column = "club_image",property = "clubImage"),
    })
    ShowClub getPartClubInfoByClubId(@Param("clubId")Long clubId);

    @Select("SELECT distinct club_info.club_id " +
            "from club_info,club_tag " +
            "where club_info.club_id=club_tag.club_id and (club_name like '%${searchParam}%' or tag_value like '%${searchParam}%')")
    List<Long> getClubIdListBySearchParam(String searchParam);
}




