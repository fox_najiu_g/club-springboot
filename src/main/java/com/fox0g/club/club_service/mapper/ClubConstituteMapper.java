package com.fox0g.club.club_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.ClubConstitute;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【club_constitute】的数据库操作Mapper
* @createDate 2023-03-01 13:59:23
* @Entity com.fox0g.model.po.ClubConstitute
*/
@Mapper
public interface ClubConstituteMapper extends BaseMapper<ClubConstitute> {


    @Select("select * from club_constitute where parent_id = #{parentId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "parent_id",property = "parentId"),
            @Result(column = "label",property = "label"),
            @Result(column = "name",property = "name"),
            @Result(column = "value",property = "value"),
            @Result(column = "is_leaf",property = "isLeaf"),
    })
    List<ClubConstitute> selectAllByParentId(@Param("parentId")Long parentId);

    @Delete("delete from club_constitute where value = #{value}")
    void deleteClubConstituteByValue(@Param("value") Long value);

    @Delete("delete from club_constitute where parent_id = #{parentId}")
    void deleteClubConstituteByParentId(@Param("parentId") Long parentId);

    @Select("select parent_id from club_constitute where value=#{value}")
    List<Long> getParentIdByValue(@Param("value")Long value);
}




