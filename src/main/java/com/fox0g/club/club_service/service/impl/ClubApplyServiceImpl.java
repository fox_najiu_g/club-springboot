package com.fox0g.club.club_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.club_service.mapper.ClubApplyMapper;
import com.fox0g.club.club_service.service.ClubApplyService;
import com.fox0g.club.model.dto.ClubApplyDto;
import com.fox0g.club.model.dto.ClubApplyUserDto;
import com.fox0g.club.model.po.ClubApply;
import com.fox0g.club.model.vo.ClubApplyVo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
* @author 86187
* @description 针对表【club_apply】的数据库操作Service实现
* @createDate 2023-03-01 21:08:25
*/
@Service
public class ClubApplyServiceImpl extends ServiceImpl<ClubApplyMapper, ClubApply>
    implements ClubApplyService {

    @Override
    public PageInfo<ClubApplyDto> getAllApplyByClubId(Long clubId, PageParamVo pageParam) {

        PageHelper.startPage(pageParam.getPageNum(),pageParam.getPageSize());
        List<ClubApplyDto> clubApplyList = this.baseMapper.selectApplyByClubId(clubId);
        PageInfo<ClubApplyDto> clubApplyPageInfo = new PageInfo<>(clubApplyList);
        return clubApplyPageInfo;
    }

    @Override
    public boolean updateStatusById(Integer id, Integer applyStatus) {

        return  this.baseMapper.updateStatusById(id, applyStatus);

    }

    @Override
    public PageInfo<ClubApplyUserDto> selectAllClubAllApplyUser(PageParamVo pageParamVo) {

        PageHelper.startPage(pageParamVo.getPageNum(),pageParamVo.getPageSize());
        List<ClubApplyUserDto> clubApplyDtoList = this.baseMapper.getAllClubAllApplyUser();
        PageInfo<ClubApplyUserDto> pageInfo = new PageInfo<ClubApplyUserDto>(clubApplyDtoList);
        return pageInfo;
    }

    @Override
    public boolean insertClubApply(ClubApplyVo clubApplyVo) {
        //
        LambdaQueryWrapper<ClubApply> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubApply::getUserId,clubApplyVo.getUserId())
                .eq(ClubApply::getClubId,clubApplyVo.getClubId())
                .eq(ClubApply::getApplyStatus,3);
        ClubApply clubApply = this.baseMapper.selectOne(queryWrapper);
        if(!ObjectUtils.isEmpty(clubApply)){
            //该用户已发送过申请
            return false;
        }
        ClubApply apply = new ClubApply();
        BeanUtils.copyProperties(clubApplyVo,apply);
        apply.setApplyDate(LocalDate.now());
        apply.setApplyStatus("3");

        int insert = this.baseMapper.insert(apply);
        return insert==1 ? true:false;
    }
}




