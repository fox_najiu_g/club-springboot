package com.fox0g.club.club_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.club_service.mapper.ClubConstituteMapper;
import com.fox0g.club.club_service.service.ClubConstituteService;
import com.fox0g.club.club_service.service.ClubInfoService;
import com.fox0g.club.model.dto.ClubConstituteDto;
import com.fox0g.club.model.po.ClubConstitute;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.user_service.controller.UserInfoController;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @author 86187
* @description 针对表【club_constitute】的数据库操作Service实现
* @createDate 2023-03-01 13:59:23
*/
@Service
public class ClubConstituteServiceImpl extends ServiceImpl<ClubConstituteMapper, ClubConstitute>
    implements ClubConstituteService {

    @Autowired
    private UserInfoController userInfoController;
    @Autowired
    private ClubInfoService clubInfoService;

    @Override
    public List<ClubConstituteDto> getClubParentByParentId(Long parentId, Long value) {

        LambdaQueryWrapper<ClubConstitute> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubConstitute::getParentId,parentId).eq(ClubConstitute::getValue,value);
        List<ClubConstitute> clubConstituteList = this.baseMapper.selectList(queryWrapper);
        List<ClubConstituteDto> clubConstituteDtoList = new ArrayList<>();
        clubConstituteList.stream().forEach(clubConstitute -> {
            ClubConstituteDto clubConstituteDto = new ClubConstituteDto();
            BeanUtils.copyProperties(clubConstitute,clubConstituteDto);
            clubConstituteDtoList.add(clubConstituteDto);
        });
        return clubConstituteDtoList;
    }

    @Override
    public List<ClubConstituteDto> getClubChildByParentId(Long parentId) {
        LambdaQueryWrapper<ClubConstitute> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubConstitute::getParentId,parentId);
        List<ClubConstitute> clubConstituteList = this.baseMapper.selectList(queryWrapper);
        List<ClubConstituteDto> clubConstituteDtoList = new ArrayList<>();
        clubConstituteList.stream().forEach(clubConstitute -> {
            ClubConstituteDto clubConstituteDto = new ClubConstituteDto();
            BeanUtils.copyProperties(clubConstitute,clubConstituteDto);
            clubConstituteDtoList.add(clubConstituteDto);
        });
        return clubConstituteDtoList;

    }

    @Override
    public boolean insertClubConstitute(ClubConstitute clubConstitute) {
        //判断是否存在name  存在即为根节点(用户) 不存在反之
        if(StringUtils.isEmpty(clubConstitute.getName())){
            //为空 ：不是根节点
            //补全 value
            //获取同类总数
            LambdaQueryWrapper<ClubConstitute> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(ClubConstitute::getParentId,clubConstitute.getParentId());
            Integer count = this.baseMapper.selectCount(queryWrapper);
            //2247446675 + 3
            //value = 22474466754
            String value = String.valueOf(clubConstitute.getParentId())+String.valueOf(count+1);
            clubConstitute.setValue(Long.valueOf(value));
            int insert = this.baseMapper.insert(clubConstitute);
            return insert==1 ? true:false;
        }

        //不为空 ：是根节点
        //先判断该用户 是否存在
        UserInfo userInfo = userInfoController.getUserInfoByUserId(clubConstitute.getValue());
        if(ObjectUtils.isEmpty(userInfo)){
            //为空：不存在该用户
            return false;
        }
        //不为空：存在用户 判断信息是否正确
//        boolean equals = StringUtils.equals(clubConstitute.getName(), userInfo.get());
//        if(equals==false){
//            //不相等
//            return false;
//        }
        //等同 信息正确 添加入库
        int insert = this.baseMapper.insert(clubConstitute);
        return insert==1 ? true:false;

    }

    @Override
    public boolean deleteClubConstitute(Long value) {

        //删除 value的信息
        this.baseMapper.deleteClubConstituteByValue(value);
        //查询父id为value的所有
        List<ClubConstitute> list = this.baseMapper.selectAllByParentId(value);
        //无子元素
        if(list.size()==0){
            return true;
        }
        //有子元素
        //判断子元素是否存在子元素
        list.stream().forEach(clubConstitute -> {
            Long valueId = clubConstitute.getValue();
            //还存在子元素 递归
            deleteClubConstitute(valueId);
        });

        return true;
    }

    @Override
    public List<ClubInfo> getUserClubListByUserId(Long userId) {
        List<Long> valueList = getParentValueByChildValue(userId);
        if(valueList.size()==0 || ObjectUtils.isEmpty(valueList)){
            //没有父节点/未加入组织
            return null;
        }
        List<ClubInfo> clubDtoList = new ArrayList<>();
        valueList.stream().forEach(value -> {
            ClubInfo clubInfo = clubInfoService.getClubInfo(value);
            clubDtoList.add(clubInfo);
        });
        return clubDtoList;
    }

    @Override
    public List<Long> getParentValueByChildValue(Long value){
        List<Long> valueList = new ArrayList<>();
        List<Long> parentIds = this.baseMapper.getParentIdByValue(value);
        if(parentIds.size()==0|| ObjectUtils.isEmpty(parentIds)){
            return null;
        }
/*        for (Long parentId : parentIds) {
            LambdaQueryWrapper<ClubConstitute> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(ClubConstitute::getValue,parentId);
            ClubConstitute clubConstitute = this.baseMapper.selectOne(queryWrapper);
            if(ObjectUtils.isEmpty(clubConstitute)){
                //确定为父节点 组织
                valueList.add(value);
                return valueList;
            }
        }*/
        parentIds.stream().forEach(parentId -> {
            List<Long> list = getParentValueByChildValue(parentId);
            if(ObjectUtils.isEmpty(list)){
                valueList.add(value);
            }else{
                list.stream().forEach(item -> {
                    valueList.add(item);
                });
            }

        });
        return valueList;
    }


}




