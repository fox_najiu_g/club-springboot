package com.fox0g.club.club_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.dto.ClubTagDto;
import com.fox0g.club.model.dto.ClubsDto;
import com.fox0g.club.model.po.ClubTag;


import java.util.List;

/**
* @author 86187
* @description 针对表【club_tag】的数据库操作Service
* @createDate 2023-03-02 14:36:58
*/
public interface ClubTagService extends IService<ClubTag> {

    List<ClubTagDto> getClubTagByTagTypeClubId(Integer tagType, Long clubId);

    boolean deleteClubTagByIdClubId(Integer id, Long clubId);


    int getClubTagCountByTagType(Integer tagType, Long clubId);

    List<ClubsDto> getClubByClubTag(Integer tagType, String tagValue);
}
