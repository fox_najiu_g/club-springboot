package com.fox0g.club.club_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.dto.ClubConstituteDto;
import com.fox0g.club.model.po.ClubConstitute;
import com.fox0g.club.model.po.ClubInfo;


import java.util.List;

/**
* @author 86187
* @description 针对表【club_constitute】的数据库操作Service
* @createDate 2023-03-01 13:59:23
*/
public interface ClubConstituteService extends IService<ClubConstitute> {

    /*
     * @Description: 根据clubId获取父节点
     * @Param: clubId
     * @return: List<ClubConstituteDto>
     * @Date: 2023/3/1
    */
    List<ClubConstituteDto> getClubParentByParentId(Long parentId, Long value);

    /*
     * @Description: 根据value获取子节点的信息
     * @Param: parentId
     * @return: List<ClubConstituteDto>
     * @Date: 2023/3/1
    */
    List<ClubConstituteDto> getClubChildByParentId(Long parentId);

    /*
     * @Description: 新增club 成员/部门
     * @Param: clubConstitute
     * @return: boolean
     * @Date: 2023/3/1
    */
    boolean insertClubConstitute(ClubConstitute clubConstitute);

    boolean deleteClubConstitute(Long value);

    List<ClubInfo> getUserClubListByUserId(Long userId);

    List<Long> getParentValueByChildValue(Long value);
}
