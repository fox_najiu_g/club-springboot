package com.fox0g.club.club_service.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.fox0g.club.model.dto.ClubApplyDto;
import com.fox0g.club.model.dto.ClubApplyUserDto;
import com.fox0g.club.model.po.ClubApply;
import com.fox0g.club.model.vo.ClubApplyVo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;

/**
* @author 86187
* @description 针对表【club_apply】的数据库操作Service
* @createDate 2023-03-01 21:08:25
*/
public interface ClubApplyService extends IService<ClubApply> {

    /*
     * @Description: 根据clubId 分页获取申请信息
     * @Param: clubId
 * @param pageParam
     * @return: PageInfo<ClubApply>
     * @Date: 2023/3/1
    */
    PageInfo<ClubApplyDto> getAllApplyByClubId(Long clubId, PageParamVo pageParam);

    /*
     * @Description: 根据id 修改申请状态
     * @Param: id
 * @param applyStatus
     * @return: boolean
     * @Date: 2023/3/1
    */
    boolean updateStatusById(Integer id, Integer applyStatus);

    /*
     *
     *  管理员
    */
    PageInfo<ClubApplyUserDto> selectAllClubAllApplyUser(PageParamVo pageParamVo);

    boolean insertClubApply(ClubApplyVo clubApplyVo);
}
