package com.fox0g.club.club_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fox0g.club.club_service.mapper.ClubAccountMapper;
import com.fox0g.club.club_service.service.ClubAccountService;
import com.fox0g.club.model.po.ClubAccount;
import org.springframework.stereotype.Service;

/**
* @author 86187
* @description 针对表【club_account】的数据库操作Service实现
* @createDate 2023-02-26 22:23:00
*/
@Service
public class ClubAccountServiceImpl extends ServiceImpl<ClubAccountMapper, ClubAccount>
    implements ClubAccountService {

    @Override
    public ClubAccount getByClubId(Long clubId) {
        LambdaQueryWrapper<ClubAccount> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubAccount::getClubId,clubId);
        return this.baseMapper.selectOne(queryWrapper);
    }

    @Override
    public boolean updateClubAccount(ClubAccount clubAccount) {
        //调用之前 所有参数都必须先校验合法
        //合法使用
        //clubAccount必定存在 clubId
        LambdaQueryWrapper<ClubAccount> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubAccount::getClubId,clubAccount.getClubId());
        int isUpdate = this.baseMapper.update(clubAccount, queryWrapper);
        return isUpdate==1 ? true:false;

    }

    @Override
    public Integer getClubAccountIsJoin(Long clubId) {

        Integer isJoin = this.baseMapper.getClubAccountIsJoin(clubId);
        return isJoin;
    }

    @Override
    public Boolean updateClubAccountIsJoin(Long clubId, Integer clubIsJoin) {

        Boolean isUpdate = this.baseMapper.updateClubIsJoin(clubIsJoin, clubId);
        return isUpdate;
    }
}




