package com.fox0g.club.club_service.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.fox0g.club.model.dto.AllClubVo;
import com.fox0g.club.model.dto.ApplyClubDto;
import com.fox0g.club.model.dto.ClubInfoDto;
import com.fox0g.club.model.dto.ClubsDto;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.ShowClub;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
* @author 86187
* @description 针对表【club_info】的数据库操作Service
* @createDate 2023-02-25 21:58:48
*/
public interface ClubInfoService extends IService<ClubInfo> {

    /*
     * @Description: 根据用户id 添加一个 club
     * @Param: userId
     * @param clubVo
     * @return: boolean
     * @Date: 2023/2/25
    */
    boolean addClubInfo(Long userId, Object object);

    List<AllClubVo> getByUserId(Long userId);

    /*
     * @Description: 根据用户id获取用户信息
     * @Param: userId
     * @return: AllClubVo
     * @Date: 2023/3/3
    */
    ClubInfo getClubInfo(Long clubId);

    boolean updateClubInfo(ClubInfo clubInfo);

    PageInfo<ClubInfoDto> selectAllClubList(PageParamVo pageParamVo);

    PageInfo<ApplyClubDto> selectAllApplyClubList(PageParamVo pageParamVo);

    PageInfo<ApplyClubDto> selectAllApplyingClubList(PageParamVo pageParamVo);

    void multipleUpdateApplyingClubStatus(List<Integer> ids, Integer applyStatus);

    boolean deleteClubByClubId(Long clubId);

    List<ShowClub> getClubList();

    ShowClub getPartClubInfoByClubId(Long clubId);

    PageInfo<ClubsDto> getClubListBySearch(String searchParam, Integer pageNum, Integer pageSize);
}
