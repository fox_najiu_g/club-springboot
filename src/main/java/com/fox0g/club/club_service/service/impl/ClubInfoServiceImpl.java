package com.fox0g.club.club_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.base_util.utils.RandomIdUtil;
import com.fox0g.club.club_service.mapper.ClubInfoMapper;
import com.fox0g.club.club_service.mapper.ClubTagMapper;
import com.fox0g.club.club_service.service.ClubAccountService;
import com.fox0g.club.club_service.service.ClubConstituteService;
import com.fox0g.club.club_service.service.ClubInfoService;
import com.fox0g.club.model.dto.AllClubVo;
import com.fox0g.club.model.dto.ApplyClubDto;
import com.fox0g.club.model.dto.ClubInfoDto;
import com.fox0g.club.model.dto.ClubsDto;
import com.fox0g.club.model.po.ClubAccount;
import com.fox0g.club.model.po.ClubConstitute;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.ShowClub;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
* @author 86187
* @description 针对表【club_info】的数据库操作Service实现
* @createDate 2023-02-25 21:58:48
*/
@Service
public class ClubInfoServiceImpl extends ServiceImpl<ClubInfoMapper, ClubInfo>
    implements ClubInfoService {

    @Autowired
    private ClubInfoService clubInfoService;
    @Autowired
    private ClubAccountService clubAccountService;
    @Autowired
    private ClubTagMapper clubTagMapper;
    @Autowired
    private ClubConstituteService clubConstituteService;

    @Override
    public boolean addClubInfo(Long userId, Object object) {

        ClubInfo clubInfo = new ClubInfo();

        BeanUtils.copyProperties(object,clubInfo);
        clubInfo.setUserId(userId);
        clubInfo.setApplyStatus(3);
        clubInfo.setApplyDate(LocalDate.now());
        clubInfo.setClubId(RandomIdUtil.getRandomId("22"));
        int insert = this.baseMapper.insert(clubInfo);

        return insert == 1 ? true : false;
    }

    @Override
    public List<AllClubVo> getByUserId(Long userId) {

        return this.baseMapper.getAllClubIdName(userId);

    }

    @Override
    public ClubInfo getClubInfo(Long clubId) {

        LambdaQueryWrapper<ClubInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubInfo::getClubId,clubId);
        ClubInfo clubInfo = this.baseMapper.selectOne(queryWrapper);

        return clubInfo;
    }

    @Override
    public boolean updateClubInfo(ClubInfo clubInfo) {
        int isUpdate = this.baseMapper.updateById(clubInfo);
        return isUpdate == 1 ? true:false;
    }

    @Override
    public PageInfo<ClubInfoDto> selectAllClubList(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(),pageParamVo.getPageSize());
        List<ClubInfoDto> userDtoList = this.baseMapper.getAllClubList();
        PageInfo<ClubInfoDto> pageInfo = new PageInfo<>(userDtoList);
        return pageInfo;
    }

    @Override
    public PageInfo<ApplyClubDto> selectAllApplyClubList(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(),pageParamVo.getPageSize());
        List<ApplyClubDto> clubDtoList = this.baseMapper.getApplyClubList();
        PageInfo<ApplyClubDto> pageInfo = new PageInfo<>(clubDtoList);
        return pageInfo;
    }

    @Override
    public PageInfo<ApplyClubDto> selectAllApplyingClubList(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(),pageParamVo.getPageSize());
        List<ApplyClubDto> clubDtoList = this.baseMapper.getApplyingClubList();
        PageInfo<ApplyClubDto> pageInfo = new PageInfo<>(clubDtoList);
        return pageInfo;
    }

    @Override
    public void multipleUpdateApplyingClubStatus(List<Integer> ids, Integer applyStatus) {
        //批量更新isJoin
        ids.stream().forEach(id -> {
            ClubInfo clubInfo = clubInfoService.getById(id);
            clubInfo.setClubRegistration(LocalDate.now());
            clubInfo.setApplyStatus(applyStatus);
            boolean isUpdate = clubInfoService.updateClubInfo(clubInfo);
            if(isUpdate){
                ClubAccount clubAccount = new ClubAccount();
                clubAccount.setClubId(clubInfo.getClubId());
                clubAccount.setClubIsJoin(1);
                clubAccount.setUserId(clubInfo.getUserId());
                clubAccountService.save(clubAccount);

                ClubConstitute clubConstitute = new ClubConstitute();
                clubConstitute.setParentId(0L);
                clubConstitute.setLabel(clubInfo.getClubName());
                clubConstitute.setValue(clubInfo.getClubId());
                clubConstitute.setIsLeaf(0);
                clubConstituteService.save(clubConstitute);
            }
        });
    }

    @Override
    public boolean deleteClubByClubId(Long clubId) {
        LambdaQueryWrapper<ClubInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubInfo::getClubId,clubId);
        int isDelete = this.baseMapper.delete(queryWrapper);
        return isDelete==1 ? true:false;
    }

    @Override
    public List<ShowClub> getClubList() {
        List<ShowClub> showClubList = this.baseMapper.getShowClubList();
        return showClubList;
    }

    @Override
    public ShowClub getPartClubInfoByClubId(Long clubId) {
        ShowClub partClubInfoByClubId = this.baseMapper.getPartClubInfoByClubId(clubId);
        return partClubInfoByClubId;
    }

    @Override
    public PageInfo<ClubsDto> getClubListBySearch(String searchParam, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Long> ids = this.baseMapper.getClubIdListBySearchParam(searchParam);
        List<ClubsDto> list = new ArrayList<>();
        ids.stream().forEach(clubId -> {
            ClubsDto clubsDto = new ClubsDto();
            ClubInfo clubInfo = clubInfoService.getClubInfo(clubId);
            BeanUtils.copyProperties(clubInfo,clubsDto);
            List<String> tag1Values = clubTagMapper.getTagValueByClubIdTagValue(clubId, 331);
            List<String> tag2Values = clubTagMapper.getTagValueByClubIdTagValue(clubId, 332);
            if(tag2Values.size()>=0 || ObjectUtils.isNotEmpty(tag2Values)){
                tag2Values.stream().forEach(value -> {
                    tag1Values.add(value);
                });
            }
            clubsDto.setTagValue(tag1Values);
            list.add(clubsDto);
        });

        PageInfo<ClubsDto> clubsDtoPageInfo = new PageInfo<>(list);
        return clubsDtoPageInfo;
    }
}




