package com.fox0g.club.club_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.club_service.mapper.ClubTagMapper;
import com.fox0g.club.club_service.service.ClubInfoService;
import com.fox0g.club.club_service.service.ClubTagService;
import com.fox0g.club.model.dto.ClubTagDto;
import com.fox0g.club.model.dto.ClubsDto;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.ClubTag;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @author 86187
* @description 针对表【club_tag】的数据库操作Service实现
* @createDate 2023-03-02 14:36:58
*/
@Service
public class ClubTagServiceImpl extends ServiceImpl<ClubTagMapper, ClubTag>
    implements ClubTagService {

    @Autowired
    private ClubInfoService clubInfoService;

    @Override
    public List<ClubTagDto> getClubTagByTagTypeClubId(Integer tagType, Long clubId) {

        List<ClubTagDto> tagList = this.baseMapper.getClubTagByTagTypeClubId(tagType, clubId);
        return tagList;
    }

    @Override
    public boolean deleteClubTagByIdClubId(Integer id, Long clubId) {

        return this.baseMapper.deleteClubTagByIdAndClubId(id, clubId);
    }

    @Override
    public int getClubTagCountByTagType(Integer tagType, Long clubId) {

        LambdaQueryWrapper<ClubTag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ClubTag::getTagType,tagType).eq(ClubTag::getClubId,clubId);
        int count = this.count(queryWrapper);
        return count;
    }

    @Override
    public List<ClubsDto> getClubByClubTag(Integer tagType, String tagValue) {
        List<Long> ids = this.baseMapper.getClubIdByClubTag(tagType,tagValue);
        List<ClubsDto> list = new ArrayList<>();
        ids.stream().forEach(clubId -> {
            ClubsDto clubsDto = new ClubsDto();
            ClubInfo clubInfo = clubInfoService.getClubInfo(clubId);
            BeanUtils.copyProperties(clubInfo,clubsDto);
            List<String> tagValues = this.baseMapper.getTagValueByClubIdTagValue(clubId, 332);
            clubsDto.setTagValue(tagValues);
            list.add(clubsDto);
        });
        return list;
    }


}




