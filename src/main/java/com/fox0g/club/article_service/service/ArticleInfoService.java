package com.fox0g.club.article_service.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ArticleInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
* @author 86187
* @description 针对表【article_info】的数据库操作Service
* @createDate 2023-02-28 17:14:33
*/
public interface ArticleInfoService extends IService<ArticleInfo> {

    /*
     * @Description: 新增一个动态
     * @Param: articleInfo
     * @return: boolean
     * @Date: 2023/2/28
    */
    boolean addArticleInfo(ArticleInfo articleInfo);

    /*
     * @Description: 分页/根据publishId/userOrClub获取状态为1的动态信息
     * @Param: userOrClub
 * @param publishId
 * @param pageParam
     * @return: PageInfo<ArticleDto>
     * @Date: 2023/2/28
    */
    PageInfo<ArticleDto> getArticleStatus1(Integer userOrClub, Long publishId, PageParamVo pageParam);


    /*
     * @Description: 获取所有动态
     * @Param: userOrClub
 * @param publishId
 * @param pageParam
     * @return: PageInfo<ArticleDto>
     * @Date: 2023/3/4
    */
    PageInfo<ArticleDto> getAllArticle(Integer userOrClub, Long publishId, PageParamVo pageParam);


    /*
     * 管理员
     */

    PageInfo<ArticlePassDto> getAllPassArticle(PageParamVo pageParamVo);

    PageInfo<ArticleApplyDto> selectAllApplyArticle(PageParamVo pageParamVo);

    PageInfo<ArticleApplyDto> selectAllApplyingArticle(PageParamVo pageParamVo);

    void multipleUpdateApplyingArticleStatus(List<Integer> ids, Integer applyStatus);

    List<ArticlePassDto> getArticleShowById(List<Integer> ids);

    List<ArticleInfoDto> getArticleByPublish(Integer userOrClub, Long publishId);

    PageInfo<ArticleInfoListDto> getArticleInfoListByArticleTag1(String tagValue, Integer pageNum, Integer pageSize);

    PageInfo<ArticleInfoListDto> getArticleInfoListBySearch(String searchParam, Integer pageNum, Integer pageSize);
}
