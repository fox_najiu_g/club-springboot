package com.fox0g.club.article_service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.article_service.mapper.ArticleInfoMapper;
import com.fox0g.club.article_service.service.ArticleInfoService;
import com.fox0g.club.club_service.controller.ClubInfoController;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ArticleInfo;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.user_service.controller.UserInfoController;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 86187
* @description 针对表【article_info】的数据库操作Service实现
* @createDate 2023-02-28 17:14:33
*/
@Service
public class ArticleInfoServiceImpl extends ServiceImpl<ArticleInfoMapper, ArticleInfo>
    implements ArticleInfoService {

    @Autowired
    private UserInfoController userFeignClient;

    @Autowired
    private ClubInfoController clubFeignClient;

    @Autowired
    private ArticleInfoService articleInfoService;

    @Override
    public boolean addArticleInfo(ArticleInfo articleInfo) {

        int isAdd = this.baseMapper.insert(articleInfo);
        return isAdd==1 ? true:false;
    }

    @Override
    public PageInfo<ArticleDto> getArticleStatus1(Integer userOrClub, Long publishId, PageParamVo pageParam) {

        PageHelper.startPage(pageParam.getPageNum(),pageParam.getPageSize());
        List<ArticleDto> articleDtoList = this.baseMapper.getArticleStatus1(userOrClub, publishId);
        PageInfo<ArticleDto> pageInfo = new PageInfo<>(articleDtoList);
        return pageInfo;

    }

    @Override
    public PageInfo<ArticleDto> getAllArticle(Integer userOrClub, Long publishId, PageParamVo pageParam) {
        PageHelper.startPage(pageParam.getPageNum(),pageParam.getPageSize());
        List<ArticleDto> articleDtoList = this.baseMapper.getAllArticle(userOrClub,publishId);
        PageInfo<ArticleDto> pageInfo = new PageInfo<>(articleDtoList);
        return pageInfo;
    }

    @Override
    public PageInfo<ArticlePassDto> getAllPassArticle(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(),pageParamVo.getPageSize());
        List<ArticlePassDto> articlePassDtoList = this.baseMapper.getAllPassArticle();
        articlePassDtoList.stream().map(articlePassDto -> {
            Integer userOrClub = articlePassDto.getUserOrClub();
            Long publishId = articlePassDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                articlePassDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                articlePassDto.setPublishName(clubInfo.getClubName());
            }
            return articlePassDto;
        }).collect(Collectors.toList());
        PageInfo<ArticlePassDto> pageInfo = new PageInfo<>(articlePassDtoList);
        return pageInfo;
    }

    @Override
    public PageInfo<ArticleApplyDto> selectAllApplyArticle(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<ArticleApplyDto> articleDtoList = this.baseMapper.getAllApplyArticle();
        articleDtoList.stream().map(articleDto -> {
            Integer userOrClub = articleDto.getUserOrClub();
            Long publishId = articleDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                articleDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                articleDto.setPublishName(clubInfo.getClubName());
            }
            return articleDto;
        }).collect(Collectors.toList());
        PageInfo<ArticleApplyDto> pageInfo = new PageInfo<>(articleDtoList);
        return  pageInfo;
    }

    @Override
    public PageInfo<ArticleApplyDto> selectAllApplyingArticle(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<ArticleApplyDto> articleApplyDtoList = this.baseMapper.getAllApplyingArticle();
        articleApplyDtoList.stream().map(articleDto -> {
            Integer userOrClub = articleDto.getUserOrClub();
            Long publishId = articleDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                articleDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                articleDto.setPublishName(clubInfo.getClubName());
            }
            return articleDto;
        }).collect(Collectors.toList());
        PageInfo<ArticleApplyDto> pageInfo = new PageInfo<>(articleApplyDtoList);
        return  pageInfo;
    }

    @Override
    public void multipleUpdateApplyingArticleStatus(List<Integer> ids, Integer applyStatus) {
        //批量更新isJoin
        ids.stream().forEach(id -> {
            ArticleInfo articleInfo = articleInfoService.getById(id);
            articleInfo.setArticleStatus(applyStatus);
            articleInfo .setPublishDate(LocalDateTime.now());
            boolean isUpdate = articleInfoService.updateById(articleInfo);
        });
    }

    @Override
    public List<ArticlePassDto> getArticleShowById(List<Integer> ids) {
        List<ArticlePassDto> list = new ArrayList<>();
        ids.stream().forEach(id -> {
            ArticleInfo articleInfo = articleInfoService.getById(id);
            ArticlePassDto articlePassDto = new ArticlePassDto();
            BeanUtils.copyProperties(articleInfo,articlePassDto);

            Integer userOrClub = articlePassDto.getUserOrClub();
            Long publishId = articlePassDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                articlePassDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                articlePassDto.setPublishName(clubInfo.getClubName());
            }
            list.add(articlePassDto);

        });

        return list;
    }

    @Override
    public List<ArticleInfoDto> getArticleByPublish(Integer userOrClub, Long publishId) {
        List<ArticleInfoDto> articleInfoDto = this.baseMapper.getArticleListByPublish(userOrClub, publishId);
        return articleInfoDto;
    }

    @Override
    public PageInfo<ArticleInfoListDto> getArticleInfoListByArticleTag1(String tagValue, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ArticleInfoListDto> list = new ArrayList<>();
        if(tagValue.equals("全部")){
            list = this.baseMapper.getAllArticleInfoList();
        }else{
            list = this.baseMapper.getArticleInfoListByArticleTag1(tagValue);
        }
        list.stream().map(articleInfo -> {
            Long publishId = articleInfo.getPublishId();
            Integer userOrClub = articleInfo.getUserOrClub();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                articleInfo.setPublishName(userInfo.getUserName());
                articleInfo.setPublishImage(userInfo.getUserImage());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                articleInfo.setPublishName(clubInfo.getClubName());
                articleInfo.setPublishImage(clubInfo.getClubImage());
            }
            return articleInfo;
        }).collect(Collectors.toList());
        PageInfo<ArticleInfoListDto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<ArticleInfoListDto> getArticleInfoListBySearch(String searchParam, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ArticleInfoListDto> list = new ArrayList<>();
        list = this.baseMapper.getArticleInfoListBySearchParam(searchParam);

        list.stream().map(articleInfo -> {
            Long publishId = articleInfo.getPublishId();
            Integer userOrClub = articleInfo.getUserOrClub();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                articleInfo.setPublishName(userInfo.getUserName());
                articleInfo.setPublishImage(userInfo.getUserImage());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                articleInfo.setPublishName(clubInfo.getClubName());
                articleInfo.setPublishImage(clubInfo.getClubImage());
            }
            return articleInfo;
        }).collect(Collectors.toList());
        PageInfo<ArticleInfoListDto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
}




