package com.fox0g.club.article_service.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ArticleInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【article_info】的数据库操作Mapper
* @createDate 2023-02-28 17:14:33
* @Entity com.fox0g.model.po.ArticleInfo
*/
@Mapper
public interface ArticleInfoMapper extends BaseMapper<ArticleInfo> {

    @Select("select id,article_content,article_image,apply_date,publish_date,article_status from article_info " +
            "where user_or_club=#{userOrClub} and publish_id=#{publishId} and article_status=1 order by publish_date desc")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "article_image",property = "articleImage"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "publish_date",property = "publishDate"),
            @Result(column = "article_status",property = "articleStatus")
    })
    List<ArticleDto> getArticleStatus1(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);

    @Select("select id,article_content,apply_date,publish_date,article_status from article_info where user_or_club=#{userOrClub} and publish_id=#{publishId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "publish_date",property = "publishDate"),
            @Result(column = "article_status",property = "articleStatus")
    })
    List<ArticleDto> getAllArticle(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);


    @Select("select id,publish_id,article_content,publish_date,user_or_club from article_info where article_status=1 ")
    List<ArticlePassDto> getAllPassArticle();

    @Select("select id,publish_id,article_content,apply_date,user_or_club,article_status from article_info")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "article_status",property = "articleStatus"),
    })
    List<ArticleApplyDto> getAllApplyArticle();

    @Select("select id,publish_id,article_content,apply_date,user_or_club,article_status from article_info where article_status=3")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "apply_date",property = "applyDate"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "article_status",property = "articleStatus"),
    })
    List<ArticleApplyDto> getAllApplyingArticle();

    @Select("select id,publish_date,article_content from article_info " +
            "where user_or_club=#{userOrClub} and publish_id=#{publishId} and (article_status=1 or article_status=2) " +
            "order by publish_date desc limit 5")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "publish_date",property = "publishDate"),
            @Result(column = "article_content",property = "articleContent"),
    })
    List<ArticleInfoDto> getArticleListByPublish(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);

    @Select("select id,publish_id,user_or_club,article_content,article_image,article_tag1,article_tag2,publish_date " +
            "from article_info where (article_tag1 like '%${tagValue}%' or article_tag2 like '%${tagValue}%') and (article_status=1 or article_status=2) order by publish_date desc")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "article_image",property = "articleImage"),
            @Result(column = "article_tag1",property = "articleTag1"),
            @Result(column = "article_tag2",property = "articleTag2"),
            @Result(column = "publish_date",property = "publishDate"),
    })
    List<ArticleInfoListDto> getArticleInfoListByArticleTag1(@Param("tagValue")String tagValue);

    @Select("select id,publish_id,user_or_club,article_content,article_image,article_tag1,article_tag2,publish_date " +
            "from article_info where (article_status=1 or article_status=2) order by publish_date desc")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "article_image",property = "articleImage"),
            @Result(column = "article_tag1",property = "articleTag1"),
            @Result(column = "article_tag2",property = "articleTag2"),
            @Result(column = "publish_date",property = "publishDate"),
    })
    List<ArticleInfoListDto> getAllArticleInfoList();


    @Select("select id,publish_id,user_or_club,article_content,article_image,article_tag1,article_tag2,publish_date " +
            "from article_info where (article_content like '%${searchParam}%' " +
            "or article_tag1 like '%${searchParam}%' or article_tag2 like '%${searchParam}%') " +
            "and (article_status=1 or article_status=2) order by publish_date desc")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "article_content",property = "articleContent"),
            @Result(column = "article_image",property = "articleImage"),
            @Result(column = "article_tag1",property = "articleTag1"),
            @Result(column = "article_tag2",property = "articleTag2"),
            @Result(column = "publish_date",property = "publishDate"),
    })
    List<ArticleInfoListDto> getArticleInfoListBySearchParam(String searchParam);
}




