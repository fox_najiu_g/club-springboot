package com.fox0g.club.article_service.controller.api;


import com.fox0g.club.article_service.service.ArticleInfoService;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.ArticleApplyDto;
import com.fox0g.club.model.dto.ArticlePassDto;
import com.fox0g.club.model.po.ArticleInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-06 08:39
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/article/manage")
public class ArticleApiController {

    @Autowired
    private ArticleInfoService articleInfoService;

    @GetMapping("/getAllPassArticle")
    public Result getAllPassedArticle(PageParamVo pageParamVo){

        PageInfo<ArticlePassDto> pageInfo = articleInfoService.getAllPassArticle(pageParamVo);
        return Result.success(pageInfo);
    }

    @GetMapping("/getArticleInfo/{id}")
    public Result getArticleInfo(@PathVariable Integer id){

        ArticleInfo articleInfo = articleInfoService.getById(id);
        return Result.success(articleInfo);
    }

    @DeleteMapping("/deleteArticle/{id}")
    public Result deleteArticle(@PathVariable Integer id){

        boolean isDelete = articleInfoService.removeById(id);
        return isDelete ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @GetMapping("/getAllApplyArticle")
    public Result getAllApplyArticle(PageParamVo pageParamVo){

        PageInfo<ArticleApplyDto> pageInfo = articleInfoService.selectAllApplyArticle(pageParamVo);
        return Result.success(pageInfo);
    }

    @GetMapping("/getAllApplyingArticle")
    public Result getAllApplyingArticle(PageParamVo pageParamVo){

        PageInfo<ArticleApplyDto> pageInfo = articleInfoService.selectAllApplyingArticle(pageParamVo);
        return Result.success(pageInfo);
    }

    @PostMapping("/updateApplyingArticleStatus/{id}/{applyStatus}")
    public Result updateApplyingArticleStatus(@PathVariable("id")Integer id,@PathVariable("applyStatus")Integer applyStatus){
        ArticleInfo articleInfo = articleInfoService.getById(id);
        articleInfo.setArticleStatus(applyStatus);
        articleInfo.setPublishDate(LocalDateTime.now());
        boolean isUpdate = articleInfoService.updateById(articleInfo);
        return isUpdate ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @PostMapping("/multipleUpdateApplyingArticleStatus/{applyStatus}")
    public Result multipleUpdateApplyingArticleStatus(@PathVariable("applyStatus")Integer applyStatus,@RequestBody List<Integer> ids){

        articleInfoService.multipleUpdateApplyingArticleStatus(ids,applyStatus);
        return Result.success("操作成功");

    }
}
