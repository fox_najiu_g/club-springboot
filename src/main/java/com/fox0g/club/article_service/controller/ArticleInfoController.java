package com.fox0g.club.article_service.controller;


import com.fox0g.club.article_service.service.ArticleInfoService;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.ArticleDto;
import com.fox0g.club.model.dto.ArticleInfoDto;
import com.fox0g.club.model.dto.ArticleInfoListDto;
import com.fox0g.club.model.dto.ArticlePassDto;
import com.fox0g.club.model.po.ArticleInfo;
import com.fox0g.club.model.vo.ArticleInfoVo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-02-28 17:16
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/article")
public class ArticleInfoController {


    @Autowired
    private ArticleInfoService articleInfoService;

    /*
     * @Description: 根据publishId
     * @Param: clubId
 * @param articleInfoVo
     * @return: Result
     * @Date: 2023/2/28
    */
    @PutMapping("/addArticleInfo/{publishId}")
    public Result addArticleInfo(@PathVariable Long publishId,
                                @RequestBody ArticleInfoVo articleInfoVo){
        //publishId 是否合法
        if(Long.toString(publishId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        ArticleInfo articleInfo = new ArticleInfo();
        BeanUtils.copyProperties(articleInfoVo,articleInfo);
        articleInfo.setPublishId(publishId);
        articleInfo.setArticleStatus(3);
        articleInfo.setApplyDate(LocalDateTime.now());
        boolean isAdd = articleInfoService.addArticleInfo(articleInfo);
        return isAdd ? Result.success("创建成功，审核中..."):Result.fail("失败，请稍后重试");
    }

    /*
     * @Description: 分页/根据publishId/userOrClub获取已通过(1)的动态
     * @Param: userOrClub
 * @param publishId
 * @param pageParam
     * @return: Result
     * @Date: 2023/2/28
    */
    @GetMapping("/getArticleStatus1/{userOrClub}/{publishId}")
    public Result getArticleStatus1(@PathVariable("userOrClub") Integer userOrClub,
                                    @PathVariable("publishId") Long publishId,
                                    PageParamVo pageParam){
        //校验publishId 是否合法
        if(Long.toString(publishId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        PageInfo<ArticleDto> articleDtoList = articleInfoService.getArticleStatus1(userOrClub, publishId,pageParam);
        return Result.success("获取成功",articleDtoList);
    }

    /*
     * @Description: 分页/根据publishId/userOrClub获取所有动态
     * @Param: userOrClub
 * @param publishId
 * @param pageParam
     * @return: Result
     * @Date: 2023/2/28
    */
    @GetMapping("/getAllArticle/{userOrClub}/{publishId}")
    public Result getAllArticle(@PathVariable("userOrClub") Integer userOrClub,
                                    @PathVariable("publishId") Long publishId,
                                    PageParamVo pageParam){
        //校验publishId 是否合法
        if(Long.toString(publishId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        PageInfo<ArticleDto> articleDtoList = articleInfoService.getAllArticle(userOrClub, publishId,pageParam);
        return Result.success("获取成功",articleDtoList);
    }

    @DeleteMapping("/deleteArticleById/{id}")
    public Result deleteArticleById(@PathVariable("id")Integer id){

        boolean isDelete = articleInfoService.removeById(id);
        return isDelete ? Result.success("删除成功"):Result.fail("删除失败");
    }

    @PostMapping("/getArticleShowById")
    public Result getArticleShowById(@RequestBody List<Integer> ids){

        List<ArticlePassDto> list = articleInfoService.getArticleShowById(ids);
        return Result.success(list);
    }

    @GetMapping("/getArticleInfo/{id}")
    public Result getArticleInfo(@PathVariable Integer id){
        ArticleInfo articleInfo = articleInfoService.getById(id);
        return Result.success(articleInfo);
    }

    @GetMapping("/getArticleByPublish/{userOrClub}/{publishId}")
    public Result getArticleByPublish(@PathVariable("userOrClub")Integer userOrClub,@PathVariable("publishId")Long publishId){
        List<ArticleInfoDto> activeDtoList = articleInfoService.getArticleByPublish(userOrClub,publishId);
        return Result.success(activeDtoList);
    }

    @GetMapping("/getArticleInfoByArticleTag1/{tag1Value}")
    public Result getArticleInfoByArticleTag1(@PathVariable("tag1Value") String tagValue,PageParamVo pageParam){

        PageInfo<ArticleInfoListDto> list = articleInfoService.getArticleInfoListByArticleTag1(tagValue,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }

    @GetMapping("/searchArticleListByParam/{searchParam}")
    public Result searchArticleListByParam(@PathVariable String searchParam,PageParamVo pageParam){

        PageInfo<ArticleInfoListDto> list = articleInfoService.getArticleInfoListBySearch(searchParam,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }

}
