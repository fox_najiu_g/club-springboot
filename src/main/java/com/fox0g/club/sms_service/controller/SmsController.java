package com.fox0g.club.sms_service.controller;



import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.base_util.utils.PhoneUtil;
import com.fox0g.club.base_util.validation.ValidationGroups;
import com.fox0g.club.model.vo.CheckCodeVo;
import com.fox0g.club.redis_util.utils.RedisUtil;
import com.fox0g.club.sms_service.service.SmsService;
import com.fox0g.club.sms_service.utils.RandomUtil;
import com.fox0g.club.user_service.controller.UserAccountController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

/**
 * @program: club-parent
 * @Date: 2023-02-24 15:50
 * @author: Fox0g
 * @description: TODO
 */

@Slf4j
@RestController
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private SmsService service;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private UserAccountController userAccountController;

    @GetMapping("/send/{phone}")
    public Result sendCheckCode(@PathVariable String phone){

    //1.校验手机号
        Boolean matches = PhoneUtil.isMatches(phone);
    //2.1不合法
        if(matches == false){
            return Result.fail("手机号不合法");
        }
    //2.2合法
        //2.2.1在redis 查 是否已经存在
        Boolean hasKey = redisUtil.hasKey("sms:" + phone);
        if(BooleanUtils.isTrue(hasKey)){
            //redis中存在 不用重复发送了
            return Result.fail("请勿重复发送(2分钟)");
        }
        //2.2.2redis中不存在 发送验证码
        //2.2.2.1生成 验证码
        String code = RandomUtil.getSixBitRandom();
        log.info("验证码("+phone+"):"+code);
        //2.2.2.2发送验证码
        boolean isSend = service.send(phone, code);
        if(!isSend){
            //发送失败
            return Result.fail("发送失败，请重试");
        }
        //发送成功
        //将 验证码 保存到redis中
        redisUtil.set("sms:"+phone,code,2L, TimeUnit.MINUTES);
        return Result.success("成功发送");
    }

    @PostMapping("/verify")
    public Result verifyCheckCode(@Validated(ValidationGroups.CheckCode.class) @RequestBody CheckCodeVo checkCodeVo){

        //经验证 请求体都合法
        String code = redisUtil.get("sms:" + checkCodeVo.getPhone());
        if(StringUtils.isEmpty(code)){
            //验证码过期
            return Result.fail("验证码过期，请重新获取");
        }
        //存在验证码 对比
        boolean isTrue = StringUtils.equals(code, checkCodeVo.getCode());
        if(isTrue == false){
            return Result.fail("验证码错误");
        }
        //验证码正确
        //注册用户
        String password = userAccountController.addUserBuUserPhone(checkCodeVo.getPhone());
        if(StringUtils.isEmpty(password)){
            //已注册
            return Result.fail("手机号已注册");
        }

        return Result.success("注册成功,初始密码为:"+password,password);
    }


}
