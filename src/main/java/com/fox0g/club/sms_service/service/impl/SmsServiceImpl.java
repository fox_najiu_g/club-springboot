package com.fox0g.club.sms_service.service.impl;/**
 * @program: club-parent
 * @Date: 2023-02-24 20:40
 * @author: Fox0g
 * @description: TODO
 */



import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.fox0g.club.base_util.utils.JsonUtil;
import com.fox0g.club.model.dto.CheckCodeDto;
import com.fox0g.club.sms_service.service.SmsService;
import com.fox0g.club.sms_service.utils.ConstantPropertiesUtils;
import com.google.gson.Gson;

import darabonba.core.client.ClientOverrideConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * @author 86187
 * @version 1.0
 * @description TODO
 * @date 2023/2/24 20:40
 */
@Service
public class SmsServiceImpl implements SmsService {




    @Override
    public boolean send(String phone, String code) {

        StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(ConstantPropertiesUtils.ACCESS_KEY_ID)
                .accessKeySecret(ConstantPropertiesUtils.SECRECT)
                //.securityToken("<your-token>") // use STS token
                .build());

        AsyncClient client = AsyncClient.builder()
                .region(ConstantPropertiesUtils.REGION_Id) // Region ID
                //.httpClient(httpClient) // Use the configured HttpClient, otherwise use the default HttpClient (Apache HttpClient)
                .credentialsProvider(provider)
                //.serviceConfiguration(Configuration.create()) // Service-level configuration
                // Client-level configuration rewrite, can set Endpoint, Http request parameters, etc.
                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                .setEndpointOverride("dysmsapi.aliyuncs.com")
                        //.setConnectTimeout(Duration.ofSeconds(30))
                )
                .build();
        // Parameter settings for API request
        CheckCodeDto checkCode= new CheckCodeDto(code);
        SendSmsRequest sendSmsRequest = SendSmsRequest.builder()
                .signName(ConstantPropertiesUtils.SIGN_NAME)
                .templateCode(ConstantPropertiesUtils.TEMPLATE_CODE)
                .phoneNumbers(phone)
                .templateParam(JsonUtil.objectToJson(checkCode))
                // Request-level configuration rewrite, can set Http request parameters, etc.
                // .requestConfiguration(RequestConfiguration.create().setHttpHeaders(new HttpHeaders()))
                .build();
        // Asynchronously get the return value of the API request
        CompletableFuture<SendSmsResponse> response = client.sendSms(sendSmsRequest);
        // Synchronously get the return value of the API request
        SendSmsResponse resp = null;
        try {
            resp = response.get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(new Gson().toJson(resp));
        // Asynchronous processing of return values
        if(StringUtils.equals(resp.getBody().getCode(),"OK")){
            //发送成功
            client.close();
            return true;
        }
        // Finally, close the client
        client.close();
        //发送失败
        return false;
    }
}
