package com.fox0g.club.sms_service.service;
/**
 * @program: club-parent
 * @Date: 2023-02-24 20:38
 * @author: Fox0g
 * @description: TODO
 */

public interface SmsService {
    /*
     * @Description: 发送验证码
     * @Param: phone
     * @param code
     * @return: boolean
     * @Date: 2023/2/24
    */
    boolean send(String phone, String code);
}
