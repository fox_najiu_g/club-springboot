package com.fox0g.club.tag_service.controller.api;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.po.TagInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.tag_service.service.TagInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-06 18:19
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/tag/manage")
public class TagApiController {

    @Autowired
    private TagInfoService tagInfoService;

    @GetMapping("/getTagList/{tagType}")
    public Result getTagList(@PathVariable("tagType")Integer tagType, PageParamVo pageParamVo){
        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<TagInfo> allTagByTagType = tagInfoService.getAllTagByTagType(tagType);
        PageInfo<TagInfo> pageInfo = new PageInfo<>(allTagByTagType);
        return Result.success(pageInfo);

    }

    @PutMapping("/addTag/{addTagType}/{addTagValue}")
    public Result addTagByAddTagTypeAddTagValue(@PathVariable("addTagType")Integer addTagType,@PathVariable("addTagValue")String addTagValue){

        TagInfo tagInfo = new TagInfo(addTagType,addTagValue);
        boolean isAdd = tagInfoService.save(tagInfo);
        return isAdd ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @DeleteMapping("/deleteTagById/{id}")
    public Result deleteTagById(@PathVariable Integer id){
        boolean isDelete = tagInfoService.removeById(id);
        return isDelete ? Result.success("操作成功"):Result.fail("操作失败，请稍后重试");
    }

    @GetMapping("/getAllTag/{tagType}")
    public Result getAllTag(@PathVariable("tagType")Integer tagType){
        List<TagInfo> allTagByTagType = tagInfoService.getAllTagByTagType(tagType);
        return Result.success(allTagByTagType);
    }
}
