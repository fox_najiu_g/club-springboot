package com.fox0g.club.tag_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.po.TagInfo;
import com.fox0g.club.tag_service.service.TagInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-02-27 13:07
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/tag")
public class TagInfoController {

    @Autowired
    private TagInfoService tagInfoService;

    @GetMapping("/getAllTag/{tagType}")
    public Result getAllTag(@PathVariable Integer tagType){


        List<TagInfo> tagList = tagInfoService.getAllTagByTagType(tagType);
        return Result.success("成功",tagList);
    }
}
