package com.fox0g.club.tag_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.TagInfo;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 86187
* @description 针对表【tag_info】的数据库操作Mapper
* @createDate 2023-02-27 13:04:44
* @Entity com.fox0g.model.po.TagInfo
*/
@Mapper
public interface TagInfoMapper extends BaseMapper<TagInfo> {

}




