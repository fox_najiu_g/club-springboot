package com.fox0g.club.tag_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.po.TagInfo;
import com.fox0g.club.tag_service.mapper.TagInfoMapper;
import com.fox0g.club.tag_service.service.TagInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 86187
* @description 针对表【tag_info】的数据库操作Service实现
* @createDate 2023-02-27 13:04:44
*/
@Service
public class TagInfoServiceImpl extends ServiceImpl<TagInfoMapper, TagInfo>
    implements TagInfoService {

    @Override
    public List<TagInfo> getAllTagByTagType(Integer tagType) {

        LambdaQueryWrapper<TagInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TagInfo::getTagType,tagType);
        List<TagInfo> tagInfoList = this.baseMapper.selectList(queryWrapper);
        return tagInfoList;
    }
}




