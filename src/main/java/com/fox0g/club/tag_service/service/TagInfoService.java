package com.fox0g.club.tag_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.TagInfo;


import java.util.List;

/**
* @author 86187
* @description 针对表【tag_info】的数据库操作Service
* @createDate 2023-02-27 13:04:44
*/
public interface TagInfoService extends IService<TagInfo> {

    /*
     * @Description: 根据标签类型 查询所有标签
     * @Param: tagType
     * @return: List<TagInfo>
     * @Date: 2023/2/27
    */
    List<TagInfo> getAllTagByTagType(Integer tagType);
}
