package com.fox0g.club.base_util.config;


import com.fox0g.club.base_util.helper.JwtHelper;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.base_util.result.ResultCodeEnum;
import com.fox0g.club.base_util.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

// @Order(-1)
@Slf4j
//@Component
public class AuthorizeFilter implements Filter, Ordered {

    private AntPathMatcher antPathMatcher = new AntPathMatcher();


    /**
     * api接口鉴权失败返回数据
     * @param response
     * @return
     */
    private Mono<Void> out(ServerHttpResponse response, ResultCodeEnum resultCodeEnum) {
        Result result = Result.build(null, resultCodeEnum);
        byte[] bits = JsonUtil.objectToJson(result).getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

    @Override
    public int getOrder() {
        return -1;
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String path = request.getRequestURI();
        System.out.println("请求=="+path);

        String token = request.getHeader("access-control-request-headers");

        //api接口，异步请求，校验用户必须登录
        if(antPathMatcher.match("/**/auth/**",path)) {
            if(StringUtils.isEmpty(token)) {
                //不存在token 未登录
                log.error("token not found-未登录");
                response.setContentType("text/html; charset=utf-8");
                response.getWriter().write(ResultCodeEnum.Login4_ERROR.getMessage());
            }
            //存在token,校验
            boolean isVerify = JwtHelper.verifyToken(token);
            if(!isVerify){
                //token不合法
                log.error("invalid token-token不合法");
                response.setContentType("text/html; charset=utf-8");
                response.getWriter().write(ResultCodeEnum.Login4_ERROR.getMessage());
            }
            //token 合法且正确
            //更新token
            //TODO
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }


    @Override
    public void destroy() {

    }
}
