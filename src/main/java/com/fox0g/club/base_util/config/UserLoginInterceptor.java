package com.fox0g.club.base_util.config;

import com.fox0g.club.base_util.helper.JwtHelper;
import com.fox0g.club.base_util.result.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @program: club-springboot
 * @Date: 2023-04-30 10:47
 * @author: Fox0g
 * @description: TODO
 */
@Slf4j
public class UserLoginInterceptor implements HandlerInterceptor {

    private AntPathMatcher antPathMatcher = new AntPathMatcher();
    /***
     * 在请求处理之前进行调用(Controller方法调用之前)
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getRequestURI();
        log.info("请求="+path);
        String token = request.getHeader("Authorization");
        if(antPathMatcher.match("/**/auth/**",path)) {
            log.error("拦截请求=="+path);
            if(StringUtils.isEmpty(token)) {
                //不存在token 未登录
                log.error("token not found-未登录");
                return false;
            }
            //存在token,校验
            boolean isVerify = JwtHelper.verifyToken(token);
            if(!isVerify){
                //token不合法
                log.error("invalid token-token不合法");
                return false;
            }
        }
        return true;
        //如果设置为false时，被请求时，拦截器执行到此处将不会继续操作
        //如果设置为true时，请求将会继续执行后面的操作
    }

    /***
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        System.out.println("执行了拦截器的postHandle方法");
    }

    /***
     * 整个请求结束之后被调用，也就是在DispatchServlet渲染了对应的视图之后执行（主要用于进行资源清理工作）
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        System.out.println("执行了拦截器的afterCompletion方法");
    }
}



