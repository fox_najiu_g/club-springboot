package com.fox0g.club.base_util.helper;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtHelper {

    //过期时间
    private static final long tokenExpiration = 30*60*1000;
    //签名秘钥
    private static final String tokenSignKey = "jsj5201314";

    //根据参数生成token
    public static String createToken(String userPhone,String userPassword) {
        Map<String, Object> map = new HashMap<>();
        map.put("alg","HS256");
        map.put("typ","JWT");
        String token = JWT.create()
                .withHeader(map)//添加头部
                //添加基本信息到claims
                .withClaim("userPhone",userPhone)
                .withClaim("userPassword",userPassword)
                .withExpiresAt(new Date(System.currentTimeMillis()+tokenExpiration))
                .withIssuedAt(new Date())
                .sign(Algorithm.HMAC256(tokenSignKey));
        return token;
    }

    /**
     * 校验token并解析token
     */
    public static boolean verifyToken(String token) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(tokenSignKey))
                    .build();
            verifier.verify(token);
            return true;

            //decodedJWT.getClaim("属性").asString()  获取负载中的属性值

        } catch (Exception e) {

            log.error(e.getMessage());
            log.error("token解码异常");
            //解码异常则抛出异常
            return false;
        }
    }

    public static long getExpiresAt(String token){
        DecodedJWT decode = JWT.decode(token);
        return decode.getExpiresAt().getTime();
    }



    //根据token字符串得到用户id
    /*public static Long getUserId(String token) {
        if(StringUtils.isEmpty(token)) return null;

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        Integer userId = (Integer)claims.get("userId");
        return userId.longValue();
    }*/

    //根据token字符串得到用户名称
    /*public static String getUserName(String token) {
        if(StringUtils.isEmpty(token)) return "";

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        return (String)claims.get("userName");
    }*/

    public static void main(String[] args) {
        String token = JwtHelper.createToken("18761029163", "lucy");
        System.out.println(token);
        boolean stringClaimMap = JwtHelper.verifyToken(token);
        System.out.println(stringClaimMap);
        DecodedJWT decode = JWT.decode(token);
        System.out.println(decode.getExpiresAt().getTime()-System.currentTimeMillis());
        System.out.println(decode.getClaim("userPhone").toString());
        System.out.println(decode.getClaim("userPassword").toString());

    }
}

