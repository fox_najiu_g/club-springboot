package com.fox0g.club.base_util.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @program: club-parent
 * @Date: 2023-02-26 11:51
 * @author: Fox0g
 * @description: TODO
 */
public class MultipartFileUtils {

    public static File MultipartFileToFile(MultipartFile multipartFile){
        // 获取文件名
        //fox.png
        String fileName = multipartFile.getOriginalFilename();
        // 获取文件后缀
        String prefix = fileName.substring(0,fileName.lastIndexOf("."));
        if(prefix.length()<3){
            prefix=prefix+"-fox";
        }
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        // 若须要防止生成的临时文件重复,能够在文件名后添加随机码

        try {
            File file = File.createTempFile(prefix, suffix);
            multipartFile.transferTo(file);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
