package com.fox0g.club.base_util.utils;

import org.apache.commons.lang3.RandomUtils;

/**
 * @program: club-parent
 * @Date: 2023-02-25 12:35
 * @author: Fox0g
 * @description: TODO
 */
public class RandomIdUtil {

    public static Long getRandomId(String type){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 8; i++) {
            int num = RandomUtils.nextInt(0, 9);
            sb.append(num);
        }
        return Long.valueOf(type+sb);
    }
}
