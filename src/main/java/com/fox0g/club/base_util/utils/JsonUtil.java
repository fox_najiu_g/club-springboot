/**
 * @(#)JsonUtil.java 2014-2-23 下午5:44:19
 */
package com.fox0g.club.base_util.utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fox0g.club.base_util.exception.ClubException;
import com.fox0g.club.base_util.result.ResultCodeEnum;


import java.util.List;
import java.util.Map;

public class JsonUtil {
    
    
    /*
     * @Description: 将对象序列化
     * @Param:  object
     * @return: String
     * @Date: 2023/2/24
    */
    public static String objectToJson(Object object){
        ObjectMapper objectMapper = new ObjectMapper();
        String value = null;
        try {
            value = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            System.err.print("捕获异常："+e.getMessage());
            new ClubException(ResultCodeEnum.JSON_ERROR);
        }
        return value;
    }
    

    /*
     * @Description: 将list集合序列化
     * @Param: list
     * @return: String
     * @Date: 2023/2/24
    */
    public static String listToJson(List list){

        ObjectMapper objectMapper = new ObjectMapper();
        String value = null;
        try {
            value = objectMapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            System.err.print("捕获异常："+e.getMessage());
            new ClubException(ResultCodeEnum.JSON_ERROR);
        }
        return value;

    }

    /*
     * @Description: 将map集合 序列化
     * @Param: map
     * @return: String
     * @Date: 2023/2/24
    */
    public static String mapToJson(Map map){

        ObjectMapper objectMapper = new ObjectMapper();
        String value = null;
        try {
            value = objectMapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            System.err.print("捕获异常："+e.getMessage());
            new ClubException(ResultCodeEnum.JSON_ERROR);
        }
        return value;

    }

    /*
     * @Description: 将Json字符串转成 对象
     * @Param: jsonString
     * @return: Object
     * @Date: 2023/2/24
    */
    public static Object jsonToObject(String jsonString){
        ObjectMapper objectMapper = new ObjectMapper();
        Object value = null;
        try {
            value = objectMapper.readValue(jsonString,Object.class);
        } catch (JsonProcessingException e) {
            System.err.print("捕获异常："+e.getMessage());
            new ClubException(ResultCodeEnum.JSON_ERROR);
        }
        return value;
    }

    /*
     * @Description: 将Json字符串转成List对象
     * @Param: jsonString
     * @return: List
     * @Date: 2023/2/24
    */
    public static List jsonToList(String jsonString){
        ObjectMapper objectMapper = new ObjectMapper();
        List value = null;
        try {
            value = objectMapper.readValue(jsonString,List.class);
        } catch (JsonProcessingException e) {
            System.err.print("捕获异常："+e.getMessage());
            new ClubException(ResultCodeEnum.JSON_ERROR);
        }
        return value;
    }

    /*
     * @Description: 将Json字符串转成 map对象
     * @Param: jsonString
     * @return: Map<String,Object>
     * @Date: 2023/2/24
    */
    public static Map<String, Object> jsonToMap(String jsonString){
        ObjectMapper objectMapper = new ObjectMapper();
        Map value = null;
        try {
            value = objectMapper.readValue(jsonString,Map.class);
        } catch (JsonProcessingException e) {
            System.err.print("捕获异常："+e.getMessage());
            new ClubException(ResultCodeEnum.JSON_ERROR);
        }
        return value;
    }
    

}
