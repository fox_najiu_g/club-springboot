package com.fox0g.club.base_util.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneUtil {
    /**
     * 校验用户手机号是否合法
     * @param phone
     * @return
     */
    public static Boolean isMatches(String phone){
        //String regex = "((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}";
        String regex = "(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(phone);
        return m.matches();



    }

    //public static void main(String[] args) {
    //    System.out.println(isMatches("13512341233"));
    //}
}
