package com.fox0g.club.base_util.exception;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fox0g.club.base_util.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/** 
 * @Description: 控制器增强
 * @author: Fox0g
 * @Date: 2023/2/24
*/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    
    //处理Exception异常  此类异常是程序员主动抛出的，可预知异常
    @ExceptionHandler(Exception.class)
    public Result error(Exception e) {
        log.error("捕获异常：{}",e.getMessage());
        e.printStackTrace();
        return Result.fail();
    }



    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result exceptionHandler(MethodArgumentNotValidException exception){
        BindingResult result = exception.getBindingResult();
        StringBuilder stringBuilder = new StringBuilder();
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            if (errors != null) {
                errors.forEach(p -> {
                    FieldError fieldError = (FieldError) p;
                    log.warn("Bad Request Parameters: dto entity [{}],field [{}],message [{}]",fieldError.getObjectName(), fieldError.getField(), fieldError.getDefaultMessage());
                    stringBuilder.append(fieldError.getDefaultMessage());
                });
            }
        }
        return Result.fail(stringBuilder);
    }

    //处理ClubException异常  此类异常是程序员主动抛出的，可预知异常
    @ExceptionHandler(ClubException.class)
    public Result error(ClubException e) {
        log.error("捕获异常：{}",e.getMessage());
        e.printStackTrace();
        return Result.fail(e.getMessage());
    }

    //处理JsonProcessingException异常  此类异常是程序员主动抛出的，可预知异常
    @ExceptionHandler(JsonProcessingException.class)
    public Result error(JsonProcessingException e) {
        log.error("捕获异常：{}",e.getMessage());
        e.printStackTrace();
        return Result.fail();
    }

}
