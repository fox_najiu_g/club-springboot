package com.fox0g.club.base_util.exception;



import com.fox0g.club.base_util.result.ResultCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 自定义全局异常
 * @author Fox0g
 * @date 2023/2/24 13:31
*/
@Data
@ApiModel(value = "自定义全局异常类")
public class ClubException extends RuntimeException {

    @ApiModelProperty(value = "异常状态码")
    private Integer code;

    /**
     * 通过状态码和错误消息创建异常对象
     * @param message
     * @param code
     */
    public ClubException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    /**
     * 接收枚举类型对象
     * @param resultCodeEnum
     */
    public ClubException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "ClubException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
