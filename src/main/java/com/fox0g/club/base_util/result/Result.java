package com.fox0g.club.base_util.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @description 全局统一返回结果类
 * @author Fox0g
 * @date 2023/2/24 13:30
*/
@Data
@ApiModel(value = "全局统一返回结果")
public class Result<T> {

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private T data;

    public Result(){}

    //只封装 data
    protected static <T> Result<T> build(T data) {
        Result<T> result = new Result<T>();
        if (data != null)
            result.setData(data);
        return result;
    }

    public static <T> Result<T> build(T body, ResultCodeEnum resultCodeEnum) {
        Result<T> result = build(body);
        result.setCode(resultCodeEnum.getCode());
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }
    public static <T> Result<T> build(String msg,T body, ResultCodeEnum resultCodeEnum) {
        Result<T> result = build(body);
        result.setCode(resultCodeEnum.getCode());
        result.setMessage(msg);
        return result;
    }

    public static <T> Result<T> build(Integer code, String message) {
        Result<T> result = build(null);
        result.setCode(code);
        result.setMessage(message);
        return result;
    }



    /**
     * 操作成功
     */
    public static<T> Result<T> success(){
        return Result.success(null);
    }
    public static<T> Result<T> success(T data){
        Result<T> result = build(data);
        return build(data, ResultCodeEnum.SUCCESS);
    }
    public static<T> Result<T> success(String msg){
        return build(msg,null, ResultCodeEnum.SUCCESS);
    }
    public static<T> Result<T> success(String msg,T data){
        Result<T> result = build(data);

        return build(msg, data,ResultCodeEnum.SUCCESS);
    }



    /**
     * 操作失败
     */
    public static<T> Result<T> fail(){
        return build(null, ResultCodeEnum.FAIL);
    }
    public static<T> Result<T> fail(String msg){
        return build(msg,null, ResultCodeEnum.FAIL);
    }
    public static<T> Result<T> fail(T data){
        Result<T> result = build(data);
        return build(data, ResultCodeEnum.FAIL);
    }
    public static<T> Result<T> fail(String msg,T data){
        Result<T> result = build(data);
        return build(msg, data, ResultCodeEnum.FAIL);
    }



    public Result<T> message(String msg){
        this.setMessage(msg);
        return this;
    }

    public Result<T> code(Integer code){
        this.setCode(code);
        return this;
    }

    public boolean isSuccess() {
        if(this.getCode().intValue() ==ResultCodeEnum.SUCCESS.getCode().intValue()) {
            return true;
        }
        return false;
    }
}
