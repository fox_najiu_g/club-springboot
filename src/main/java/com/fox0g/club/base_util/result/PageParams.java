package com.fox0g.club.base_util.result;/**
 * @program: club-parent
 * @Date: 2023-02-24 12:03
 * @author: Fox0g
 * @description:
 */

import lombok.Data;

import java.util.List;

/**
 * @description page 对象的包装类
 * @author Fox0g
 * @date 2023/2/24 13:30
*/
@Data
public class PageParams<T> {

    //总记录数
    private long counts;

    //当前页码
    private long page;

    //每页记录数
    private long pageSize;

    // 数据列表
    private List<T> data;

    public PageParams(){}

    public PageParams(List<T> data, long counts, long page, long pageSize) {
        this.data = data;
        this.counts = counts;
        this.page = page;
        this.pageSize = pageSize;
    }
}
