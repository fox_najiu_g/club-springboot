package com.fox0g.club.active_service.controller;


import com.fox0g.club.active_service.service.ActiveInfoService;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ActiveInfo;
import com.fox0g.club.model.po.ShowActive;
import com.fox0g.club.model.vo.ActiveInfoVo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-02-27 10:34
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/active")
public class ActiveInfoController {

    @Autowired
    private ActiveInfoService activeInfoService;

    /*
     * @Description: 根据clubId对应的club新增一个活动
     * @Param: clubId
 * @param activeInfoVo
     * @return: Result
     * @Date: 2023/2/27
    */
    @PutMapping("/addActiveInfo/{clubId}")
    public Result addActiveInfo(@PathVariable Long clubId,
                                @RequestBody ActiveInfoVo activeInfoVo){
        //校验clubId 是否合法
        if(Long.toString(clubId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        ActiveInfo activeInfo = new ActiveInfo();
        BeanUtils.copyProperties(activeInfoVo,activeInfo);
        activeInfo.setApplyDate(LocalDateTime.now());
        activeInfo.setPublishId(clubId);
        activeInfo.setActiveStatus(3);
        boolean isAdd = activeInfoService.addActiveInfo(activeInfo);
        return isAdd ? Result.success("创建成功，审核中..."):Result.fail("失败，请稍后重试");
    }

    /*
     * @Description: 查询 该club 已发布活动状态为1/2 活动中/已结束
     * @Param: clubId
     * @return: Result
     * @Date: 2023/2/27
    */
    @GetMapping("/getActiveStatus12/{userOrClub}/{publishId}")
    public Result getActiveStatus12(@PathVariable("userOrClub") Integer userOrClub,
                                    @PathVariable("publishId") Long publishId,
                                    PageParamVo pageParam){
        //校验publishId 是否合法
        if(Long.toString(publishId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        PageInfo<ActiveStatusDto> activeList = activeInfoService.getActiveStatus12(userOrClub, publishId,pageParam);
        return Result.success("获取成功",activeList);
    }

    /*
     * @Description: 查询 该club 活动状态为1/3/4 活动中/审核中/未通过
     * @Param: userOrClub
     * @param publishId
     * @return: Result
     * @Date: 2023/2/27
    */
    @GetMapping("/getAllActiveApply/{userOrClub}/{publishId}")
    public Result getAllActiveApply(@PathVariable("userOrClub") Integer userOrClub,
                                    @PathVariable("publishId") Long publishId,
                                    PageParamVo pageParam){
        //校验publishId 是否合法
        if(Long.toString(publishId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        PageInfo<ActiveStatusApplyDto> activeList = activeInfoService.getAllActiveApply(userOrClub, publishId,pageParam);
        return Result.success("获取成功",activeList);
    }

    /*
     * @Description: 根据id查到活动，修改其状态
     * @Param: id
 * @param activeStatus
     * @return: Result
     * @Date: 2023/2/27
    */
    @PostMapping("/updateActiveStatus")
    public Result updateActiveStatusById(@RequestParam("id")Integer id,
                                @RequestParam("activeStatus")Integer activeStatus){

        boolean isUpdate = activeInfoService.updateActiveStatusById(id, activeStatus);
        return isUpdate ? Result.success("成功"):Result.fail("失败");
    }

    @DeleteMapping ("/endActiveSubmit")
    public Result endActiveSubmitById(@RequestParam("id")Integer id){

        boolean isDelete = activeInfoService.endActiveById(id);
        return isDelete ? Result.success("成功"):Result.fail("失败");
    }

    @GetMapping("/getActiveShowByActiveTag1/{activeTag1}/{count}")
    public Result getActiveShowByActiveTag1(@PathVariable("activeTag1") String activeTag1,@PathVariable("count")Integer count){
        List<ActiveShowDto> list = activeInfoService.getActiveShowByActiveTag1ByCount(count, activeTag1);
        return Result.success(list);
    }

    @PostMapping("/getActiveShowByActiveTag1List/{count}")
    public Result getActiveShowByActiveTag1List(@RequestBody List<String> activeTag1List,@PathVariable("count")Integer count){
        List listList = new ArrayList<>();
        for (String activeTag1 : activeTag1List) {
            List<ActiveShowDto> list = activeInfoService.getActiveShowByActiveTag1ByCount(count, activeTag1);
            listList.add(list);
        }
        return Result.success(listList);
    }

    @GetMapping("/getActiveList")
    public List<ShowActive> getActiveList(){

        //查询该 club 的所有信息
        List<ShowActive> list = activeInfoService.getActiveList();

        return list;
    }

    @GetMapping("/getActiveInfoPass/{userOrClub}/{publishId}")
    public Result getActiveInfoPass(@PathVariable("userOrClub") Integer userOrClub,
                                    @PathVariable("publishId") Long publishId,
                                    PageParamVo pageParam){
        //校验publishId 是否合法
        if(Long.toString(publishId).length()!=10){
            return Result.fail("请检查后重新申请");
        }
        PageInfo<ActiveInfoDto> activeList = activeInfoService.getActiveInfoPass(userOrClub, publishId,pageParam);
        return Result.success("获取成功",activeList);
    }

    @GetMapping("/getActiveInfo/{id}")
    public Result getActiveInfo(@PathVariable Integer id){
        ActiveInfo activeInfo = activeInfoService.getById(id);
        return Result.success(activeInfo);
    }

    @GetMapping("/getActiveByPublish/{userOrClub}/{publishId}")
    public Result getActiveByPublish(@PathVariable("userOrClub")Integer userOrClub,@PathVariable("publishId")Long publishId){
        List<ActiveShowDto> activeDtoList = activeInfoService.getActiveByPublish(userOrClub,publishId);

        return Result.success(activeDtoList);
    }

    @GetMapping("/getNewActiveImage/{userOrClub}/{publishId}")
    public Result getNewActiveImage(@PathVariable("userOrClub")Integer userOrClub,@PathVariable Long publishId){

        List<ActiveShowDto> list = activeInfoService.getNewImageByPublishId(userOrClub,publishId);
        return Result.success(list);
    }

    @GetMapping("/getActiveShowByActiveTag12/{tag1Value}/{tag2Value}/{count}")
    public Result getActiveShowByActiveTag12(@PathVariable("tag1Value") String tag1Value,
                                             @PathVariable("tag2Value") String tag2Value,
                                             @PathVariable("count")Integer count){
        List<ActiveShowDto> list = activeInfoService.getActiveShowByActiveTag12ByCount(count, tag1Value,tag2Value);
        return Result.success(list);
    }

    @GetMapping("/getActiveInfoByActiveTag1/{tag1Value}")
    public Result getActiveInfoByActiveTag1(@PathVariable("tag1Value") String tag1Value,PageParamVo pageParam){

        PageInfo<ActiveInfoListDto> list = activeInfoService.getActiveInfoListByActiveTag1(tag1Value,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }

    @GetMapping("/getActiveInfoByActiveTag2/{tag2Value}")
    public Result getActiveInfoByActiveTag2(@PathVariable("tag2Value") String tag2Value,PageParamVo pageParam){

        PageInfo<ActiveInfoList2Dto> list = activeInfoService.getActiveInfoListByActiveTag2(tag2Value,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }


    /*
     * @Description: 搜索获取活动
     * @Param: searchParam
     * @return: Result
     * @Date: 2023/4/20
    */
    @GetMapping("/searchActiveListByParam/{searchParam}")
    public Result searchActiveListByParam(@PathVariable String searchParam,PageParamVo pageParam){
        PageInfo<ActiveInfoListDto> list = activeInfoService.getActiveInfoListBySearch(searchParam,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }

}
