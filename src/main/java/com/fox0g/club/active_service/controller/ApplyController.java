package com.fox0g.club.active_service.controller;


import com.fox0g.club.active_service.service.ActiveApplyService;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.ActiveApplyJoinDto;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-02-27 21:16
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/active")
public class ApplyController {

    @Autowired
    private ActiveApplyService activeApplyService;

    /*
     * @Description: 获取已报名且通过审核的用户报名信息
     * @Param: activeId
 * @param pageParam
     * @return: Result
     * @Date: 2023/2/28
    */
    @GetMapping("/getApplyJoinList/{activeId}")
    public Result getApplyJoinListByActiveId(@PathVariable("activeId") Integer activeId, PageParamVo pageParam){

        PageInfo<ActiveApplyJoinDto> activeList = activeApplyService.getPageActiveByIdIsPass(activeId,pageParam,1);

        return Result.success("成功",activeList);

    }

    /*
     * @Description: 获取已报名且审核中的用户报名信息
     * @Param: activeId
 * @param pageParam
     * @return: Result
     * @Date: 2023/2/28
    */
    @GetMapping("/getApplyPassList/{activeId}")
    public Result getApplyPassIngListByActiveId(@PathVariable("activeId") Integer activeId, PageParamVo pageParam){

        PageInfo<ActiveApplyJoinDto> activeList = activeApplyService.getPageActiveByIdIsPass(activeId,pageParam,3);

        return Result.success("成功",activeList);

    }

    @PostMapping("/joinActiveApply/{id}/{isJoin}")
    public Result joinActiveApplyPassById(@PathVariable("id")Integer id,
                                          @PathVariable("isJoin")Integer isJoin){

        boolean isUpdate = activeApplyService.updateActiveApplyJoinById(id,isJoin);
        return isUpdate ? Result.success("成功"):Result.fail("失败");
    }

    /*
     * @Description: 根据参与活动id获取并更新pass
     * @Param: id
 * @param isPass
     * @return: Result
     * @Date: 2023/2/28
    */
    @PostMapping("/passActiveApply/{id}/{isPass}")
    public Result passActiveApplyPassById(@PathVariable("id")Integer id,
                                      @PathVariable("isPass")Integer isPass){

        boolean isUpdate = activeApplyService.updateActiveApplyPassById(id,isPass);
        return isUpdate ? Result.success("成功"):Result.fail("失败");
    }

    @PostMapping("/joinMultipleActiveApply/{isJoin}")
    public Result joinMultipleActiveApplyById(@PathVariable("isJoin")Integer isJoin,@RequestBody List<Integer> ids){

//        List<Integer> idList = JsonUtil.jsonToList(ids);

        activeApplyService.multipleUpdateActiveApplyJoin(ids,isJoin);
        return Result.success();

    }

    @PostMapping("/passMultipleActiveApply/{isPass}")
    public Result passMultipleActiveApplyById(@PathVariable("isPass")Integer isPass,@RequestBody List<Integer> ids){
        activeApplyService.multipleUpdateActiveApplyPass(ids,isPass);
        return Result.success();

    }

}
