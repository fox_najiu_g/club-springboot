package com.fox0g.club.active_service.controller.api;

import com.fox0g.club.active_service.service.ActiveInfoService;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.ActiveApplyUserDto;
import com.fox0g.club.model.dto.ActiveDto;
import com.fox0g.club.model.po.ActiveInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-05 19:36
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/active/manage")
public class ActiveApiController {
    @Autowired
    private ActiveInfoService activeInfoService;

    @GetMapping("/getAllPassActive")
    public Result getAllPassActive(PageParamVo pageParamVo){

        PageInfo<ActiveDto> pageInfo = activeInfoService.selectAllPassActive(pageParamVo);
        return Result.success(pageInfo);
    }

    @GetMapping("/getActiveInfo/{id}")
    public Result getActiveInfo(@PathVariable Integer id){

        ActiveInfo activeInfo = activeInfoService.getById(id);
        return Result.success(activeInfo);
    }

    @DeleteMapping("/deleteActive/{id}")
    public Result deleteActive(@PathVariable Integer id){

        boolean isDelete = activeInfoService.removeById(id);
        return isDelete ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @GetMapping("/getAllApplyActive")
    public Result getAllApplyActive(PageParamVo pageParamVo){

        PageInfo<ActiveDto> pageInfo = activeInfoService.selectAllApplyActive(pageParamVo);
        return Result.success(pageInfo);
    }

    @GetMapping("/getAllApplyingActive")
    public Result getAllApplyingActive(PageParamVo pageParamVo){

        PageInfo<ActiveDto> pageInfo = activeInfoService.selectAllApplyingActive(pageParamVo);
        return Result.success(pageInfo);
    }

    @PostMapping("/updateApplyingActiveStatus/{id}/{applyStatus}")
    public Result updateApplyingActiveStatus(@PathVariable("id")Integer id,@PathVariable("applyStatus")Integer applyStatus){
        ActiveInfo activeInfo = activeInfoService.getById(id);
        activeInfo.setActiveStatus(applyStatus);
        activeInfo.setPublishDate(LocalDateTime.now());
        boolean isUpdate = activeInfoService.updateById(activeInfo);
        return isUpdate ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @PostMapping("/multipleUpdateApplyingActiveStatus/{applyStatus}")
    public Result multipleUpdateApplyingActiveStatus(@PathVariable("applyStatus")Integer applyStatus,@RequestBody List<Integer> ids){

        activeInfoService.multipleUpdateApplyingActiveStatus(ids,applyStatus);
        return Result.success("操作成功");

    }

    @GetMapping("/getAllActiveAllApplyUser")
    public Result getAllActiveAllApplyUser(PageParamVo pageParamVo){
        PageInfo<ActiveApplyUserDto> pageInfo = activeInfoService.getActiveAllApplyUser(pageParamVo);
        return Result.success(pageInfo);
    }




}
