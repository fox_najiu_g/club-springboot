package com.fox0g.club.active_service.controller;


import com.fox0g.club.active_service.service.ActiveApplyService;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.UserActiveApplyDto;
import com.fox0g.club.model.dto.UserActivePassDto;
import com.fox0g.club.model.po.ActiveApply;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: club-parent
 * @Date: 2023-03-03 07:58
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/active")
public class UserActiveApplyController {

    @Autowired
    private ActiveApplyService activeApplyService;

    /*
     * @Description: 根据userId获取用户参与过的所有活动
     * @Param: userId
     * @return: Result
     * @Date: 2023/3/3
    */
    @GetMapping("/getUserAllJoinActive/{userId}")
    public Result getUserJoinActive(@PathVariable Long userId, PageParamVo pageParam){
        //获取已报名通过且已参与的活动
        PageInfo<UserActiveApplyDto> pageInfo = activeApplyService.selectActiveApplyByUserIdIsJoinIsPass(userId, 1, 1,pageParam);
        return Result.success("获取成功",pageInfo);
    }

    @GetMapping("/getUserApplyActive/{userId}")
    public Result getUserApplyActive(@PathVariable Long userId, PageParamVo pageParam){

        PageInfo<UserActivePassDto> pageInfo = activeApplyService.selectActiveApplyByUserId(userId,pageParam);

        return Result.success("获取成功",pageInfo);
    }

    @PutMapping("/addUserApplyActive/{activeId}/{userId}")
    public Result addUserApplyActive(@PathVariable("activeId")Integer activeId,@PathVariable("userId")Long userId){

        ActiveApply activeApply = activeApplyService.getActiveApplyByActiveIdUserId(activeId, userId);
        if(!ObjectUtils.isEmpty(activeApply)){
            if(activeApply.getIsPass()==1 && activeApply.getIsJoin()==1){
                return Result.fail("已参与此活动");
            }
            if(activeApply.getIsPass()==3){
                return Result.fail("请勿重复报名活动！");
            }
        }
        boolean isAdd = activeApplyService.addUserApplyActiveByActiveIdUserId(activeId,userId);
        return isAdd ? Result.success("报名成功，审核中..."):Result.fail("失败，请稍后重试");
    }
}
