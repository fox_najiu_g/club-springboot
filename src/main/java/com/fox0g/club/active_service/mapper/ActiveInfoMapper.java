package com.fox0g.club.active_service.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ActiveInfo;
import com.fox0g.club.model.po.ShowActive;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.util.List;

/**
* @author 86187
* @description 针对表【active_info】的数据库操作Mapper
* @createDate 2023-02-27 10:33:50
* @Entity com.fox0g.model.po.ActiveInfo
*/
@Mapper
public interface ActiveInfoMapper extends BaseMapper<ActiveInfo> {

    @Select("select id,active_name,active_date,active_status,publish_date from active_info " +
            "where user_or_club=#{userOrClub} and publish_id=#{publishId} and (active_status=1 or active_status=2) order by publish_date desc")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_date",property = "activeDate"),
            @Result(column = "active_status",property = "activeStatus"),
            @Result(column = "publish_date",property = "publishDate")
    })
    List<ActiveStatusDto> getActiveStatus12(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);


    @Select("select id,active_name,active_date,active_status,apply_date from active_info where user_or_club=#{userOrClub} and publish_id=#{publishId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_date",property = "activeDate"),
            @Result(column = "active_status",property = "activeStatus"),
            @Result(column = "apply_date",property = "applyDate")
    })
    List<ActiveStatusApplyDto> getAllActiveApply(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);

    @Select("select active_name from active_info where id=#{id}")
    String getActiveNameById(@Param("id")Integer id);

    @Select("select active_date from active_info where id=#{id}")
    LocalDate getActiveDateById(@Param("id")Integer id);


    @Select("select id,publish_id,active_name,active_date,user_or_club,active_status from active_info where active_status=1 or active_status=2")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_date",property = "activeDate"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "active_status",property = "activeStatus")
    })
    List<ActiveDto> getAllPassActive();

    @Select("select id,publish_id,active_name,active_date,user_or_club,active_status from active_info")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_date",property = "activeDate"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "active_status",property = "activeStatus")
    })
    List<ActiveDto> getAllApplyActive();

    @Select("select id,publish_id,active_name,active_date,user_or_club,active_status from active_info where active_status=3")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_date",property = "activeDate"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "active_status",property = "activeStatus")
    })
    List<ActiveDto> getAllApplyingActive();

    @Select("select id,active_id,user_id,apply_date,is_join,is_pass from active_apply")
    List<ActiveApplyUserDto> getActiveAllApplyUser();

    @Select("select id,active_name,active_image,active_status from active_info " +
            "where active_tag1 like '%${activeValue}%' and (active_status=1 or active_status=2) " +
            "order by active_date desc limit #{count}")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_image",property = "activeImage"),
            @Result(column = "active_status",property = "activeStatus"),
    })
    List<ActiveShowDto> getActiveShowByActiveTag1ByCount(@Param("activeValue")String activeValue, @Param("count")int count);

    @Select("select id,active_name,active_image,active_status from active_info")
    @Results(value ={
            @Result(column = "id",property = "activeId"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_image",property = "activeImage"),
            @Result(column = "active_status",property = "activeStatus"),
    })
    List<ShowActive> getShowActiveList();

    @Select("select id,active_name,active_date,active_status,publish_date,active_address,active_desc,active_image from active_info " +
            "where user_or_club=#{userOrClub} and publish_id=#{publishId} and (active_status=1 or active_status=2) order by publish_date desc")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_date",property = "activeDate"),
            @Result(column = "active_status",property = "activeStatus"),
            @Result(column = "publish_date",property = "publishDate"),
            @Result(column = "active_address",property = "activeAddress"),
            @Result(column = "active_desc",property = "activeDesc"),
            @Result(column = "active_image",property = "activeImage"),
    })
    List<ActiveInfoDto> getActiveInfoPassList(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);

    @Select("select id,active_name,active_image,active_status from active_info " +
            "where user_or_club=#{userOrClub} and publish_id=#{publishId} and (active_status=1 or active_status=2) " +
            "order by publish_date desc limit 10")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_status",property = "activeStatus"),
            @Result(column = "active_image",property = "activeImage"),
    })
    List<ActiveShowDto> getActiveListByPublish(@Param("userOrClub")Integer userOrClub, @Param("publishId")Long publishId);

    @Select("select id,active_name,active_image,active_status from active_info " +
            "where active_tag1 like '%${tag1Value}%' and active_tag2 like '%${tag2Value}%' and (active_status=1 or active_status=2) " +
            "order by active_date desc limit #{count}")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_image",property = "activeImage"),
            @Result(column = "active_status",property = "activeStatus"),
    })
    List<ActiveShowDto> getActiveShowByActiveTag12ByCount(@Param("tag1Value")String tag1Value,@Param("tag2Value")String tag2Value,@Param("count")int count);

    @Select("select id,publish_id,user_or_club,active_name,active_desc,active_image,active_tag2,publish_date " +
            "from active_info where active_tag1 like '%${tag1Value}%' and (active_status=1 or active_status=2) order by publish_date desc")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_desc",property = "activeDesc"),
            @Result(column = "active_image",property = "activeImage"),
            @Result(column = "active_tag2",property = "activeTag2"),
            @Result(column = "publish_date",property = "publishDate"),
    })
    List<ActiveInfoListDto> getActiveInfoListByActiveTag1(@Param("tag1Value")String tag1Value);

    @Select("select id,publish_id,user_or_club,active_name,active_desc,active_image,active_tag1,active_tag2,publish_date " +
            "from active_info where active_tag2 like '%${tag2Value}%' and (active_status=1 or active_status=2) order by publish_date desc")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_desc",property = "activeDesc"),
            @Result(column = "active_image",property = "activeImage"),
            @Result(column = "active_tag1",property = "activeTag1"),
            @Result(column = "active_tag2",property = "activeTag2"),
            @Result(column = "publish_date",property = "publishDate"),
    })
    List<ActiveInfoList2Dto> getActiveInfoListByActiveTag2(@Param("tag2Value")String tag2Value);

    @Select("select id,publish_id,user_or_club,active_name,active_desc,active_image,active_tag2,publish_date " +
            "from active_info where (active_name like '%${searchParam}%' or active_desc like '%${searchParam}%' " +
            "or active_tag1 like '%${searchParam}%' or active_tag2 like '%${searchParam}%' )" +
            "and (active_status=1 or active_status=2) order by publish_date desc")
    @Results(value = {
            @Result(column = "id",property = "id"),
            @Result(column = "publish_id",property = "publishId"),
            @Result(column = "user_or_club",property = "userOrClub"),
            @Result(column = "active_name",property = "activeName"),
            @Result(column = "active_desc",property = "activeDesc"),
            @Result(column = "active_image",property = "activeImage"),
            @Result(column = "active_tag2",property = "activeTag2"),
            @Result(column = "publish_date",property = "publishDate"),
    })
    List<ActiveInfoListDto> getActiveInfoListBySearchParam(String searchParam);
}




