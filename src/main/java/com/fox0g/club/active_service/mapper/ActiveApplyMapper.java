package com.fox0g.club.active_service.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fox0g.club.model.dto.ActiveIdDto;
import com.fox0g.club.model.po.ActiveApply;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【active_apply】的数据库操作Mapper
* @createDate 2023-02-27 21:35:47
* @Entity com.fox0g.model.po.ActiveApply
*/
@Mapper
public interface ActiveApplyMapper extends BaseMapper<ActiveApply> {

    @Update("update active_apply set is_join = #{isJoin} where id = #{id}")
    boolean updateActiveApplyJoinStatusById(@Param("id")Integer id,@Param("isJoin")Integer isJoin);

    @Update("update active_apply set is_pass = #{isPass} where id = #{id}")
    boolean updateActiveApplyPassStatusById(@Param("id")Integer id,@Param("isPass")Integer isPass);

    @Select("select id,active_id from active_apply where user_id=#{userId} and is_join=#{isJoin} and is_pass=#{isPass}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "active_id",property = "activeId"),
    })
    List<ActiveIdDto> selectActiveApplyByUserIdIsJoinIsPass(@Param("userId")Long userId, @Param("isJoin")Integer isJoin, @Param("isPass")Integer isPass);



}




