package com.fox0g.club.active_service.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ActiveInfo;
import com.fox0g.club.model.po.ShowActive;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
* @author 86187
* @description 针对表【active_info】的数据库操作Service
* @createDate 2023-02-27 10:33:50
*/
public interface ActiveInfoService extends IService<ActiveInfo> {

    /*
     * @Description: 增加一个待审核的活动
     * @Param: activeInfo
     * @return: void
     * @Date: 2023/2/27
    */
    boolean addActiveInfo(ActiveInfo activeInfo);

    /*
     * @Description: 根据 创建人 id 获取该用户/club 所有信息，并返回list
     * @Param: userOrClub
 * @param publishId
     * @return: List<ActiveStatusDto>
     * @Date: 2023/2/27
    */
    PageInfo<ActiveStatusDto> getActiveStatus12(Integer userOrClub, Long publishId, PageParamVo pageParam);

    PageInfo<ActiveStatusApplyDto> getAllActiveApply(Integer userOrClub, Long publishId, PageParamVo pageParam);

    /*
     * @Description: 根据活动id查询，并更改状态
     * @Param: id
 * @param activeStatus
     * @return: boolean
     * @Date: 2023/2/27
    */
    boolean updateActiveStatusById(Integer id, Integer activeStatus);

    /*
     * @Description: 根据id删除活动
     * @Param: id
     * @return: boolean
     * @Date: 2023/2/27
    */
    boolean endActiveById(Integer id);


    /*
     * 管理员
     */

    PageInfo<ActiveDto> selectAllPassActive(PageParamVo pageParamVo);

    PageInfo<ActiveDto> selectAllApplyActive(PageParamVo pageParamVo);

    PageInfo<ActiveDto> selectAllApplyingActive(PageParamVo pageParamVo);

    void multipleUpdateApplyingActiveStatus(List<Integer> ids, Integer applyStatus);

    PageInfo<ActiveApplyUserDto> getActiveAllApplyUser(PageParamVo pageParamVo);

    List<ActiveShowDto> getActiveShowByActiveTag1ByCount(int count, String activeValue);

    List<ShowActive> getActiveList();

    PageInfo<ActiveInfoDto> getActiveInfoPass(Integer userOrClub, Long publishId, PageParamVo pageParam);

    List<ActiveShowDto> getActiveByPublish(Integer userOrClub, Long publishId);

    List<ActiveShowDto> getNewImageByPublishId(Integer userOrClub,Long publishId);

    List<ActiveShowDto> getActiveShowByActiveTag12ByCount(Integer count, String tag1Value, String tag2Value);

    PageInfo<ActiveInfoListDto> getActiveInfoListByActiveTag1(String tag1Value,Integer pageNum,Integer pageSize);

    PageInfo<ActiveInfoList2Dto> getActiveInfoListByActiveTag2(String tag2Value, Integer pageNum, Integer pageSize);

    PageInfo<ActiveInfoListDto> getActiveInfoListBySearch(String searchParam, Integer pageNum, Integer pageSize);
}
