package com.fox0g.club.active_service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fox0g.club.active_service.mapper.ActiveInfoMapper;
import com.fox0g.club.active_service.service.ActiveApplyService;
import com.fox0g.club.active_service.service.ActiveInfoService;
import com.fox0g.club.club_service.controller.ClubInfoController;
import com.fox0g.club.model.dto.*;
import com.fox0g.club.model.po.ActiveInfo;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.ShowActive;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.user_service.controller.UserInfoController;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 86187
* @description 针对表【active_info】的数据库操作Service实现
* @createDate 2023-02-27 10:33:50
*/
@Service
public class ActiveInfoServiceImpl extends ServiceImpl<ActiveInfoMapper, ActiveInfo>
    implements ActiveInfoService {

    @Autowired
    private UserInfoController userFeignClient;

    @Autowired
    private ClubInfoController clubFeignClient;
    @Autowired
    private ActiveInfoService activeInfoService;
    @Autowired
    private ActiveApplyService activeApplyService;

    @Override
    public boolean addActiveInfo(ActiveInfo activeInfo) {

        int isAdd = this.baseMapper.insert(activeInfo);
        return isAdd==1 ? true:false;
    }

    @Override
    public PageInfo<ActiveStatusDto> getActiveStatus12(Integer userOrClub, Long publishId, PageParamVo pageParam) {
        PageHelper.startPage(pageParam.getPageNum(), pageParam.getPageSize());

        List<ActiveStatusDto> activeList = this.baseMapper.getActiveStatus12(userOrClub, publishId);
        PageInfo<ActiveStatusDto> pageInfo = new PageInfo<>(activeList);

        return pageInfo;

    }

    @Override
    public PageInfo<ActiveStatusApplyDto> getAllActiveApply(Integer userOrClub, Long publishId, PageParamVo pageParam) {
        PageHelper.startPage(pageParam.getPageNum(), pageParam.getPageSize());
        List<ActiveStatusApplyDto> activeList = this.baseMapper.getAllActiveApply(userOrClub, publishId);
        PageInfo<ActiveStatusApplyDto> pageInfo = new PageInfo<>(activeList);

        return pageInfo;
    }

    @Override
    public boolean updateActiveStatusById(Integer id, Integer activeStatus) {

        ActiveInfo activeInfo = this.baseMapper.selectById(id);
        activeInfo.setActiveStatus(activeStatus);
        int isUpdate = this.baseMapper.updateById(activeInfo);
        return isUpdate==1 ? true:false;
    }

    @Transactional
    @Override
    public boolean endActiveById(Integer id) {

        int isDelete = this.baseMapper.deleteById(id);
        activeApplyService.deleteAllActiveApplyByActiveId(id);
        return isDelete==1 ? true:false;
    }

    @Override
    public PageInfo<ActiveDto> selectAllPassActive(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<ActiveDto> activeDtoList = this.baseMapper.getAllPassActive();
        activeDtoList.stream().map(activeDto -> {
            Integer userOrClub = activeDto.getUserOrClub();
            Long publishId = activeDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                activeDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                activeDto.setPublishName(clubInfo.getClubName());
            }
            return activeDto;
        }).collect(Collectors.toList());
        PageInfo<ActiveDto> pageInfo = new PageInfo<>(activeDtoList);
        return  pageInfo;

    }

    @Override
    public PageInfo<ActiveDto> selectAllApplyActive(PageParamVo pageParamVo) {

        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<ActiveDto> activeDtoList = this.baseMapper.getAllApplyActive();
        activeDtoList.stream().map(activeDto -> {
            Integer userOrClub = activeDto.getUserOrClub();
            Long publishId = activeDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                activeDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                activeDto.setPublishName(clubInfo.getClubName());
            }
            return activeDto;
        }).collect(Collectors.toList());
        PageInfo<ActiveDto> pageInfo = new PageInfo<>(activeDtoList);
        return  pageInfo;
    }

    @Override
    public PageInfo<ActiveDto> selectAllApplyingActive(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<ActiveDto> activeDtoList = this.baseMapper.getAllApplyingActive();
        activeDtoList.stream().map(activeDto -> {
            Integer userOrClub = activeDto.getUserOrClub();
            Long publishId = activeDto.getPublishId();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                activeDto.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                activeDto.setPublishName(clubInfo.getClubName());
            }
            return activeDto;
        }).collect(Collectors.toList());
        PageInfo<ActiveDto> pageInfo = new PageInfo<>(activeDtoList);
        return  pageInfo;
    }

    @Override
    public void multipleUpdateApplyingActiveStatus(List<Integer> ids, Integer applyStatus) {
        //批量更新isJoin
        ids.stream().forEach(id -> {
            ActiveInfo activeInfo = activeInfoService.getById(id);
            activeInfo.setActiveStatus(applyStatus);
            activeInfo.setPublishDate(LocalDateTime.now());
            boolean isUpdate = activeInfoService.updateById(activeInfo);
        });
    }

    @Override
    public PageInfo<ActiveApplyUserDto> getActiveAllApplyUser(PageParamVo pageParamVo) {
        PageHelper.startPage(pageParamVo.getPageNum(), pageParamVo.getPageSize());
        List<ActiveApplyUserDto> activeDtoList = this.baseMapper.getActiveAllApplyUser();
        activeDtoList.stream().map(activeDto -> {
            Integer activeId = activeDto.getActiveId();
            Long userId = activeDto.getUserId();
            //活动
            ActiveInfo activeInfo = activeInfoService.getById(activeId);
            activeDto.setActiveName(activeInfo.getActiveName());
            //用户
            UserInfo userInfo = userFeignClient.getUserInfoByUserId(userId);
            activeDto.setUserName(userInfo.getUserName());
            return activeDto;
        }).collect(Collectors.toList());
        PageInfo<ActiveApplyUserDto> pageInfo = new PageInfo<>(activeDtoList);
        return  pageInfo;
    }

    @Override
    public List<ActiveShowDto> getActiveShowByActiveTag1ByCount(int count, String activeValue) {
        List<ActiveShowDto> activeShowDtoList = this.baseMapper.getActiveShowByActiveTag1ByCount(activeValue,count);
        return activeShowDtoList;
    }

    @Override
    public List<ShowActive> getActiveList() {
        List<ShowActive> list = this.baseMapper.getShowActiveList();
        return list;
    }

    @Override
    public PageInfo<ActiveInfoDto> getActiveInfoPass(Integer userOrClub, Long publishId, PageParamVo pageParam) {
        PageHelper.startPage(pageParam.getPageNum(), pageParam.getPageSize());

        List<ActiveInfoDto> activeList = this.baseMapper.getActiveInfoPassList(userOrClub, publishId);
        PageInfo<ActiveInfoDto> pageInfo = new PageInfo<>(activeList);

        return pageInfo;
    }

    @Override
    public List<ActiveShowDto> getActiveByPublish(Integer userOrClub, Long publishId) {
        List<ActiveShowDto> activeListByPublish = this.baseMapper.getActiveListByPublish(userOrClub, publishId);
        return activeListByPublish;
    }

    @Override
    public List<ActiveShowDto> getNewImageByPublishId(Integer userOrClub,Long publishId) {
        List<ActiveShowDto> activeByPublish = activeInfoService.getActiveByPublish(userOrClub, publishId);

        if(activeByPublish.size()>=4){
            activeByPublish=activeByPublish.subList(0,4);
        }
        return activeByPublish;
    }

    @Override
    public List<ActiveShowDto> getActiveShowByActiveTag12ByCount(Integer count, String tag1Value, String tag2Value) {
        List<ActiveShowDto> activeShowDtoList = this.baseMapper.getActiveShowByActiveTag12ByCount(tag1Value,tag2Value,count);
        return activeShowDtoList;
    }

    @Override
    public PageInfo<ActiveInfoListDto> getActiveInfoListByActiveTag1(String tag1Value,Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ActiveInfoListDto> list = this.baseMapper.getActiveInfoListByActiveTag1(tag1Value);
        list.stream().map(activeInfo -> {
            Long publishId = activeInfo.getPublishId();
            Integer userOrClub = activeInfo.getUserOrClub();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                activeInfo.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                activeInfo.setPublishName(clubInfo.getClubName());
            }
            return activeInfo;
        }).collect(Collectors.toList());
        PageInfo<ActiveInfoListDto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<ActiveInfoList2Dto> getActiveInfoListByActiveTag2(String tag2Value, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ActiveInfoList2Dto> list = this.baseMapper.getActiveInfoListByActiveTag2(tag2Value);
        list.stream().map(activeInfo -> {
            Long publishId = activeInfo.getPublishId();
            Integer userOrClub = activeInfo.getUserOrClub();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                activeInfo.setPublishName(userInfo.getUserName());
                activeInfo.setPublishImage(userInfo.getUserImage());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                activeInfo.setPublishName(clubInfo.getClubName());
                activeInfo.setPublishImage(clubInfo.getClubImage());
            }
            return activeInfo;
        }).collect(Collectors.toList());
        PageInfo<ActiveInfoList2Dto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<ActiveInfoListDto> getActiveInfoListBySearch(String searchParam, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ActiveInfoListDto> list = this.baseMapper.getActiveInfoListBySearchParam(searchParam);
        list.stream().map(activeInfo -> {
            Long publishId = activeInfo.getPublishId();
            Integer userOrClub = activeInfo.getUserOrClub();
            if(userOrClub==11){
                //用户
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(publishId);
                activeInfo.setPublishName(userInfo.getUserName());
            }
            if(userOrClub==22){
                //组织
                ClubInfo clubInfo = clubFeignClient.getClubInfo(publishId);
                activeInfo.setPublishName(clubInfo.getClubName());
            }
            return activeInfo;
        }).collect(Collectors.toList());
        PageInfo<ActiveInfoListDto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
}




