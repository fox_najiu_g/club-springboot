package com.fox0g.club.active_service.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.dto.ActiveApplyJoinDto;
import com.fox0g.club.model.dto.UserActiveApplyDto;
import com.fox0g.club.model.dto.UserActivePassDto;
import com.fox0g.club.model.po.ActiveApply;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
* @author 86187
* @description 针对表【active_apply】的数据库操作Service
* @createDate 2023-02-27 21:35:47
*/
public interface ActiveApplyService extends IService<ActiveApply> {



    /*
     * @Description: 根据活动参与的用户的审核状态信息获取
     * @Param: activeId
 * @param pageParam
     * @return: PageInfo<ActiveApplyJoinDto>
     * @Date: 2023/2/28
    */
    PageInfo<ActiveApplyJoinDto> getPageActiveByIdIsPass(Integer activeId, PageParamVo pageParam, Integer isPass);

    boolean updateActiveApplyJoinById(Integer id,Integer isJoin);

    /*
     * @Description: 根据id 修改用户参与的Pass状态
     * @Param: id
     * @return: boolean
     * @Date: 2023/2/28
    */
    boolean updateActiveApplyPassById(Integer id,Integer isPass);

    /*
     * @Description: 多 修改用户参与活动信息 isJoin
     * @Param: idList
 * @param isJoin
     * @return: void
     * @Date: 2023/2/28
    */
    void multipleUpdateActiveApplyJoin(List<Integer> idList, Integer isJoin);

    void multipleUpdateActiveApplyPass(List<Integer> ids, Integer isPass);

    /*
     * @Description: 根据userId/isJoin/isPass 获取用户活动
     * @Param: userId
 * @param isJoin
 * @param isPass
     * @return: void
     * @Date: 2023/3/3
    */
    PageInfo<UserActiveApplyDto> selectActiveApplyByUserIdIsJoinIsPass(Long userId, Integer isJoin, Integer isPass, PageParamVo pageParam);

    PageInfo<UserActivePassDto> selectActiveApplyByUserId(Long userId, PageParamVo pageParam);

    ActiveApply getActiveApplyByActiveIdUserId(Integer activeId, Long userId);

    boolean addUserApplyActiveByActiveIdUserId(Integer activeId, Long userId);

    /*
     * @Description: 删除活动所有报名信息
     * @Param: null
     * @return: null
     * @Date: 2023/4/4
    */
    void deleteAllActiveApplyByActiveId(Integer activeId);
}
