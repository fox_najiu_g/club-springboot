package com.fox0g.club.active_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.active_service.mapper.ActiveApplyMapper;
import com.fox0g.club.active_service.mapper.ActiveInfoMapper;
import com.fox0g.club.active_service.service.ActiveApplyService;
import com.fox0g.club.active_service.service.ActiveInfoService;
import com.fox0g.club.club_service.controller.ClubInfoController;
import com.fox0g.club.model.dto.ActiveApplyJoinDto;
import com.fox0g.club.model.dto.ActiveIdDto;
import com.fox0g.club.model.dto.UserActiveApplyDto;
import com.fox0g.club.model.dto.UserActivePassDto;
import com.fox0g.club.model.po.ActiveApply;
import com.fox0g.club.model.po.ActiveInfo;
import com.fox0g.club.model.po.ClubInfo;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.user_service.controller.UserInfoController;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 86187
* @description 针对表【active_apply】的数据库操作Service实现
* @createDate 2023-02-27 21:35:47
*/
@Service
public class ActiveApplyServiceImpl extends ServiceImpl<ActiveApplyMapper, ActiveApply>
    implements ActiveApplyService {

    @Autowired
    private UserInfoController userFeignClient;

    @Autowired
    private ClubInfoController clubFeignClient;
    @Autowired
    private ActiveInfoService activeInfoService;
    @Autowired
    private ActiveInfoMapper activeInfoMapper;


    @Override
    public PageInfo<ActiveApplyJoinDto> getPageActiveByIdIsPass(Integer activeId, PageParamVo pageParam, Integer isPass) {
        //分页
        PageHelper.startPage(pageParam.getPageNum(),pageParam.getPageSize());
        //queryWrapper查询该活动的所有 信息
        LambdaQueryWrapper<ActiveApply> queryWrapper = new LambdaQueryWrapper<>();
        //条件：活动Id/报名审核状态
        queryWrapper.eq(ActiveApply::getActiveId,activeId).eq(ActiveApply::getIsPass,isPass);
        List<ActiveApply> activeApplyList = this.baseMapper.selectList(queryWrapper);
        //将所有ActiveApply 转到 ActiveApplyDto
        List<ActiveApplyJoinDto> activeApplyJoinDtoList = new ArrayList<>();
        activeApplyList.stream().forEach(activeApply -> {
            ActiveApplyJoinDto activeApplyJoinDto = new ActiveApplyJoinDto();
            BeanUtils.copyProperties(activeApply, activeApplyJoinDto);
            activeApplyJoinDtoList.add(activeApplyJoinDto);
        });
        //获取填充其中的userName
        activeApplyJoinDtoList.stream().map(activeApply -> {
            UserInfo userInfo = userFeignClient.getUserInfoByUserId(activeApply.getUserId());
            activeApply.setUserName(userInfo.getUserName());
            return activeApply;
        }).collect(Collectors.toList());
        PageInfo<ActiveApplyJoinDto> pageInfo = new PageInfo<>(activeApplyJoinDtoList);
        return pageInfo;
    }

    @Override
    public boolean updateActiveApplyJoinById(Integer id, Integer isJoin) {

        return this.baseMapper.updateActiveApplyJoinStatusById(id, isJoin);
    }

    @Override
    public boolean updateActiveApplyPassById(Integer id,Integer isPass) {

        return this.baseMapper.updateActiveApplyPassStatusById(id, isPass);

    }

    @Override
    public void multipleUpdateActiveApplyJoin(List<Integer> idList, Integer isJoin) {

        //批量更新isJoin
        idList.stream().forEach(id -> {
            this.updateActiveApplyJoinById(id, isJoin);
        });
    }

    @Override
    public void multipleUpdateActiveApplyPass(List<Integer> idList, Integer isPass) {
        //批量更新isPass
        idList.stream().forEach(id -> {
            this.updateActiveApplyPassById(id, isPass);
        });
    }

    @Override
    public PageInfo<UserActiveApplyDto> selectActiveApplyByUserIdIsJoinIsPass(Long userId, Integer isJoin, Integer isPass, PageParamVo pageParam) {
        //分页
        PageHelper.startPage(pageParam.getPageNum(),pageParam.getPageSize());
        //先获取参与活动的记录
        List<ActiveIdDto> activeIdDtoList = this.baseMapper.selectActiveApplyByUserIdIsJoinIsPass(userId, isJoin, isPass);
        if(activeIdDtoList.size()==0){
            return null;
        }
        List<UserActiveApplyDto> userActiveApplyDtoList = new ArrayList<>();
        activeIdDtoList.stream().forEach(activeIdDto -> {
            //获取到参与活动记录id/活动id
            UserActiveApplyDto userActiveApplyDto = new UserActiveApplyDto();
            userActiveApplyDto.setId(activeIdDto.getId());
            userActiveApplyDto.setActiveId(activeIdDto.getActiveId());
            //封装返回信息
            //封装活动信息
            ActiveInfo activeInfo = activeInfoService.getById(activeIdDto.getActiveId());
            userActiveApplyDto.setUserOrClub(activeInfo.getUserOrClub());
            userActiveApplyDto.setActiveDate(activeInfo.getActiveDate());
            userActiveApplyDto.setPublishDate(activeInfo.getPublishDate());
            userActiveApplyDto.setActiveName(activeInfo.getActiveName());
            userActiveApplyDto.setActiveDesc(activeInfo.getActiveDesc());
            //封装发布者信息
            if(activeInfo.getUserOrClub()==11){
                //用户发布的活动
                UserInfo userInfo = userFeignClient.getUserInfoByUserId(activeInfo.getPublishId());
                userActiveApplyDto.setPublishId(userInfo.getUserId());
                userActiveApplyDto.setPublishImage(userInfo.getUserImage());
                userActiveApplyDto.setPublishName(userInfo.getUserName());
            }
            if(activeInfo.getUserOrClub()==22){
                //club发布的活动
                ClubInfo clubInfo = clubFeignClient.getClubInfo(activeInfo.getPublishId());
                userActiveApplyDto.setPublishId(clubInfo.getClubId());
                userActiveApplyDto.setPublishImage(clubInfo.getClubImage());
                userActiveApplyDto.setPublishName(clubInfo.getClubName());
            }
            userActiveApplyDtoList.add(userActiveApplyDto);
        });
        PageInfo<UserActiveApplyDto> pageInfo = new PageInfo<>(userActiveApplyDtoList);

        return pageInfo;
    }

    @Override
    public PageInfo<UserActivePassDto> selectActiveApplyByUserId(Long userId, PageParamVo pageParam) {
        PageHelper.startPage(pageParam.getPageNum(),pageParam.getPageSize());
        //根据用户id 查找该用户所有报名活动
        LambdaQueryWrapper<ActiveApply> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ActiveApply::getUserId,userId);
        List<ActiveApply> activeApplyList = this.baseMapper.selectList(queryWrapper);
        if(activeApplyList.size()==0){
            return null;
        }
        //封装成对象
        List<UserActivePassDto> userActivePassDtoList = new ArrayList<>();
        activeApplyList.stream().forEach(activeApply -> {
            UserActivePassDto userActivePassDto = new UserActivePassDto();
            BeanUtils.copyProperties(activeApply,userActivePassDto);
            //根据活动id 获取活动名
            userActivePassDto.setActiveDate(activeInfoMapper.getActiveDateById(activeApply.getActiveId()));
            userActivePassDto.setActiveName(activeInfoMapper.getActiveNameById(activeApply.getActiveId()));
            userActivePassDtoList.add(userActivePassDto);
        });
        PageInfo<UserActivePassDto> pageInfo = new PageInfo<>(userActivePassDtoList);

        return pageInfo;
    }

    @Override
    public ActiveApply getActiveApplyByActiveIdUserId(Integer activeId, Long userId) {
        LambdaQueryWrapper<ActiveApply> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ActiveApply::getActiveId,activeId).eq(ActiveApply::getUserId,userId);
        ActiveApply activeApply = this.baseMapper.selectOne(queryWrapper);
        return activeApply;
    }

    @Override
    public boolean addUserApplyActiveByActiveIdUserId(Integer activeId, Long userId) {
        ActiveApply activeApply = new ActiveApply();
        activeApply.setActiveId(activeId);
        activeApply.setUserId(userId);
        activeApply.setApplyDate(LocalDateTime.now());
        activeApply.setIsPass(3);
        int insert = this.baseMapper.insert(activeApply);
        return insert==1 ? true:false;
    }

    @Override
    public void deleteAllActiveApplyByActiveId(Integer activeId) {
        LambdaQueryWrapper<ActiveApply> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ActiveApply::getActiveId,activeId);
        this.baseMapper.delete(queryWrapper);
    }
}




