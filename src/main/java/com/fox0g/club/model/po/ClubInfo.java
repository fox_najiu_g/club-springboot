package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 
 * @TableName club_info
 */
@TableName(value ="club_info")
@Data
public class ClubInfo implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private Long clubId;

    /**
     * 
     */
    private Long userId;

    /**
     * 
     */
    private String clubName;

    /**
     *
     */
    private String clubAddress;

    /**
     * 
     */
    private String clubImage;

    /**
     * 
     */
    private String clubSignature;

    /**
     * 
     */
    private String clubIntroduce;

    /**
     *
     */
    private LocalDate clubRegistration;

    /**
     * 
     */
    private String clubReason;

    private Integer applyStatus;

    private LocalDate applyDate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}