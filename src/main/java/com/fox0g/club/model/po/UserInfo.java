package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 
 * @TableName user_info
 */
@TableName(value ="user_info")
@Data
public class UserInfo implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户昵称
     */
    @Length(max=15,min=1)
    private String userName;

    /**
     * 用户id
     */
    @Min(value = 1000000000L)
    @Max(value = 9999999999L)
    private Long userId;

    /**
     * 用户头像
     */
    private String userImage;

    /**
     * 性别 男：1 女：2
     */
    private Integer userSex;

    /**
     * 地址
     */
    private String userAddress;

    /**
     * 出生日期
     */
    private LocalDate userBirth;

    /**
     * 个性签名
     */
    @Length(max=10)
    private String userSignature;

    /**
     * 创建日期
     */
    private LocalDate userRegistration;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}