package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName show_article
 */
@TableName(value ="show_active")
@Data
public class ShowActive implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer activeId;

    /**
     * 
     */
    private Integer activeStatus;

    /**
     * 
     */
    private String activeImage;

    private String activeName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}