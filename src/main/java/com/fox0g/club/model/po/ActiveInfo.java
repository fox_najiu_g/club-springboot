package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 
 * @TableName active_info
 */
@TableName(value ="active_info")
@Data
public class ActiveInfo implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 发布者Id
     */
    private Long publishId;

    /**
     * 活动名
     */
    private String activeName;

    /**
     * 活动时间
     */
    private LocalDate activeDate;

    /**
     * 活动地址
     */
    private String activeAddress;

    /**
     * 活动内容
     */
    private String activeContent;

    /**
     * 活动摘要
     */
    private String activeDesc;

    /**
     * 活动图片
     */
    private String activeImage;

    /**
     * 活动标签1
     */
    private String activeTag1;

    /**
     * 活动标签2
     */
    private String activeTag2;

    /**
     * 活动状态 1:活动中 2:已结束 3:审核中 4:未通过
     */
    private Integer activeStatus;

    /**
     * 发布者： 11：user 22：club
     */
    private Integer userOrClub;
    /**
     * 提交审核时间
     */
    private LocalDateTime applyDate;
    /**
     * 发布时间
     */
    private LocalDateTime publishDate;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}