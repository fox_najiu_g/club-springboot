package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName article_info
 */
@TableName(value ="article_info")
@Data
public class ArticleInfo implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 发布者Id
     */
    private Long publishId;

    /**
     * 动态内容
     */
    private String articleContent;

    /**
     * 动态内容
     */
    private String articleImage;

    /**
     * 动态标签1
     */
    private String articleTag1;

    /**
     * 动态标签2
     */
    private String articleTag2;

    /**
     * 发布状态1:已通过(已发布)2:未通过3:审核中
     */
    private Integer articleStatus;

    /**
     * 发布时间
     */
    private LocalDateTime applyDate;

    /**
     * 发布时间
     */
    private LocalDateTime publishDate;

    /**
     * 11:user 22:club
     */
    private Integer userOrClub;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}