package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName club_account
 */
@TableName(value ="club_account")
@Data
public class ClubAccount implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * clubId
     */
    private Long clubId;

    /**
     * 邮箱
     */
    private String clubEmail;

    /**
     * 可以加入：1 不可以加入：2
     */
    private Integer clubIsJoin;

    /**
     * userId
     */
    private Long userId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}