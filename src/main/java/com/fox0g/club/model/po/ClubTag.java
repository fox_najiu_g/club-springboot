package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName club_tag
 */
@TableName(value ="club_tag")
@Data
public class ClubTag implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * tag类型
     */
    private Integer tagType;

    /**
     * tag值
     */
    private String tagValue;

    private Long clubId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public ClubTag() {
    }

    public ClubTag(Integer tagType, String tagValue, Long clubId) {
        this.tagType = tagType;
        this.tagValue = tagValue;
        this.clubId = clubId;
    }
}