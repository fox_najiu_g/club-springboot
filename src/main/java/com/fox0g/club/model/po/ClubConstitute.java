package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName club_constitute
 */
@TableName(value ="club_constitute")
@Data
public class ClubConstitute implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 该节点label
     */
    private String label;

    /**
     * 该节点label
     */
    private String name;

    /**
     * 该节点Value
     */
    private Long value;

    /**
     * 是否为叶子节点 0:false 1:true
     */
    private Integer isLeaf;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}