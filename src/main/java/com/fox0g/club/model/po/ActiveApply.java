package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @TableName active_apply
 */
@TableName(value ="active_apply")
@Data
public class ActiveApply implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 活动ID
     */
    private Integer activeId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 报名时间
     */
    private LocalDateTime applyDate;

    /**
     * 1:参与 2：未参与
     */
    private Integer isJoin;

    /**
     * 1：通过 2：未通过 3：审核中
     */
    private Integer isPass;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}