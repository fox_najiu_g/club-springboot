package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 
 * @TableName club_apply
 */
@TableName(value ="club_apply")
@Data
public class ClubApply implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Long clubId;

    /**
     * 申请时间
     */
    private LocalDate applyDate;

    /**
     * 申请用户id
     */
    private Long userId;

    /**
     * 申请用户姓名
     */
    private String userName;

    /**
     * 申请备注
     */
    private String applyRemark;

    /**
     * 申请状态 1:通过 2：未通过 3：审核中
     */
    private String applyStatus;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}