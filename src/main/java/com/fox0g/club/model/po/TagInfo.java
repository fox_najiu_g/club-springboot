package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName tag_info
 */
@TableName(value ="tag_info")
@Data
public class TagInfo implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 标签类型
     * club：33...
     *       主：331...
     *       副：332...
     * 用户：44...
     * 活动：55...
     *       主：551...
     *       副：552...
     * 动态：66...
     *       主：661...
     *       副：662...
     */
    private Integer tagType;

    /**
     * 标签内容
     */
    private String tagValue;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public TagInfo() {
    }

    public TagInfo(Integer tagType, String tagValue) {
        this.tagType = tagType;
        this.tagValue = tagValue;
    }
}