package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName show_club
 */
@TableName(value ="show_club")
@Data
public class ShowClub implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private Long clubId;

    /**
     * 
     */
    private String clubName;

    /**
     * 
     */
    private String clubImage;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}