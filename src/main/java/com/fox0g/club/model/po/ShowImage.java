package com.fox0g.club.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName show_image
 */
@TableName(value ="show_image")
@Data
public class ShowImage implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 文件路径
     */
    private String imageUrl;

    /**
     * 文件key
     */
    private String imageKey;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}