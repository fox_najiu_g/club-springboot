package com.fox0g.club.model.vo;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-20 11:50
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class LoginVo {
    private String userPhone;
    private String userPassword;

    public LoginVo() {
    }

    public LoginVo(String userPhone, String userPassword) {
        this.userPhone = userPhone;
        this.userPassword = userPassword;
    }
}
