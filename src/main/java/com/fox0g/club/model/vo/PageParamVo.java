package com.fox0g.club.model.vo;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-27 20:32
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class PageParamVo {

    private Integer pageNum;
    private Integer pageSize;
    private Integer pageCount;

    public PageParamVo() {
    }

    public PageParamVo(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
}
