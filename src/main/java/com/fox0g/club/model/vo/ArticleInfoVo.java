package com.fox0g.club.model.vo;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-28 17:21
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ArticleInfoVo {

    /**
     * 发布者： 11：user 22：club
     */
    private Integer userOrClub;

    /**
     * 活动内容
     */
    private String articleContent;

    /**
     * 活动图片
     */
    private String articleImage;

    /**
     * 活动标签1
     */
    private String articleTag1;

    /**
     * 活动标签2
     */
    private String articleTag2;
}
