package com.fox0g.club.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @program: club-parent
 * @Date: 2023-02-27 09:31
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubAccountIsJoinVo {

    @NotNull
//    @Pattern(regexp = "^\d{10}$")
    private Long clubId;

    @NotNull
    private Integer clubIsJoin;
}
