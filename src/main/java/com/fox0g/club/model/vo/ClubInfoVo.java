package com.fox0g.club.model.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @program: club-parent
 * @Date: 2023-02-25 21:55
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubInfoVo {
    /**
     * 名称
     */
//    @Pattern(regexp = "^.{1,25}$" , message = "")
    @NotEmpty(message = "名称不能为空")
    @Length(max=25,message = "名称长度不符合规")
    private String clubName;

    /**
     * 名称
     */
    @NotEmpty(message = "地址不能为空")
    private String clubAddress;

    /**
     * 图片
     */
    @NotEmpty(message = "图片不能为空")
    private String clubImage;

    /**
     * 签名
     */
    @NotEmpty(message = "签名不能为空")
    @Length(max=15,message = "名称长度不符合规")
    private String clubSignature;

    /**
     * 介绍
     */
    @NotEmpty(message = "组织介绍不能为空")
    @Length(max=1000,message = "名称长度不符合规")
    private String clubIntroduce;

    /**
     * 申请原因
     */
    @NotEmpty(message = "申请原因不能为空")
    @Length(max=200,message = "名称长度不符合规")
    private String clubReason;
}
