package com.fox0g.club.model.vo;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-27 07:43
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubAccountVo {

    private Long clubId;
    private String clubEmail;
    private Long userId;
}
