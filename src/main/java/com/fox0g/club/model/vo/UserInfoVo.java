package com.fox0g.club.model.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-02-25 17:36
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserInfoVo {


    private Integer id;

    /**
     * 用户昵称
     */
    @Length(max=15,min=1)
    private String userName;

    /**
     * 用户id
     */
    @Min(value = 1000000000L)
    @Max(value = 9999999999L)
    private Long userId;

    /**
     * 用户头像
     */
    private String userImage;

    /**
     * 性别 男：1 女：2
     */

    private Integer userSex;

    /**
     * 地址
     */
    private String userAddress;

    /**
     * 出生日期
     */
    private LocalDate userBirth;

    /**
     * 个性签名
     */
    @Length(max=10)
    private String userSignature;

    /**
     * 创建日期
     */
    private LocalDate userRegistration;
}
