package com.fox0g.club.model.vo;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-25 19:18
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserAccountVo {

    private String newPass1;

    private String newPass2;

    private String newName;

    private String newPhone;

    private String newEmail;

    private String newQQ;

    private String newWx;
}
