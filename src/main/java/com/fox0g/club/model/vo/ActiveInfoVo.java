package com.fox0g.club.model.vo;

import lombok.Data;

import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-02-27 10:37
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveInfoVo {

    /**
     * 发布者： 11：user 22：club
     */
    private Integer userOrClub;

    /**
     * 活动名
     */
    private String activeName;

    /**
     * 活动时间
     */
    private LocalDate activeDate;

    /**
     * 活动地址
     */
    private String activeAddress;

    /**
     * 活动内容
     */
    private String activeContent;

    /**
     * 活动摘要
     */
    private String activeDesc;

    /**
     * 活动图片
     */
    private String activeImage;

    /**
     * 活动标签1
     */
    private String activeTag1;

    /**
     * 活动标签2
     */
    private String activeTag2;

}
