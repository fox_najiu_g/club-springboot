package com.fox0g.club.model.vo;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-13 17:56
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubApplyVo {


    private Long clubId;
    private Long userId;
    private String userName;
    private String applyRemark;
}
