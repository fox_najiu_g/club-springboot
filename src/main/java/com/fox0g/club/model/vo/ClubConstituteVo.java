package com.fox0g.club.model.vo;


import com.fox0g.club.model.po.ClubConstitute;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-01 17:21
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubConstituteVo extends ClubConstitute {
}
