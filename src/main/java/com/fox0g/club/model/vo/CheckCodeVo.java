package com.fox0g.club.model.vo;/**
 * @program: club-parent
 * @Date: 2023-02-25 10:08
 * @author: Fox0g
 * @description: TODO
 */


import com.fox0g.club.base_util.validation.ValidationGroups;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Fox0g
 * @version 1.0
 * @description TODO
 * @date 2023/2/25 10:08
 */
@Data
public class CheckCodeVo {


    @NotBlank(message="手机号不能为空",groups = {ValidationGroups.CheckCode.class})
    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$" , message = "手机号有误",groups = {ValidationGroups.CheckCode.class})
    private String phone;


    @Size(min=4,max=6,message = "验证码不合法",groups = {ValidationGroups.CheckCode.class})
    private String code;

    public CheckCodeVo() {
    }

    public CheckCodeVo(String phone, String code) {
        this.phone = phone;
        this.code = code;
    }
}
