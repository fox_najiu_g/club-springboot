package com.fox0g.club.model.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @program: club-parent
 * @Date: 2023-02-26 21:11
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UpdateClubVo {

    /**
     *
     */
    @NotNull
    private Long clubId;

    /**
     *
     */
    @NotEmpty(message = "名称不能为空")
    @Length(max=25,message = "名称长度不符合规")
    private String clubName;

    /**
     *
     */
    @NotEmpty(message = "地址不能为空")
    private String clubAddress;

    /**
     *
     */
    private String clubImage;

    /**
     *
     */
    @NotEmpty(message = "签名不能为空")
    @Length(max=20,message = "名称长度不符合规")
    private String clubSignature;

    /**
     *
     */
    @NotEmpty(message = "签名不能为空")
    @Length(max=1000,message = "名称长度不符合规")
    private String clubIntroduce;

    /**
     *
     */
    @NotEmpty
    private String clubRegistration;

}
