package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ClubConstitute;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-01 14:15
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubConstituteDto extends ClubConstitute {
}
