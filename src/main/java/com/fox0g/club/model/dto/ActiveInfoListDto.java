package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-12 16:21
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveInfoListDto {

    private Integer id;
    private Long publishId;
    private String publishName;
    private Integer userOrClub;
    private String activeName;
    private String activeDesc;
    private String activeImage;
    private String activeTag2;
    private LocalDateTime publishDate;

}
