package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-03 15:20
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserActivePassDto {

    private Integer id;
    private Integer activeId;
    private String activeName;
    private LocalDate activeDate;
    private LocalDateTime applyDate;
    private Integer isPass;

    public UserActivePassDto(Integer id, Integer activeId, String activeName, LocalDate activeDate, LocalDateTime applyDate, Integer isPass) {
        this.id = id;
        this.activeId = activeId;
        this.activeName = activeName;
        this.activeDate = activeDate;
        this.applyDate = applyDate;
        this.isPass = isPass;
    }

    public UserActivePassDto() {
    }


}
