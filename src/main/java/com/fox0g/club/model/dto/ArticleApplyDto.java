package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-06 10:37
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ArticleApplyDto {
    private Integer id;
    private LocalDateTime applyDate;
    private Integer userOrClub;
    private String publishName;
    private Long publishId;
    private String articleContent;
    private Integer articleStatus;
}
