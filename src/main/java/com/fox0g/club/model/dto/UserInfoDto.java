package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.UserInfo;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-25 17:26
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserInfoDto extends UserInfo {
}
