package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ClubApply;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-01 21:23
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubApplyDto extends ClubApply {
}
