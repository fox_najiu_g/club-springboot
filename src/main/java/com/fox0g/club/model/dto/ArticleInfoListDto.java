package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-13 11:35
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ArticleInfoListDto {

    private Integer id;
    private Long publishId;
    private String publishName;
    private String publishImage;
    private String articleContent;
    private String articleImage;
    private String articleTag1;
    private String articleTag2;
    private LocalDateTime publishDate;
    private Integer userOrClub;
}
