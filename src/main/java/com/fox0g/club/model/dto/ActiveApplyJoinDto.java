package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ActiveApply;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-02-27 21:42
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveApplyJoinDto extends ActiveApply {

    private Integer id;

    /**
     * 活动ID
     */
    private Integer activeId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 报名时间
     */
    private LocalDateTime applyDate;

    /**
     * 报名者姓名
     */
    private String userName;

    /**
     * 1:参与 2：未参与
     */
    private Integer isJoin;
    /**
     * :参与 2：未参与
     */
    private Integer isPass;



}
