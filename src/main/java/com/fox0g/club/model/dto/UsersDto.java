package com.fox0g.club.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-04-23 16:43
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UsersDto {
    private Long userId;
    private String userName;
    private String userImage;
    private String userSignature;
    private List<String> tagValue;
}
