package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-03-04 15:08
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserDto {

    private LocalDate userRegistration;
    private String userName;
    private Long userId;
    private String userRealName;
    private String userPhone;

    public UserDto() {
    }

    public UserDto(LocalDate userRegistration, String userName, Long userId, String userRealName, String userPhone) {
        this.userRegistration = userRegistration;
        this.userName = userName;
        this.userId = userId;
        this.userRealName = userRealName;
        this.userPhone = userPhone;
    }
}
