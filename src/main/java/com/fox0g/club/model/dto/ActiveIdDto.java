package com.fox0g.club.model.dto;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-03 11:18
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveIdDto {

    private Integer id;
    private Integer activeId;
}
