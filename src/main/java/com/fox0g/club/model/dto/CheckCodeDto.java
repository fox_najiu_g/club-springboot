package com.fox0g.club.model.dto;/**
 * @program: club-parent
 * @Date: 2023-02-25 08:57
 * @author: Fox0g
 * @description: TODO
 */

import lombok.Data;

/**
 * @author 86187
 * @version 1.0
 * @description TODO
 * @date 2023/2/25 8:57
 */
@Data
public class CheckCodeDto {



    String code;

    public CheckCodeDto(){};

    public CheckCodeDto(String code) {

        this.code = code;
    }




}
