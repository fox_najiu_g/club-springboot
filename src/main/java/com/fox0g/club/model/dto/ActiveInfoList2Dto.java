package com.fox0g.club.model.dto;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-13 09:47
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveInfoList2Dto extends ActiveInfoListDto{

    private String publishImage;
    private String activeTag1;
}
