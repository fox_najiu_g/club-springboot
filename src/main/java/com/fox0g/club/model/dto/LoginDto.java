package com.fox0g.club.model.dto;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-20 11:49
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class LoginDto {
    private String userPhone;
    private String userPassword;
}
