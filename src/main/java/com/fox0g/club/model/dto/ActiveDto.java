package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-03-05 19:38
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveDto {

    private Integer id;
    private LocalDate activeDate;
    private String activeName;
    private Integer userOrClub;
    private String publishName;
    private Long publishId;
    private Integer activeStatus;

}
