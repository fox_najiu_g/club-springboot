package com.fox0g.club.model.dto;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-26 13:41
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class OssUpDto {

    private String fileUrl;
    private String key;


    public OssUpDto() {
    }

    public OssUpDto(String fileUrl, String key) {
        this.fileUrl = fileUrl;
        this.key = key;

    }
}
