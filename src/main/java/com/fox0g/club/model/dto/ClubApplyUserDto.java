package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ClubApply;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-05 16:34
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubApplyUserDto extends ClubApply {
    private String clubName;
}
