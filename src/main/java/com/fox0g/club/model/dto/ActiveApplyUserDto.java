package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ActiveApply;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-06 07:51
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveApplyUserDto extends ActiveApply {

    private String activeName;
    private String userName;
}
