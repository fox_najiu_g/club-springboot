package com.fox0g.club.model.dto;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-26 16:01
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class AllClubVo {

    Long clubId;
    String clubName;

}
