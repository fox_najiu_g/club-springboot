package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ClubInfo;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-26 16:32
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubDto extends ClubInfo {
}
