package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-03 08:09
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserActiveApplyDto {

    /**
     * 用户（报名活动）的id
     */
    private Integer id;
    /**
     * 活动id
     */
    private Integer activeId;
    /**
     * 发布活动者ID
     */
    private Long publishId;
    /**
     * 发布活动者ID
     */
    private Integer userOrClub;

    /**
     * 参与活动日期
     */
    private LocalDate activeDate;
    /**
     * 发布活动者头像
     */
    private String publishImage;
    /**
     * 发布活动者名称
     */
    private String publishName;
    /**
     * 发布活动时间
     */
    private LocalDateTime publishDate;
    /**
     * 活动名称
     */
    private String activeName;
    /**
     * 活动内容
     */
    private String activeDesc;

    public UserActiveApplyDto() {
    }

    public UserActiveApplyDto(Integer id, Integer activeId, Long publishId, LocalDate activeDate, String publishImage, String publishName, LocalDateTime publishDate, String activeName, String activeDesc) {
        this.id = id;
        this.activeId = activeId;
        this.publishId = publishId;
        this.activeDate = activeDate;
        this.publishImage = publishImage;
        this.publishName = publishName;
        this.publishDate = publishDate;
        this.activeName = activeName;
        this.activeDesc = activeDesc;
    }
}
