package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-02-28 21:43
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ArticleDto {

    private Integer id;

    private String articleContent;

    private String articleImage;

    private LocalDateTime applyDate;

    private LocalDateTime publishDate;

    private Integer articleStatus;

}
