package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-06 08:48
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ArticlePassDto {

    private Integer id;
    private Integer userOrClub;
    private Long publishId;
    private String publishName;
    private String articleContent;
    private LocalDateTime publishDate;
}
