package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-09 16:14
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveInfoDto {

    private Integer id;
    private String activeName;
    private Integer activeStatus;
    private LocalDate activeDate;
    private LocalDateTime publishDate;
    private String activeAddress;
    private String activeDesc;
    private String activeImage;
}
