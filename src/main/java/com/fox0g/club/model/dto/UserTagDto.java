package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.UserTag;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-04 12:15
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserTagDto extends UserTag {
}
