package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-03-05 15:16
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ApplyClubDto {

    private Integer id;
    private LocalDate applyDate;
    private Long clubId;
    private String clubName;
    private Long userId;
    private String clubReason;
    private Integer applyStatus;
}
