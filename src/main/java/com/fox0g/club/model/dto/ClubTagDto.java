package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.ClubTag;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-02 14:53
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubTagDto extends ClubTag {
}
