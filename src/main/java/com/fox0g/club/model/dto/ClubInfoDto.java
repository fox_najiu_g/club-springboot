package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-03-05 11:21
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubInfoDto {

    private Long clubId;
    private Long userId;
    private String clubName;
    private LocalDate clubRegistration;
}
