package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-03 10:39
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveStatusApplyDto {

    private Integer id;
    /**
     * 活动名
     */
    private String activeName;
    /**
     * 活动时间
     */
    private LocalDate activeDate;
    /**
     * 活动状态 1:活动中 2:已结束 3:审核中 4:已通过 5:未通过
     */
    private Integer activeStatus;
    /**
     * 提交申请时间
     */
    private LocalDateTime applyDate;
}
