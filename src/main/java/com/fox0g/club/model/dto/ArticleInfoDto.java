package com.fox0g.club.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @program: club-parent
 * @Date: 2023-03-10 18:53
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ArticleInfoDto {

    private Integer id;
    private LocalDateTime publishDate;
    private String articleContent;

}
