package com.fox0g.club.model.dto;


import com.fox0g.club.model.po.UserAccount;
import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-02-25 18:42
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class UserAccountDto extends UserAccount {
}
