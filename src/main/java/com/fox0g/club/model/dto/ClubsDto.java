package com.fox0g.club.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-09 14:00
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ClubsDto {

    private String clubImage;
    private String clubName;
    private String clubSignature;
    private Long clubId;
    private List<String> tagValue;

}
