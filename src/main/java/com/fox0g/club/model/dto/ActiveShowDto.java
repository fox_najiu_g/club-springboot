package com.fox0g.club.model.dto;

import lombok.Data;

/**
 * @program: club-parent
 * @Date: 2023-03-08 13:57
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class ActiveShowDto {

    private Integer id;
    private String activeName;
    private String activeImage;
    private Integer activeStatus;
}
