package com.fox0g.club.show_service.controller.api;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.po.ShowImage;
import com.fox0g.club.show_service.service.ShowImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-07 08:53
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/show/manage")
public class ImageControllerApi {

    @Autowired
    private ShowImageService showImageService;

    @GetMapping("/getAllImages")
    public Result getAllImages(){

        List<ShowImage> imageList = showImageService.list();
        return Result.success(imageList);
    }

    @PutMapping("/addImage")
    public Result addImage(@RequestBody ShowImage showImage){

        boolean save = showImageService.save(showImage);
        return save ? Result.success("操作成功"):Result.fail("操作失败");

    }

    @DeleteMapping("/deleteImage/{id}")
    public Result deleteImage(@PathVariable Integer id){
        boolean remove = showImageService.removeById(id);
        return remove ? Result.success("操作成功"):Result.fail("操作失败");
    }
}
