package com.fox0g.club.show_service.controller.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.fox0g.club.active_service.controller.ActiveInfoController;
import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.po.ShowActive;
import com.fox0g.club.show_service.service.ShowActiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-09 11:43
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/show/manage")
public class ActiveControllerApi {
    @Autowired
    private ShowActiveService showActiveService;
    @Autowired
    private ActiveInfoController activeInfoController;

    @GetMapping("/getAllActive")
    public Result getAllActive(){

        List<ShowActive> list = showActiveService.list();
        return Result.success(list);
    }
    @GetMapping("/getActiveList")
    public Result getActiveList(){
        List<ShowActive> list = activeInfoController.getActiveList();
        return Result.success(list);
    }

    @PutMapping("/addShowActive")
    public Result addShowActive(@RequestBody ShowActive showActive){
        LambdaQueryWrapper<ShowActive> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShowActive::getActiveId,showActive.getActiveId());
        ShowActive active = showActiveService.getOne(queryWrapper);
        if(!ObjectUtils.isEmpty(active)){
            return Result.fail("请勿重复添加");
        }
        boolean save = showActiveService.save(showActive);
        return save ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @DeleteMapping("/deleteActive/{id}")
    public Result deleteActive(@PathVariable("id")Integer id){
        boolean remove = showActiveService.removeById(id);
        return remove ? Result.success("操作成功"):Result.fail("操作失败");
    }
}
