package com.fox0g.club.show_service.controller.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.club_service.controller.ClubInfoController;
import com.fox0g.club.model.po.ShowClub;
import com.fox0g.club.show_service.service.ShowClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-07 16:04
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/show/manage")
public class ClubControllerApi {

    @Autowired
    private ShowClubService showClubService;

    @Autowired
    private ClubInfoController clubInfoController;

    @GetMapping("/getAllClub")
    public Result getAllClub(){
        List<ShowClub> list = showClubService.list();
        return Result.success(list);
    }

    @GetMapping("/getClubList")
    public Result getClubList(){
        List<ShowClub> list = clubInfoController.getClubList();
        return Result.success(list);
    }

    @PutMapping("/addShowClub")
    public Result addShowClub(@RequestBody ShowClub showClub){
        LambdaQueryWrapper<ShowClub> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShowClub::getClubId,showClub.getClubId());
        ShowClub club = showClubService.getOne(queryWrapper);
        if(!ObjectUtils.isEmpty(club)){
            return Result.fail("请勿重复添加");
        }
        boolean save = showClubService.save(showClub);
        return save ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @DeleteMapping("/deleteClub/{id}")
    public Result deleteClub(@PathVariable("id")Integer id){
        boolean remove = showClubService.removeById(id);
        return remove ? Result.success("操作成功"):Result.fail("操作失败");
    }
}
