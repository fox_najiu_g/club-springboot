package com.fox0g.club.show_service.controller.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.po.ShowTag;
import com.fox0g.club.show_service.service.ShowTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-07 10:43
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/show/manage")
public class TagControllerApi {

    @Autowired
    private ShowTagService showTagService;

    @GetMapping("/getAllTag/{tagType}")
    public Result getAllTag(@PathVariable("tagType")Integer tagType){
        List<ShowTag> allTagByTagType = showTagService.getAllTagByTagType(tagType);
        return Result.success(allTagByTagType);
    }

    @PutMapping("/addTag")
    public Result getAllTag(@RequestBody ShowTag showTag){
        LambdaQueryWrapper<ShowTag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShowTag::getTagType,showTag.getTagType()).eq(ShowTag::getTagValue,showTag.getTagValue());
        ShowTag tag = showTagService.getOne(queryWrapper);
        if(!ObjectUtils.isEmpty(tag)){
            return Result.fail("请勿重复添加");
        }
        boolean save = showTagService.save(showTag);
        return save ? Result.success("操作成功"):Result.fail("操作失败");
    }

    @DeleteMapping("/deleteTag/{id}")
    public Result deleteTag(@PathVariable("id")Integer id){
        boolean remove = showTagService.removeById(id);
        return remove ? Result.success("操作成功"):Result.fail("操作失败");
    }

}
