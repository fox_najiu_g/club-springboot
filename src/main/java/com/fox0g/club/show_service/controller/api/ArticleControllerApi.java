package com.fox0g.club.show_service.controller.api;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.po.ShowArticle;
import com.fox0g.club.show_service.service.ShowArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-07 16:41
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/show/manage")
public class ArticleControllerApi {


    @Autowired
    private ShowArticleService showArticleService;

    @GetMapping("/getAllArticle")
    public Result getAllArticle(){
        List<ShowArticle> list = showArticleService.list();
        return Result.success(list);
    }

    @DeleteMapping("/deleteArticle/{id}")
    public Result deleteArticle(@PathVariable("id")Integer id){
        boolean remove = showArticleService.removeById(id);
        return remove ? Result.success("操作成功"):Result.fail("操作失败");
    }
}
