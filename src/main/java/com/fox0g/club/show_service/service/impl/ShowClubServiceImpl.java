package com.fox0g.club.show_service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.po.ShowClub;
import com.fox0g.club.show_service.mapper.ShowClubMapper;
import com.fox0g.club.show_service.service.ShowClubService;
import org.springframework.stereotype.Service;

/**
* @author 86187
* @description 针对表【show_club】的数据库操作Service实现
* @createDate 2023-03-07 16:03:54
*/
@Service
public class ShowClubServiceImpl extends ServiceImpl<ShowClubMapper, ShowClub>
    implements ShowClubService {

}




