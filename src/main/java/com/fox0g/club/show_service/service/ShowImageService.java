package com.fox0g.club.show_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.ShowImage;


/**
* @author 86187
* @description 针对表【show_image】的数据库操作Service
* @createDate 2023-03-07 08:52:14
*/
public interface ShowImageService extends IService<ShowImage> {

}
