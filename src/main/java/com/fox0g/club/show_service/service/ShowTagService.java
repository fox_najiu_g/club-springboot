package com.fox0g.club.show_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.ShowTag;


import java.util.List;

/**
* @author 86187
* @description 针对表【show_tag】的数据库操作Service
* @createDate 2023-03-07 10:42:27
*/
public interface ShowTagService extends IService<ShowTag> {

    List<ShowTag> getAllTagByTagType(Integer tagType);
}
