package com.fox0g.club.show_service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.po.ShowArticle;
import com.fox0g.club.show_service.mapper.ShowArticleMapper;
import com.fox0g.club.show_service.service.ShowArticleService;
import org.springframework.stereotype.Service;

/**
* @author 86187
* @description 针对表【show_article】的数据库操作Service实现
* @createDate 2023-03-07 16:52:29
*/
@Service
public class ShowArticleServiceImpl extends ServiceImpl<ShowArticleMapper, ShowArticle>
    implements ShowArticleService {

}




