package com.fox0g.club.show_service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.po.ShowImage;
import com.fox0g.club.show_service.mapper.ShowImageMapper;
import com.fox0g.club.show_service.service.ShowImageService;
import org.springframework.stereotype.Service;

/**
* @author 86187
* @description 针对表【show_image】的数据库操作Service实现
* @createDate 2023-03-07 08:52:14
*/
@Service
public class ShowImageServiceImpl extends ServiceImpl<ShowImageMapper, ShowImage>
    implements ShowImageService {

}




