package com.fox0g.club.show_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.po.ShowTag;
import com.fox0g.club.show_service.mapper.ShowTagMapper;
import com.fox0g.club.show_service.service.ShowTagService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 86187
* @description 针对表【show_tag】的数据库操作Service实现
* @createDate 2023-03-07 10:42:27
*/
@Service
public class ShowTagServiceImpl extends ServiceImpl<ShowTagMapper, ShowTag>
    implements ShowTagService {

    @Override
    public List<ShowTag> getAllTagByTagType(Integer tagType) {
        LambdaQueryWrapper<ShowTag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShowTag::getTagType,tagType);
        List<ShowTag> showTags = this.baseMapper.selectList(queryWrapper);
        return showTags;
    }
}




