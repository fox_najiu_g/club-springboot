package com.fox0g.club.show_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.ShowArticle;


/**
* @author 86187
* @description 针对表【show_article】的数据库操作Service
* @createDate 2023-03-07 16:52:29
*/
public interface ShowArticleService extends IService<ShowArticle> {

}
