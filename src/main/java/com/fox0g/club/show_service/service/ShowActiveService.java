package com.fox0g.club.show_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.ShowActive;


/**
 * @program: club-parent
 * @Date: 2023-03-09 11:42
 * @author: Fox0g
 * @description: TODO
 */
public interface ShowActiveService extends IService<ShowActive> {
}
