package com.fox0g.club.show_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.ShowClub;

/**
* @author 86187
* @description 针对表【show_club】的数据库操作Service
* @createDate 2023-03-07 16:03:54
*/
public interface ShowClubService extends IService<ShowClub> {

}
