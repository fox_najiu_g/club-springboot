package com.fox0g.club.show_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.ShowActive;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 86187
* @description 针对表【show_article】的数据库操作Mapper
* @createDate 2023-03-07 16:52:29
* @Entity com.fox0g.model.po.ShowArticle
*/
@Mapper
public interface ShowActiveMapper extends BaseMapper<ShowActive> {

}




