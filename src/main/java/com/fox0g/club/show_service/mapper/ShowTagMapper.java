package com.fox0g.club.show_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.ShowTag;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 86187
* @description 针对表【show_tag】的数据库操作Mapper
* @createDate 2023-03-07 10:42:27
* @Entity com.fox0g.model.po.ShowTag
*/
@Mapper
public interface ShowTagMapper extends BaseMapper<ShowTag> {

}




