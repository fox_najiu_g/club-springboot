package com.fox0g.club.show_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.ShowClub;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 86187
* @description 针对表【show_club】的数据库操作Mapper
* @createDate 2023-03-07 16:03:54
* @Entity com.fox0g.model.po.ShowClub
*/
@Mapper
public interface ShowClubMapper extends BaseMapper<ShowClub> {

}




