package com.fox0g.club.user_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.vo.LoginVo;
import com.fox0g.club.user_service.service.UserAccountService;
import com.fox0g.club.user_service.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @program: club-parent
 * @Date: 2023-03-20 11:43
 * @author: Fox0g
 * @description: TODO
 */
@RequestMapping("/user")
@RestController
public class LoginController {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private UserAccountService userAccountService;

    //用户手机号登录接口
    @PostMapping("/login")
    public Result login(@RequestBody LoginVo loginVo) {
        Map<String,Object> info = userAccountService.loginUser(loginVo);
        return Result.success(info);
    }
}
