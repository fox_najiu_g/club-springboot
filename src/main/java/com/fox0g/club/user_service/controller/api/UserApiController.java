package com.fox0g.club.user_service.controller.api;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.UserDto;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.user_service.service.UserAccountService;
import com.fox0g.club.user_service.service.UserInfoService;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @program: club-parent
 * @Date: 2023-03-04 14:26
 * @author: Fox0g
 * @description: TODO
 */
@Slf4j
@RestController
@RequestMapping("/user/manage")
public class UserApiController {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private UserAccountService userAccountService;

    /*
     * @Description: 获取所有用户
     * @Param: pageParamVo
     * @return: Result
     * @Date: 2023/3/5
    */
    @GetMapping("/getAllUser")
    public Result getAllUser(PageParamVo pageParamVo){

        PageInfo<UserDto> userDtoPageInfo = userInfoService.selectAllUserList(pageParamVo);
        return Result.success(userDtoPageInfo);
    }

    @PostMapping("/resetUserPassword/{userId}")
    public Result resetUserPassword(@PathVariable Long userId,@RequestBody String password){
        password=password.substring(0,password.length()-1);
        log.info(userId+"-重置密码-"+password);
        boolean isUpdate = userAccountService.updateUserPassword(userId, password);
        return isUpdate ? Result.success("重置成功"):Result.fail("重置失败，请稍后重试");
    }

    @DeleteMapping("/deleteUser/{userId}")
    public Result deleteUser(@PathVariable Long userId){
        boolean isDelete = userAccountService.deleteUserInfoAndUserAccountByUserId(userId);
        return isDelete ? Result.success("删除成功"):Result.fail("删除失败，请稍后重试");
    }

}
