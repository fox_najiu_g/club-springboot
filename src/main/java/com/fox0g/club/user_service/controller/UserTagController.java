package com.fox0g.club.user_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.UserTagDto;
import com.fox0g.club.model.po.UserTag;
import com.fox0g.club.user_service.service.UserTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: club-parent
 * @Date: 2023-03-02 14:45
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/user")
public class UserTagController {

    @Autowired
    private UserTagService userTagService;

    /*
     * @Description: 根据userId查询所有 标签类型的tag
     * @Param: tagType
 * @param userId
     * @return: Result
     * @Date: 2023/3/2
    */
    @GetMapping("/getUserTagBy/{tagType}/{userId}")
    public Result getUserTagByTagTypeUserId(@PathVariable("tagType")Integer tagType,@PathVariable("userId")Long userId){

        List<UserTagDto> tagList = userTagService.getUserTagByTagTypeUserId(tagType, userId);
        return Result.success("成功",tagList);
    }

    /*
     * @Description: 根据标签id和userId确定并删除
     * @Param: id
 * @param userId
     * @return: Result
     * @Date: 2023/3/2
    */
    @DeleteMapping("/deleteUserTagBy/{id}/{userId}")
    public Result deleteUserTagByIdUserId(@PathVariable("id")Integer id,@PathVariable("userId")Long userId){
        boolean isDelete = userTagService.deleteUserTagByIdUserId(id, userId);
        return isDelete ? Result.success("成功"):Result.fail("失败");
    }

    @PutMapping("/deleteUserTagBy/{tagType}/{userId}")
    public Result joinMultipleActiveApplyById(@PathVariable("tagType")Integer tagType,@PathVariable("userId")Long userId,@RequestBody List<String> userTagList){


        //查找当前共有几个
        int count = userTagService.getUserTagCountByTagType(tagType,userId);

        //判断类型 是否过量
        if(tagType==441 && (count+userTagList.size())>3){
            //主标签
            //过量
            return Result.fail("认证标签已过量！");
        }
        if(tagType==442 && (count+userTagList.size())>5){
            //副标签
            //过量
            return Result.fail("认证标签已过量！");
        }
        //未过量
        userTagList.stream().forEach(userTag -> {
            userTagService.save(new UserTag(tagType, userTag, userId));
        });
        return Result.success("成功");

    }


}
