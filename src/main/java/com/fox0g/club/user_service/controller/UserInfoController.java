package com.fox0g.club.user_service.controller;



import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.UserInfoDto;
import com.fox0g.club.model.dto.UsersDto;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.model.vo.UserInfoVo;
import com.fox0g.club.user_service.service.UserInfoService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: club-parent
 * @Date: 2023-02-25 13:33
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/user")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /*
     * @Description: 根据userId查询该用户信息
     * @Param: userId
     * @return: Result
     * @Date: 2023/3/2
    */
    @GetMapping("/auth/getUserInfoByUserID/{userId}")
    public Result getUserInfoByUserID(@PathVariable Long userId){
        //简单校验 数据
        if(Long.toString(userId).length()!=10){
            return Result.fail();
        }
        //根据userId查询userInfo
        UserInfo userInfo = userInfoService.getByUserId(userId);
        UserInfoDto userInfoDto = new UserInfoDto();
        BeanUtils.copyProperties(userInfo,userInfoDto);
        if(ObjectUtils.isEmpty(userInfoDto)){
            return Result.fail("获取失败");
        }

        return Result.success("获取成功",userInfoDto);
    }

    @GetMapping("/show/getUserInfoByUserID/{userId}")
    public Result getUserInfoSHowByUserID(@PathVariable Long userId){
        //简单校验 数据
        if(Long.toString(userId).length()!=10){
            return Result.fail();
        }
        //根据userId查询userInfo
        UserInfo userInfo = userInfoService.getByUserId(userId);
        UserInfoDto userInfoDto = new UserInfoDto();
        BeanUtils.copyProperties(userInfo,userInfoDto);
        if(ObjectUtils.isEmpty(userInfoDto)){
            return Result.fail("获取失败");
        }

        return Result.success("获取成功",userInfoDto);
    }

    /*
     * @Description: 更新用户信息
     * @Param: vo
     * @return: Result
     * @Date: 2023/3/2
    */
    @PostMapping("/auth/updateUserInfo")
    public Result updateUserInfo(@Validated @RequestBody UserInfoVo vo){
        //简单校验 数据合法
        Long userId = vo.getUserId();
        //根据userId 更新userInfo
        boolean isUpdate = userInfoService.updateByUserId(userId, vo);
        if(!isUpdate){
            return Result.fail("修改失败");
        }
        return Result.success("修改成功");

    }

    /*
     * @Description: 根据userId查询该用户信息
     * @Param: userId
     * @return: UserInfo
     * @Date: 2023/3/2
    */
    @GetMapping("/getUserInfoByUserId/{userId}")
    public UserInfo getUserInfoByUserId(@PathVariable Long userId){
        UserInfo userInfo = userInfoService.selectUserInfoByUserId(userId);
        return userInfo;
    }

    @GetMapping("/searchUserListByParam/{searchParam}")
    public Result searchUserListByParam(@PathVariable String searchParam, PageParamVo pageParam){

        PageInfo<UsersDto> list = userInfoService.getUserListBySearch(searchParam,pageParam.getPageNum(),pageParam.getPageSize());
        return Result.success(list);
    }
}
