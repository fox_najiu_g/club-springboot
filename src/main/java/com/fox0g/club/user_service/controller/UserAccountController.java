package com.fox0g.club.user_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.base_util.utils.RandomIdUtil;
import com.fox0g.club.base_util.utils.RandomStringUtil;
import com.fox0g.club.model.dto.UserAccountDto;
import com.fox0g.club.model.po.UserAccount;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.UserAccountVo;
import com.fox0g.club.user_service.service.UserAccountService;
import com.fox0g.club.user_service.service.UserInfoService;
import com.fox0g.club.user_service.service.UserTagService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

/**
 * @program: club-parent
 * @Date: 2023-02-25 18:37
 * @author: Fox0g
 * @description: TODO
 */
@RestController
@RequestMapping("/user")
public class UserAccountController {


    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private UserTagService userTagService;



    @GetMapping("/auth/getUserAccountByUserID/{userId}")
    public Result getUserAccountByUserID(@PathVariable Long userId){
        //简单校验 数据
        if(Long.toString(userId).length()!=10){
            return Result.fail();
        }
        //根据userId查询userAccount
        UserAccount userAccount = userAccountService.getByUserId(userId);
        UserAccountDto userAccountDto = new UserAccountDto();
        BeanUtils.copyProperties(userAccount,userAccountDto);
        if(ObjectUtils.isEmpty(userAccountDto)){
            return Result.fail();
        }

        return Result.success(userAccountDto);
    }
    @GetMapping("/show/getUserAccountByUserID/{userId}")
    public Result getUserAccountShowByUserID(@PathVariable Long userId){
        //简单校验 数据
        if(Long.toString(userId).length()!=10){
            return Result.fail();
        }
        //根据userId查询userAccount
        UserAccount userAccount = userAccountService.getByUserId(userId);
        UserAccountDto userAccountDto = new UserAccountDto();
        BeanUtils.copyProperties(userAccount,userAccountDto);
        if(ObjectUtils.isEmpty(userAccountDto)){
            return Result.fail();
        }

        return Result.success(userAccountDto);
    }

    @PostMapping("/auth/updateUserAccount/{userId}")
    public Result updateUserAccount(@RequestBody UserAccountVo userAccountVo,
                                    @PathVariable Long userId){
        //简单校验 数据
        if(Long.toString(userId).length()!=10 ||ObjectUtils.isEmpty(userAccountVo)){
            return Result.fail();
        }
        //是否存在该用户
        UserAccount userAccount = userAccountService.getByUserId(userId);
        if(ObjectUtils.isEmpty(userAccount)){
            return Result.fail();
        }
        //存在该用户
        UserAccountDto userAccountDto = new UserAccountDto();
        userAccountDto.setUserId(userId);
        if(StringUtils.isNotEmpty(userAccountVo.getNewPass2())){
            userAccountDto.setUserPassword(userAccountVo.getNewPass2());
        }
        if(StringUtils.isNotEmpty(userAccountVo.getNewName())){
            userAccountDto.setUserRealName(userAccountVo.getNewName());
        }
        if(StringUtils.isNotEmpty(userAccountVo.getNewPhone())){
            userAccountDto.setUserPhone(userAccountVo.getNewPhone());
        }
        if(StringUtils.isNotEmpty(userAccountVo.getNewEmail())){
            userAccountDto.setUserEmail(userAccountVo.getNewEmail());
        }
        if(StringUtils.isNotEmpty(userAccountVo.getNewQQ())){
            userAccountDto.setUserQq(userAccountVo.getNewQQ());
        }
        if(StringUtils.isNotEmpty(userAccountVo.getNewWx())){
            userAccountDto.setUserWx(userAccountVo.getNewWx());
        }
        boolean isUpdate = userAccountService.updateByUserId(userId, userAccountDto);
        if(!isUpdate){
            return Result.fail("操作失败");
        }
        return Result.success("操作成功");


    }


    @DeleteMapping("/auth/deleteUserByUserId/{userId}")
    public Result deleteUserByUserId(@PathVariable Long userId){
        //简单校验 数据
        if(Long.toString(userId).length()!=10){
            return Result.fail();
        }

        //根据userId 删除该用户
        boolean isDelete = userAccountService.deleteUserInfoAndUserAccountByUserId(userId);
        if(!isDelete){
            return Result.fail("注销失败，请稍后再试");
        }
        return Result.success("注销成功");
    }

    @PutMapping("/addUser/{userPhone}")
    public String addUserBuUserPhone(@PathVariable String userPhone){
        Long userId = userAccountService.getUserIdByUserPhone(userPhone);
        if(userId!=null){
            //该手机号已存在
            return null;
        }
        //注册该账号
        UserAccount userAccount = new UserAccount();
        UserInfo userInfo = new UserInfo();
        //生成随机用户ID
        userId = RandomIdUtil.getRandomId("11");
        //生成随机密码
        String userPassword = RandomStringUtil.getRandomString(8);

        userAccount.setUserId(userId);
        userAccount.setUserPassword(userPassword);
        userAccount.setUserPhone(userPhone);
        userAccountService.save(userAccount);

        userInfo.setUserName(userId.toString());
        userInfo.setUserId(userId);
        userInfo.setUserRegistration(LocalDate.now());
        userInfo.setUserBirth(LocalDate.now());
        userInfo.setUserSex(1);
        userInfoService.save(userInfo);

        return userPassword;

    }
}
