package com.fox0g.club.user_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.dto.UserTagDto;
import com.fox0g.club.model.po.UserTag;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【user_tag】的数据库操作Mapper
* @createDate 2023-03-04 12:12:34
* @Entity com.fox0g.model.po.UserTag
*/
@Mapper
public interface UserTagMapper extends BaseMapper<UserTag> {
    @Select("select * from user_tag where tag_type=#{tagType}  and user_id=#{userId}")
    @Results(value ={
            @Result(column = "id",property = "id"),
            @Result(column = "tag_type",property = "tagType"),
            @Result(column = "tag_value",property = "tagValue"),
            @Result(column = "user_id",property = "userId"),
    })
    List<UserTagDto> getUserTagByTagTypeUserId(@Param("tagType") Integer tagType, @Param("userId")Long userId);

    @Delete("delete from user_tag where id=#{id} and user_id=#{userId}")
    boolean deleteUserTagByIdAndUserId(@Param("id")Integer id,@Param("userId")Long userId);

    @Select("select tag_value from user_tag where user_id=#{userId} and tag_type=#{tagType}")
    List<String> getTagValueByUserIdTagValue(@Param("userId")Long userId,@Param("tagType")Integer tagType);


}




