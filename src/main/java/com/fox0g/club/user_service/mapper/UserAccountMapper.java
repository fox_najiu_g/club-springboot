package com.fox0g.club.user_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.po.UserAccount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
* @author 86187
* @description 针对表【user_account】的数据库操作Mapper
* @createDate 2023-02-25 18:35:19
* @Entity com.fox0g.model.po.UserAccount
*/
@Mapper
public interface UserAccountMapper extends BaseMapper<UserAccount> {


    @Select("select user_id from user_account where user_phone = #{userPhone}")
    Long selectUserIdByUserPhone(@Param("userPhone")String userPhone);
}




