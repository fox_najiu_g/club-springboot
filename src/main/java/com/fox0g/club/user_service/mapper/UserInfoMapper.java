package com.fox0g.club.user_service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.fox0g.club.model.dto.UserDto;
import com.fox0g.club.model.po.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author 86187
* @description 针对表【user_info】的数据库操作Mapper
* @createDate 2023-02-25 13:31:13
* @Entity com.fox0g.user.do.UserInfo
*/
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {


    @Select("select user_registration,user_name,user_info.user_id,user_real_name,user_phone from user_info,user_account where user_info.user_id=user_account.user_id ")
    @Results(value ={
            @Result(column = "user_registration",property = "userRegistration"),
            @Result(column = "user_name",property = "userName"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "user_real_name",property = "userRealName"),
            @Result(column = "user_phone",property = "userPhone"),
    })
    List<UserDto> selectUserList();

    @Select("select user_name from user_info where user_id=#{userId}")
    String getUserNameByUserId(@Param("userId")Long userId);

    @Select("SELECT user_id " +
            "from user_info " +
            "where user_name like '%${searchParam}%' ")
    List<Long> getUserIdListBySearchParam1(String searchParam);

    @Select("SELECT distinct user_info.user_id " +
            "from user_info,user_tag " +
            "where user_info.user_id=user_tag.user_id and (user_name like '%${searchParam}%' or tag_value like '%${searchParam}%')")
    List<Long> getUserIdListBySearchParam2(String searchParam);
}




