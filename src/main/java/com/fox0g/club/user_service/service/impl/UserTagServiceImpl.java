package com.fox0g.club.user_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.dto.UserTagDto;
import com.fox0g.club.model.po.UserTag;
import com.fox0g.club.user_service.mapper.UserTagMapper;
import com.fox0g.club.user_service.service.UserTagService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 86187
* @description 针对表【user_tag】的数据库操作Service实现
* @createDate 2023-03-04 12:12:34
*/
@Service
public class UserTagServiceImpl extends ServiceImpl<UserTagMapper, UserTag>
    implements UserTagService {

    @Override
    public List<UserTagDto> getUserTagByTagTypeUserId(Integer tagType, Long userId) {
        List<UserTagDto> tagList = this.baseMapper.getUserTagByTagTypeUserId(tagType, userId);
        return tagList;
    }

    @Override
    public boolean deleteUserTagByIdUserId(Integer id, Long userId) {
        return this.baseMapper.deleteUserTagByIdAndUserId(id, userId);
    }

    @Override
    public int getUserTagCountByTagType(Integer tagType, Long userId) {
        LambdaQueryWrapper<UserTag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserTag::getTagType,tagType).eq(UserTag::getUserId,userId);
        int count = this.count(queryWrapper);
        return count;
    }
}




