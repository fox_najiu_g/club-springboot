package com.fox0g.club.user_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.base_util.exception.ClubException;
import com.fox0g.club.base_util.helper.JwtHelper;
import com.fox0g.club.base_util.result.ResultCodeEnum;
import com.fox0g.club.model.po.UserAccount;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.LoginVo;
import com.fox0g.club.user_service.mapper.UserAccountMapper;
import com.fox0g.club.user_service.mapper.UserInfoMapper;
import com.fox0g.club.user_service.service.UserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
* @author 86187
* @description 针对表【user_account】的数据库操作Service实现
* @createDate 2023-02-25 18:35:19
*/
@Slf4j
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount>
    implements UserAccountService {


    @Autowired
    private UserAccountMapper userAccountMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserAccount getByUserId(Long userId) {

        LambdaQueryWrapper<UserAccount> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserAccount::getUserId,userId);
        UserAccount userAccount = this.baseMapper.selectOne(queryWrapper);
        return userAccount;
    }

    @Override
    public boolean updateByUserId(Long userId, Object object) {
        UserAccount userAccount = new UserAccount();
        BeanUtils.copyProperties(object,userAccount);
        if(userId!=userAccount.getUserId()){
            return false;
        }
        LambdaQueryWrapper<UserAccount> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserAccount::getUserId,userId);

        this.baseMapper.update(userAccount,queryWrapper);
        return true;
    }

    @Transactional
    @Override
    public boolean deleteUserInfoAndUserAccountByUserId(Long userId) {
        LambdaQueryWrapper<UserAccount> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(UserAccount::getUserId,userId);

        LambdaQueryWrapper<UserInfo> queryWrapper2 = new LambdaQueryWrapper<>();
        queryWrapper2.eq(UserInfo::getUserId,userId);

        int delete1 = userAccountMapper.delete(queryWrapper1);
        int delete2 = userInfoMapper.delete(queryWrapper2);
        if(delete1==0 || delete2==0){
            throw new ClubException(ResultCodeEnum.FAIL);
        }
        return true ;
    }

    @Override
    public boolean updateUserPassword(Long userId, String password) {
        UserAccount userAccount = this.getByUserId(userId);
        if(ObjectUtils.isEmpty(userAccount)){
            return false;
        }
        userAccount.setUserPassword(password);
        boolean isUpdate = this.updateById(userAccount);
        return isUpdate;
    }

    @Override
    public Map<String, Object> loginUser(LoginVo loginVo) {
        //从loginVo获取输入的手机号，和密码
        String phone = loginVo.getUserPhone();
        String password = loginVo.getUserPassword();

        //判断手机号和密码是否为空
        if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)) {
            throw new ClubException(ResultCodeEnum.Login1_ERROR);
        }


        //判断是否注册：根据手机号查询数据库，如果不存在相同手机号就是未注册
        QueryWrapper<UserAccount> wrapper = new QueryWrapper<>();
        wrapper.eq("user_phone",phone);
        UserAccount userAccount = this.baseMapper.selectOne(wrapper);

        if(userAccount == null) { //不存在该手机号，即未注册
            throw new ClubException(ResultCodeEnum.Login2_ERROR);
        }

        //已注册,判断密码是否正确
        if(!userAccount.getUserPassword().equals(loginVo.getUserPassword())){
            //密码不正确
            throw new ClubException(ResultCodeEnum.Login3_ERROR);
        }
        //密码正确
        //返回登录信息
        //返回token信息
        Map<String, Object> map = new HashMap<>();
        String userPhone = userAccount.getUserPhone();
        map.put("userPhone",userPhone);
        Long userId = userAccount.getUserId();
        map.put("userId",userId);
        String userName = userInfoMapper.getUserNameByUserId(userId);
        map.put("userName",userName);

        //jwt生成token字符串
        String token = JwtHelper.createToken(userPhone,userAccount.getUserPassword());
        map.put("token",token);

        return map;
    }

    @Override
    public Long getUserIdByUserPhone(String userPhone) {
        Long userId = this.baseMapper.selectUserIdByUserPhone(userPhone);
        return userId;
    }

}




