package com.fox0g.club.user_service.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.fox0g.club.model.dto.UserDto;
import com.fox0g.club.model.dto.UsersDto;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.github.pagehelper.PageInfo;

/**
* @author 86187
* @description 针对表【user_info】的数据库操作Service
* @createDate 2023-02-25 13:31:13
*/
public interface UserInfoService extends IService<UserInfo> {

    /*
     * @Description: 根据用户id获取用户信息
     * @Param: userId
     * @return: UserInfo
     * @Date: 2023/2/25
    */
    UserInfo getByUserId(Long userId);

    /*
     * @Description: 根据用户id更新该用户信息
     * @Param: userId
     * @return: void
     * @Date: 2023/2/25
    */
    boolean updateByUserId(Long userId,Object object);

    boolean deleteByUserId(Long userId);

    /*
     * @Description: 根据用户id查用户信息
     * @Param: userId
     * @return: UserInfo
     * @Date: 2023/2/27
    */
    UserInfo selectUserInfoByUserId(Long userId);


    /*
     * 管理员
    */
    PageInfo<UserDto> selectAllUserList(PageParamVo pageParamVo);


    PageInfo<UsersDto> getUserListBySearch(String searchParam, Integer pageNum, Integer pageSize);
}
