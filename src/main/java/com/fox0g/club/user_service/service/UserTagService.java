package com.fox0g.club.user_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.dto.UserTagDto;
import com.fox0g.club.model.po.UserTag;


import java.util.List;

/**
* @author 86187
* @description 针对表【user_tag】的数据库操作Service
* @createDate 2023-03-04 12:12:34
*/
public interface UserTagService extends IService<UserTag> {

    List<UserTagDto> getUserTagByTagTypeUserId(Integer tagType, Long userId);

    boolean deleteUserTagByIdUserId(Integer id, Long userId);

    int getUserTagCountByTagType(Integer tagType, Long userId);
}
