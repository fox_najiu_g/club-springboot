package com.fox0g.club.user_service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.fox0g.club.model.dto.UserDto;
import com.fox0g.club.model.dto.UsersDto;
import com.fox0g.club.model.po.UserInfo;
import com.fox0g.club.model.vo.PageParamVo;
import com.fox0g.club.user_service.mapper.UserInfoMapper;
import com.fox0g.club.user_service.mapper.UserTagMapper;
import com.fox0g.club.user_service.service.UserInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @author 86187
* @description 针对表【user_info】的数据库操作Service实现
* @createDate 2023-02-25 13:31:13
*/
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo>
    implements UserInfoService {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private UserTagMapper userTagMapper;

    @Override
    public UserInfo getByUserId(Long userId) {

        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUserId,userId);
        UserInfo userInfo = this.baseMapper.selectOne(queryWrapper);
        return userInfo;
    }

    @Override
    public boolean updateByUserId(Long userId,Object object) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(object,userInfo);
        if(userId!=userInfo.getUserId()){
            return false;
        }
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUserId,userId);

        this.baseMapper.update(userInfo,queryWrapper);
        return true;
    }

    @Override
    public boolean deleteByUserId(Long userId) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUserId,userId);
        int delete = this.baseMapper.delete(queryWrapper);
        if(delete == 0){
            return false;
        }
        return true;
    }

    @Override
    public UserInfo selectUserInfoByUserId(Long userId) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUserId,userId);
        return this.baseMapper.selectOne(queryWrapper);

    }

    @Override
    public PageInfo<UserDto> selectAllUserList(PageParamVo pageParamVo) {

        PageHelper.startPage(pageParamVo.getPageNum(),pageParamVo.getPageSize());
        List<UserDto> userDtoList = this.baseMapper.selectUserList();
        PageInfo<UserDto> pageInfo = new PageInfo<>(userDtoList);
        return pageInfo;
    }

    @Override
    public PageInfo<UsersDto> getUserListBySearch(String searchParam, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Long> ids = this.baseMapper.getUserIdListBySearchParam1(searchParam);
        List<UsersDto> list = new ArrayList<>();
        ids.stream().forEach(userId -> {
            UsersDto usersDto = new UsersDto();
            UserInfo userInfo = userInfoService.getByUserId(userId);
            BeanUtils.copyProperties(userInfo,usersDto);
            List<String> tag1Values = userTagMapper.getTagValueByUserIdTagValue(userId, 441);
            List<String> tag2Values = userTagMapper.getTagValueByUserIdTagValue(userId, 442);
            if(tag2Values.size()>=0 || ObjectUtils.isNotEmpty(tag2Values)){
                tag2Values.stream().forEach(value -> {
                    tag1Values.add(value);
                });
            }
            usersDto.setTagValue(tag1Values);
            list.add(usersDto);
        });

        PageInfo<UsersDto> usersDtoPageInfo = new PageInfo<>(list);
        return usersDtoPageInfo;
    }


}




