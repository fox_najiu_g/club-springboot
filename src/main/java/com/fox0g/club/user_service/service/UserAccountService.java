package com.fox0g.club.user_service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fox0g.club.model.po.UserAccount;
import com.fox0g.club.model.vo.LoginVo;


import java.util.Map;

/**
* @author 86187
* @description 针对表【user_account】的数据库操作Service
* @createDate 2023-02-25 18:35:19
*/
public interface UserAccountService extends IService<UserAccount> {

    /*
     * @Description: 根据用户id 获取用户账号信息
     * @Param: userId
     * @return: UserAccount
     * @Date: 2023/2/25
    */
    UserAccount getByUserId(Long userId);

    /*
     * @Description: 根据id 修改用户信息
     * @Param: userId
     * @param object
     * @return: void
     * @Date: 2023/2/25
    */
    boolean updateByUserId(Long userId, Object object);

    boolean deleteUserInfoAndUserAccountByUserId(Long userId);

    boolean updateUserPassword(Long userId, String password);

    Map<String, Object> loginUser(LoginVo loginVo);

    Long getUserIdByUserPhone(String userPhone);
}
