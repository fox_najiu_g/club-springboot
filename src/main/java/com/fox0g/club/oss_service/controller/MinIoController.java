package com.fox0g.club.oss_service.controller;


import com.fox0g.club.base_util.result.Result;
import com.fox0g.club.model.dto.OssUpDto;
import com.fox0g.club.oss_service.utils.MinioProperties;
import com.fox0g.club.oss_service.utils.MinioUtil;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @program: club-parent
 * @Date: 2023-04-23 20:48
 * @author: Fox0g
 * @description: TODO
 */
@Slf4j
@RestController
@RequestMapping("/oss/minio")
public class MinIoController {

    @Autowired
    private MinioUtil minioUtil;
    @Autowired
    private MinioProperties minioProperties;

    /*
     * @Description: minio文件上传
     * @Param: multipartFile
     * @return: Result
     * @Date: 2023/4/23
    */
    @PostMapping("/uploadFile")
    public Result minioUpload(@RequestParam(value = "file") MultipartFile multipartFile){
        OssUpDto ossUpDto = null;
        try {
            ossUpDto = minioUtil.uploadFile(multipartFile, minioProperties.getBucket());
            log.info("上传成功");
        } catch (Exception exception) {
            log.error("上传失败",exception);
        }
        if (ObjectUtils.isEmpty(ossUpDto)) {
            return Result.fail("上传失败，请稍后重试");
        }else{
            return Result.success("上传成功",ossUpDto);
        }
    }

    /*
     * @Description: minio文件删除根据文件整个路径名
     * @Param: key
     * @return: Result
     * @Date: 2023/4/23
    */
    @DeleteMapping("/deleteFile")
    public Result deleteFile(@RequestParam("key") String key){
        System.out.println(key);
        boolean isDelete = false;
        try {
            minioUtil.removeObject(minioProperties.getBucket(),key);
            log.info("删除成功");
        } catch (Exception exception) {
            log.error("删除失败",exception);
        }
        return Result.success();
    }



/*    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        try {
            // Create a minioClient with the MinIO server playground, its access key and secret key.
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://192.168.101.101:9090")
                            .credentials("JfHj5PrqG8Wb2nWK", "Zy9gWBlJFUQTGdFg2E2TJ8dOghKQrHEV")
                            .build();

            // Make 'asiatrip' bucket if not exist.
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("public").build());
            if (!found) {
                // Make a new bucket called 'asiatrip'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("public").build());
            } else {
                System.out.println("Bucket 'public' already exists.");
            }

            // Upload '/home/user/Photos/asiaphotos.zip' as object name 'asiaphotos-2015.zip' to bucket
            // 'asiatrip'.
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket("public")
                            .object("fox.jpg")
                            .filename("C:/Users/86187/Desktop/fox/fox.jpg")
                            .build());
            System.out.println("'C:/Users/86187/Desktop/fox/fox.jpg' is successfully uploaded as " + "object 'fox.jpg' to bucket 'public'.");
        } catch (MinioException e) {
            System.out.println("Error occurred: " + e);
            System.out.println("HTTP trace: " + e.httpTrace());
        }
    }*/
}
