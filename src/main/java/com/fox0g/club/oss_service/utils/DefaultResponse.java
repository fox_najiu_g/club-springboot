package com.fox0g.club.oss_service.utils;


import lombok.Data;
import org.apache.logging.log4j.util.StringMap;

/**
 * @program: club-parent
 * @Date: 2023-02-26 10:16
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class DefaultResponse {
    public String hash;
    public String key;
    public StringMap params;

    public DefaultResponse() {
    }
}
