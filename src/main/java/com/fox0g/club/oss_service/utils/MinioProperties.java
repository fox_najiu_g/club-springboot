package com.fox0g.club.oss_service.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @program: club-parent
 * @Date: 2023-04-23 21:31
 * @author: Fox0g
 * @description: TODO
 */

@ConfigurationProperties(prefix = "minio.oss")
@Component
@Data
public class MinioProperties {

    private String accessKey;

    private String secretKey;

    private String endpoint;

    private String bucket;

    private String endpoint2;

}
