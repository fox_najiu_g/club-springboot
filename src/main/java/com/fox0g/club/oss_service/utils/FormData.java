package com.fox0g.club.oss_service.utils;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: club-parent
 * @Date: 2023-02-26 11:27
 * @author: Fox0g
 * @description: TODO
 */
@Data
public class FormData {
    MultipartFile file;
}
