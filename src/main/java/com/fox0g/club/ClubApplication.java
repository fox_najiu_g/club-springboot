package com.fox0g.club;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @program: club-srpingboot
 * @Date: 2023-04-29 15:22
 * @author: Fox0g
 * @description: TODO
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.fox0g"})
public class ClubApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClubApplication.class, args);
    }
}
